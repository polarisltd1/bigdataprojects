import groovyx.javafx.GroovyFX
import groovyx.javafx.SceneGraphBuilder
import javafx.stage.StageStyle
import javafx.stage.Stage

GroovyFX.start
{
   stage(title: 'RMOUG Training Days 2013',
         width: 300, height: 100,
         show: true)
   {
      scene
      {
         stackPane
         {
            text('Hello GroovyFX!', x: 50, y: 40)
         }
      }
   }
}

