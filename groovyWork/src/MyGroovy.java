package testGroovySCript;

import java.io.File;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;

public class MyGroovy {


String GROOVY_SCRIPT_TEXT = "println 'Hello World Groovy ";	
	
	
	
	public static void main(String[] args) throws Exception{
		// args[0] - groovy script path 
	   MyGroovy me = new MyGroovy(args[0]);
	
	}
	
	MyGroovy(String scriptFileName) throws Exception{
		

		ClassLoader parent = getClass().getClassLoader();
		GroovyClassLoader loader = new GroovyClassLoader(parent);
		File script = new File(scriptFileName);
		System.out.println("Script: "+script.getAbsolutePath());
		Class groovyClass = loader.parseClass(script);

		// let's call some method on an instance
		GroovyObject groovyObject = (GroovyObject) groovyClass.newInstance();
		Object[] args = {};
		groovyObject.invokeMethod("run", args);	
		
		
		
	}
	
	
}
