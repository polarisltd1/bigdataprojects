import groovy.swing.SwingBuilder
import javax.swing.WindowConstants as WC
import javax.swing.JOptionPane
import javax.swing.JTabbedPane
import javax.swing.JScrollPane
import javax.swing.BoxLayout as BXL
import java.awt.Font

Font font = new Font("Serif", Font.BOLD, 13) 
int numPanels = 20

swing = new SwingBuilder()
frame = swing.frame(title:'test', pack:true,   visible:true, defaultCloseOperation:WC.DISPOSE_ON_CLOSE) {


    tabbedPane(id: 'tabs', tabLayoutPolicy:JTabbedPane.SCROLL_TAB_LAYOUT) {




        panel(name: 'Tab 1', background:java.awt.Color.WHITE ) {
            boxLayout(axis: BXL.Y_AXIS) 
            panel(alignmentX: 0f, background:java.awt.Color.WHITE){
                label ( 'Label 1', preferredSize: [104, 24]).setFont(font) 
                label ( 'Label 2', preferredSize: [104, 24]).setFont(font) 
                label ( 'Label 3', preferredSize: [104, 24]).setFont(font) 
            }   
            scrollPane( verticalScrollBarPolicy:JScrollPane.VERTICAL_SCROLLBAR_ALWAYS) {
                vbox (){
                    (1..numPanels).each { num ->
                        def panelID = "panel$num"
                        def pane = panel( alignmentX:0f, id:panelID, background:java.awt.Color.GREEN ) {
                            label('description') 
                            textField( id: "description$num", text:panelID, columns: 70 )
                            button( id: "buttonpanel$num", text:panelID, actionPerformed:{
                            swing."$panelID".background = java.awt.Color.RED
                            } )
                        }
                    }
                }
            }
        }       


        panel(name: 'Tab 2', background:java.awt.Color.WHITE) {
            textField(text: 'Some text', columns: 15)
            scrollPane() {
                textArea(text: 'Some text', columns: 15, rows: 4)
            }
        }




       
    }
    boxLayout(axis: BXL.Y_AXIS)
    panel(id:'secondPanel', background:java.awt.Color.WHITE){                       
        button('Quit', actionPerformed:{
        dispose()
        })
    }   
}
frame.size = [ frame.width, 600 ]
