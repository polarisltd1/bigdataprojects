/*

This script reads regullarly captured jstacks and sorts them so we can see change in time

jstacks should be stiored into subdirectory


18861916        8 -rw-r--r--    1 robertspolis     staff                 602 Jul 28 13:15 ./jstackReport.groovy
18863711     4592 -rw-r--r--    1 robertspolis     staff             2347758 Jul 28 13:09 ./out/jstackReport.txt
...
18861590       96 -rw-r--r--    1 robertspolis     staff               48067 Jul 25 18:21 ./js/js_090.txt
18861595       96 -rw-r--r--    1 robertspolis     staff               46677 Jul 25 18:22 ./js/js_091.txt
18861644       96 -rw-r--r--    1 robertspolis     staff               45529 Jul 25 18:22 ./js/js_092.txt
....

*/
File fileOut = new File("./out/jstackReport.txt")

list = []


files = new File("./js/").listFiles().each{ file ->
	
              print  "file ${file.getAbsolutePath()}\n"

	file.eachLine { line ->

	if (line.contains("prio=")){

     		txt = "${line} --  ${file.name} \n"
     		//print txt
     		// fileOut << txt
     		list << txt
		
	}
	
	}
	
}

list.sort { p1, p2 -> p1.compareToIgnoreCase(p2) }

print "sorted!\n"

list.each{
	
fileOut << it	
	
}

