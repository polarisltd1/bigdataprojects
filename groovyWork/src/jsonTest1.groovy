import groovy.json.JsonOutput

class Item{

   String name
   List children = []
    
   String toString() {
       "[Person] name : ${name}, age : ${children}"
   }
}

def myName = "Juri"

def item = new Item(name:myName,children:[])

item.children << new Item(name:"Karl",children:[])

println "Person Object : " + item

println "Person Object in JSON : " + JsonOutput.toJson(item)

println "JSON Pretty Print"

println "-----------------"

// prettyPrint requires a String and NOT an Object

println JsonOutput.prettyPrint(JsonOutput.toJson(item))

println ""

 

