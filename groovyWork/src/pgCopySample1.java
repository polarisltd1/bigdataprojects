// Copy command reference!
//
// http://jdbc.postgresql.org/documentation/publicapi/org/postgresql/copy/CopyManager.html

// http://pgpen.blogspot.com/2013/05/using-copy-in-your-jdbc-code.html

// Create the table test before running the file: 
//create table public.test(col1 int , col2 int);
//Compile using javac -classpath ./edb-jdbc14.jar pgCopy.java
// Run using java -cp ./edb-jdbc14.jar;. pgCopy

import com.edb.*;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class pgCopy
{
    public static void main(String[] args)
    {
            Connection con = null;
            PreparedStatement pst = null;
/* These are the insert values
* You can also build them from a list or
* you can accept the entries programatically 
* and then separate them with the delimeter 
* and build a byte array
* | is used a delimiter between two fields
* you can use your own here and replace the same in copyIn call
*/
            byte[] 
            insert_values="1|10\n2|20\n3|30\n4|40".getBytes();

//These are my connection parameters
            String url = "jdbc:edb://SAMEER:5444/edb";
            String user = "enterprisedb";
            String password = "ashnik";
            CopyIn cpIN=null;
            String driver="com.edb.Driver";
        try{
            Class.forName(driver);
            con = DriverManager.getConnection(
                         url, user, password);

            CopyManager cm = new CopyManager(
                               (BaseConnection) con);
/*Copy command
* Replace public.test(col1, col2) with you table Name and
* replace | with the delimeter of you choice. 
* It should be same as the delimeter used in defining 
* the variable byte[] insert_values
*/
          cpIN= 
            cm.copyIn(
     "COPY public.test(col1, col2) FROM STDIN WITH DELIMITER '|'"
      );
                
          cpIN.writeToCopy(insert_values,0,insert_values.length);
          cpIN.endCopy();

/*
//
//  There is a similar copyOut method under copyManager which can help you send the data from a table to a file/variable.
//

fileWHandle=new FileWriter("sample.txt");
 cm.copyOut
("COPY public.test to STDOUT WITH DELIMITER '|'",fileWHandle);

*/



          System.out.println("Below Values are inserted");
          System.out.println(new String(insert_values));
        }
        catch (SQLException ex)
        {
            Logger lgr = 
                      Logger.getLogger(CopyFrom.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        catch (ClassNotFoundException ex)
        {
            Logger lgr = 
                      Logger.getLogger(CopyFrom.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        finally
        {
                try
                {
                    if (con!=null)
                        con.close();
                }
                catch (SQLException ex)
                {
                    Logger lgr = 
                      Logger.getLogger(CopyFrom.class.getName());
                    lgr.log(Level.SEVERE, ex.getMessage(), ex);
                }
        }
    }
}