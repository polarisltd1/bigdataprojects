import javafx.beans.property.SimpleStringProperty

import static groovyx.javafx.GroovyFX.start

def selectedProperty = new SimpleStringProperty("");
start {

    final fileChooser = fileChooser(initialDirectory: ".", title: "FileChooser Demo") {
        filter("images", extensions: ["*.txt", "*.tx1", "*.tx2", "*.tx3"])
    }
    final dirChooser = directoryChooser(initialDirectory: ".", title: "DirectoryChooserDemo");

    stage(title: "GroovyFX Chooser Demo", width: 400, height: 300, visible: true, resizable: true) {
        scene(fill: GROOVYBLUE) {
            vbox(spacing: 10, padding: 10) {
                hbox(spacing: 10, padding: 10) {
                    button("Open file", onAction: {
                        selectedfile = fileChooser.showOpenDialog(primaryStage)
                        selectedProperty.set(selectedfile ? selectedfile.toString() : "")
                    })
                    button("Save file", onAction: {
                        selectedfile = fileChooser.showSaveDialog(primaryStage)
                        selectedProperty.set(selectedfile ? selectedfile.toString() : "")
                    })
                    button("Select directory", onAction: {
                        selectedfile = dirChooser.showDialog(primaryStage)
                        selectedProperty.set(selectedfile ? selectedfile.toString() : "")
                    })
                }
                label(id: 'selected')

            }
        }
    }
    selected.textProperty().bind(selectedProperty)
}


print(selectedProperty.get())
