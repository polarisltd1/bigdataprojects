import javafx.scene.Scene
 
import static groovyx.javafx.GroovyFX.start
 
def chamber = ["5 Stelle": 108,
               "Italia.\nBene commune": 340,
               "Con Monti per l'Italia": 45,
               "Berlusconi": 124,
               "others": 4]
 
start {
    stage(title: 'Italian chamber of Deputies', width: 1024, height: 700, visible: true) {
        Scene s = scene {
            tilePane {
                barChart(barGap: 10, categoryGap: 20, title: "Italy's election in February 2013") {
                    series(name: 'Chamber (seats)', 
                           data: ['5 Stelle', 17.5, 'Monti', 7.8, 'Bene commune', 55, 'Berlusconi', 19.9])
                    series(name: 'Senate (seats)', 
                           data: ['5 Stelle', 17.4, 'Monti', 6.1, 'Bene commune', 39.1, 'Berlusconi', 37.1])
                }
                pieChart(data: chamber, title: "Chamber of Deputies")
            }
        }
        s.stylesheets.add("Chart.css")
    }
}


