
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;


        Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        FileWriter fw = null;

        String url = "jdbc:postgresql://127.0.0.1:5432/db1"
        String user = "robertsp";
        String password = "";

        try {

            con = DriverManager.getConnection(url, user, password);
           
            cm = new CopyManager((BaseConnection) con);

            fw = new FileWriter("/Users/robertsp/weather.txt");
            cm.copyOut("COPY weather TO STDOUT WITH DELIMITER AS '|'", fw);
            fw.close()

        } catch (Exception e) {

            println(e)

        } 

