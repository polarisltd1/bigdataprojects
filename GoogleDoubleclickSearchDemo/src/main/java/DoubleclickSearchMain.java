/**
 * Created by robertspolis on 20/07/2017.
 */





        import com.google.api.client.auth.oauth2.TokenResponseException;
        import com.google.api.client.googleapis.json.GoogleJsonError.ErrorInfo;
        import com.google.api.client.googleapis.json.GoogleJsonResponseException;
        import com.google.api.services.doubleclicksearch.Doubleclicksearch;
        import com.google.api.services.doubleclicksearch.model.Report;
        import com.google.api.services.doubleclicksearch.model.ReportApiColumnSpec;
        import com.google.api.services.doubleclicksearch.model.ReportRequest;
        import com.google.api.services.doubleclicksearch.model.ReportRequest.ReportScope;
        import com.google.api.services.doubleclicksearch.model.ReportRequest.TimeRange;

        import java.io.*;
        import java.util.Arrays;
        import java.util.List;
        import java.util.concurrent.TimeUnit;
        import java.util.stream.Collectors;


/*

Dictionary of possible options.

https://github.com/google/google-api-go-client/blob/master/doubleclicksearch/v2/doubleclicksearch-api.json

References to specs:

Google DoubleClick Search (DCS) API v2
API Details:
https://developers.google.com/doubleclick-search/v2/how-tos/reporting/
DCS Log Data

Download Frequency: 	Hourly
Download Format: 		CSV (.csv)

https://developers.google.com/doubleclick-search/v2/reference/reports

https://developers.google.com/doubleclick-search/v2/report-types/conversion
https://developers.google.com/doubleclick-search/v2/report-types/keyword


 */

public class DoubleclickSearchMain {

        DoubleclickSearchService doubleclickSearchService = new DoubleclickSearchService();

 // As per spec, just Conversion and Keyword reports



        static       String[] ADVERTISER_COL_NAMES = new String[]{
                "agency"
                ,"agencyId"
                ,"advertiser"
                ,"advertiserId"
        };

///////

        static       String[] CONVERSION_COL_NAMES = new String[]{
                "status",
                "deviceSegment",
                "floodlightGroup",
                "floodlightGroupConversionType",
                "floodlightGroupId",
                "floodlightGroupTag",
                "floodlightActivity",
                "floodlightActivityId",
                "floodlightActivityTag",
                "agency",
                "agencyId",
                "advertiser",
                "advertiserId",
                "account",
                "accountId",
                "accountType",
                "campaign",
                "campaignId",
                "campaignStatus",
                "adGroup",
                "adGroupId",
                "adGroupStatus",
                "keywordId",
                "keywordMatchType",
                "keywordText",
                "productTargetId",
                "productGroupId",
                "ad",
                "adId",
                "isUnattributedAd",
                "inventoryAccountId",
                "productId",
                "productCountry",
                "productLanguage",
                "productStoreId",
                "productChannel",
                "conversionId",
                "advertiserConversionId",
                "conversionType",
                "conversionRevenue",
                "conversionQuantity",
                "conversionDate",
                "conversionTimestamp",
                "conversionLastModifiedTimestamp",
                "conversionAttributionType",
                "conversionVisitId",
                "conversionVisitTimestamp",
                "conversionVisitExternalClickId",
                "conversionSearchTerm",
                "floodlightOriginalRevenue",
                "floodlightEventRequestString",
                "floodlightReferrer",
                "floodlightOrderId",
                "feedItemId",
                "feedId",
                "feedType"

        };

////////


static       String[] KEYWORD_COL_NAMES = new String[]{
                "status"
                ,"engineStatus"
                ,"creationTimestamp"
                ,"lastModifiedTimestamp"
                ,"agency"
                ,"agencyId"
                ,"advertiser"
                ,"advertiserId"
                ,"account"
                ,"accountId"
                ,"accountType"
                ,"campaign"
                ,"campaignId"
                ,"campaignStatus"
                ,"adGroup"
                ,"adGroupId"
                ,"adGroupStatus"
                ,"keywordId"
                ,"keywordMatchType"
                ,"keywordText"
                ,"keywordEngineId"
                ,"keywordMaxCpc"
                ,"effectiveKeywordMaxCpc"
                ,"keywordLandingPage"
                ,"keywordClickserverUrl"
                ,"isDisplayKeyword"
                ,"keywordMaxBid"
                ,"keywordMinBid"
                ,"keywordUrlParams"
                ,"bingKeywordParam2"
                ,"bingKeywordParam3"
                ,"keywordLabels"
                ,"qualityScoreCurrent"
                ,"topOfPageBidCurrent"
                ,"effectiveBidStrategyId"
                ,"effectiveBidStrategy"
                ,"bidStrategyInherited"
                ,"effectiveLabels"
                ,"dfaActions"
                ,"dfaRevenue"
                ,"dfaTransactions"
                ,"dfaWeightedActions"
                ,"avgCpc"
                ,"avgCpm"
                ,"avgPos"
                ,"clicks"
                ,"cost"
                ,"ctr"
                ,"impr"
                ,"adWordsConversions"
                ,"adWordsConversionValue"
                ,"adWordsViewThroughConversions"
                ,"visits"
                ,"qualityScoreAvg"
                ,"topOfPageBidAvg"
                ,"date"
             //   ,"monthStart"
             //   ,"monthEnd"
             //   ,"quarterStart"
             //   ,"quarterEnd"
             //   ,"weekStart"
             //   ,"weekEnd"
             //   ,"yearStart"
             //   ,"yearEnd"
                ,"deviceSegment"
                ,"floodlightGroup"
                // ,"segment" // columns[66]: Column identifier is missing or have unrecognized values.
                ,"floodlightGroupId"
                ,"floodlightGroupTag"
                ,"floodlightActivity"
                ,"floodlightActivityId"
                ,"floodlightActivityTag"
                ,"ad"
                ,"adId"
                ,"adHeadline"
                ,"adHeadline2"
                ,"adDescription1"
                ,"adDescription2"
                ,"adDisplayUrl"
                ,"adLandingPage"
                ,"adType"
                ,"adPromotionLine"



};
/*

https://www.en.advertisercommunity.com/t5/Performance-Optimization/What-is-Avg-CPM-in-CPC-based-Campaign/td-p/223935#
 Your campaigns

  Avg CPC = $2
  Impression:-10,000
  Click:-200
  Your cost:-200*$2=$400
  So your avg CPM= 400/10,000*1000=$40


    */

        static       String[] KEYWORD_COL_NAMES_1 = new String[]{
                "avgCpc"
                ,"avgCpm"
                ,"avgPos"
                ,"clicks"
                ,"cost"
                ,"impr"
                ,"agencyId"
                ,"advertiserId"
                ,"accountId"
                ,"campaignId"
                ,"adGroupId"
                ,"keywordId"
                ,"date"
                //   ,"monthStart"
                //   ,"monthEnd"
                //   ,"quarterStart"
                //   ,"quarterEnd"
                //   ,"weekStart"
                //   ,"weekEnd"
                //   ,"yearStart"
                //   ,"yearEnd"




        };















        public static void main(String[] args)throws Exception {

                 new DoubleclickSearchMain().run();

        }


        void run()throws Exception{

                Doubleclicksearch service = doubleclickSearchService.getService();


                        //

                if(true) {
                        System.out.println("\n***Executing Keyword");
                        ReportRequest keywordsReq = createKeywordReportRequest();
                        String reportId = createReport(service, keywordsReq);
                        System.out.printf("Result: %s", reportId);
                        if(reportId!=null){
                                Report report = pollUntilReportIsFinished(service, reportId);
                                downloadFiles(service, report, "./report_Keyword");
                        }
                }
                if(false) {
                        System.out.println("\n***Executing Conversion");
                        ReportRequest keywordsReq = createConversionReportRequest();
                        String reportId = createReport(service, keywordsReq);
                        System.out.printf("Result: %s", reportId);
                        Report report = pollUntilReportIsFinished(service, reportId);
                        downloadFiles(service, report, "./report_Conversion");
                }

                if(false) {
                        System.out.println("\n\n***Executing Advertiser");
                        ReportRequest advertisersRequest = createAdvertiserReportRequest();
                        String reportId = createReport(service, advertisersRequest);
                        System.out.printf("Result: %s", reportId);
                        Report report = pollUntilReportIsFinished(service, reportId);
                        downloadFiles(service, report, "./report_advertiser");
                }




        }

static List<ReportApiColumnSpec> getColumnList(String[] columnsArray){

        return Arrays.asList(columnsArray)
                .stream()
                .map(it -> new ReportApiColumnSpec().setColumnName(it))
                .collect(Collectors.toList());

}


/**
 * Creates a campaign report request, submits the report, and returns the report ID.
 */
private static String createReport(Doubleclicksearch service, ReportRequest request) throws IOException {
        try {
                return service.reports().request(request).execute().getId();
        } catch (GoogleJsonResponseException e) {

                System.err.println("createReport>>Report request was rejected. \nMSG=" + e.getMessage() + " \n SM:" + e.getStatusMessage() + " - CONT:" + e.getContent());
                for (ErrorInfo error : e.getDetails().getErrors()) {

                        //System.err.println(">>" + error.getMessage() + " -- " + error.getReason() + " -- " + error.toPrettyString());
                }
                //System.exit(e.getStatusCode());

                return null;

        } catch (TokenResponseException e) {
                System.err.println("createReport>> TokenResponseException: Report request was rejected: " + e.getMessage()
                                + " \n " + e.getStatusMessage()
                                + " \n " + e.getContent()
                                + " \n " + e.toString()
                                + " \n " +e.getSuppressed().length
                );
                if (e.getDetails() != null) {
                        System.err.println(">>details : "
                                + e.getDetails().getError()
                                + e.getDetails().getErrorDescription()
                        );
                }


        } catch (Exception e) {
                System.out.println("createReport>>Exception " + e.getClass().getName() + " getting oauth " + e.getMessage());

        }
        return null;
}
/**
 * Creates KEYWORDS DoubleClick Search report request
 */
private static ReportRequest createKeywordReportRequest() {
        return new ReportRequest()
        .setReportScope(
                new ReportScope()
                .setAgencyId(20700000001120580L) // https://ddm.google.com/ds/cm/cm#all_advertisers.ay=20700000001120580;
              //  .setAdvertiserId(21700000001352016L) // BF-Italy-PPC
        )
        .setReportType("keyword")
        .setColumns(getColumnList(KEYWORD_COL_NAMES_1))
        .setTimeRange(new TimeRange()
                .setStartDate("2017-07-30")
                .setEndDate("2017-07-30")
        )
        .setDownloadFormat("csv")
        .setStatisticsCurrency("usd")
        .setMaxRowsPerFile(5000000);
        }


        private static ReportRequest createConversionReportRequest() {
                return new ReportRequest()
                        .setReportScope(
                                new ReportScope()
                                        .setAgencyId(20700000001120580L) // https://ddm.google.com/ds/cm/cm#all_advertisers.ay=20700000001120580;
                                // .setAdvertiserId(21700000001362314L) // Replace with your ID
                        )
                        .setReportType("conversion")
                        .setColumns(getColumnList(CONVERSION_COL_NAMES))
                        .setTimeRange(new TimeRange()
                                .setStartDate("2017-07-15")
                                .setEndDate("2017-07-18")
                        )
                        .setDownloadFormat("csv")
                        .setStatisticsCurrency("usd")
                        .setMaxRowsPerFile(5000000);
        }

        private static ReportRequest createAdvertiserReportRequest() {


                return new ReportRequest()

                        .setReportScope(
                                new ReportScope()
                                        .setAgencyId(20700000001120580L) // https://ddm.google.com/ds/cm/cm#all_advertisers.ay=20700000001120580;
                                // .setAdvertiserId(21700000001362314L) // Replace with your ID
                        )

                        .setReportType("advertiser")
                        .setColumns(getColumnList(ADVERTISER_COL_NAMES))
                        //.setTimeRange(new TimeRange()
                        //        .setStartDate("2017-07-18")
                        //        .setEndDate("2017-07-18")
                        .setDownloadFormat("csv")
                        .setStatisticsCurrency("usd")
                        .setMaxRowsPerFile(5000000)
                ;
        }








        /**
         * Polls the reporting API with the reportId until the report is ready.
         * Returns the report.
         *
         * If the report is not ready
         * If the report is not ready, DS returns an HTTP 202 response, and the isReportReady field is false.
         {
         "kind": "doubleclicksearch#report",
         "id": "MTMyNDM1NDYK",
         "isReportReady": false,
         "request": {
         ...
         },
         ...
         }
         *
         *
         */
        private static Report pollUntilReportIsFinished(Doubleclicksearch service, String reportId)
                throws IOException, InterruptedException {
                long delay = 1;
                while (true) {
                        Report report = null;
                        try {
                                report = service.reports().get(reportId).execute();
                        } catch (GoogleJsonResponseException e) {
                                System.err.println("Report generation has failed.");
                                System.exit(e.getStatusCode());
                        }
                        if (report.getIsReportReady()) {
                                return report;
                        }
                        System.out.format("Report %s is not ready - waiting %s seconds.%n", reportId, delay);
                        Thread.sleep(TimeUnit.SECONDS.toMillis(delay));
                        delay = delay + delay; // Double the delay for the next iteration.
                }
        }


///////////////
        /**
         * Downloads the shards of a completed report to the given local directory.
         * Files are named CampaignReport0.csv, CampaignReport1.csv, and so on.
         */
        private static void downloadFiles(
                Doubleclicksearch service,
                Report report,
                String localPath)
                throws IOException {
                for (int i = 0; i < report.getFiles().size(); i++) {
                        String filePath = localPath+ "_Report" + i;
                        FileOutputStream outputStream =
                                new FileOutputStream(new File(filePath));
                        service.reports().getFile(report.getId(), i).executeAndDownloadTo(outputStream);
                        outputStream.close();
                        System.out.printf("Report download completed %s %n",filePath);
                }

        }


        /**
         * Returns a simple static request that lists the ID and name of all
         * campaigns under agency 20100000000000895 and advertiser 21700000000011523.
         * Substitute your own agency ID and advertiser IDs for the IDs in this sample.
         */




        private static ReportRequest createSampleRequest() {
                return new ReportRequest()
                        .setReportScope(new ReportScope()
                                .setAgencyId(20700000001120580L) // Replace with your ID
                                .setAdvertiserId(21700000001375017L)) // Replace with your ID
                        .setReportType("keyword")
                        .setColumns(Arrays.asList(
                                new ReportApiColumnSpec[] {
                                        new ReportApiColumnSpec().setColumnName("campaignId"),
                                        new ReportApiColumnSpec().setColumnName("campaign")
                                }))
                        .setTimeRange(new TimeRange()
                                .setStartDate("2017-07-18")
                                .setEndDate("2017-07-18"))
                        .setDownloadFormat("csv")
                        .setStatisticsCurrency("gbp")
                        .setMaxRowsPerFile(5000000);
        }






}