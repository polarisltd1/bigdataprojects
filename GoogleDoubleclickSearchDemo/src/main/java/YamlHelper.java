import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;




/**
 * Created by robertspolis on 30/07/2017.
 */
public class YamlHelper {

    private final String CONFIG_FILENAME = "config.yaml";

    public static void main(String[] args){
    new YamlHelper().run();
    }


    public void run(){
        Config config = getConfig();
    };

    public Config getConfig() {
        if(writeConfig())System.exit(0); // if config file not found write one and exit
        Config config = new Config();
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            config = mapper.readValue(new File(CONFIG_FILENAME), Config.class);
            System.out.println(ReflectionToStringBuilder.toString(config,ToStringStyle.MULTI_LINE_STYLE));
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return config;
    }


    public boolean writeConfig() {
        File configFile = new File(CONFIG_FILENAME);
        if (configFile.exists())return false;
        Config config = new Config("aaa","bbb","ccc", AUTH_TYPE.TWO);
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
             mapper.writeValue(new FileOutputStream(configFile), config);
             System.out.println("Template config written!");
             return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    static public  enum AUTH_TYPE {ONE,TWO,THREE,FOUR,FIVE};

    static public class Config{

        private String client_id;
        private String client_secret;
        private String refresh_key;
        private String client_id_secret_json_path;
        private AUTH_TYPE auth_type;

        public Config(){};

        public Config(String a,String b,String c,AUTH_TYPE d){this.client_id=a;this.client_secret=b;this.refresh_key=c;this.auth_type=d;}




        public AUTH_TYPE getAuth_type() {
            return auth_type;
        }

        public void setAuth_type(AUTH_TYPE auth_type) {
            this.auth_type = auth_type;
        }





        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getClient_secret() {
            return client_secret;
        }

        public void setClient_secret(String client_secret) {
            this.client_secret = client_secret;
        }

        public String getRefresh_key() {
            return refresh_key;
        }

        public void setRefresh_key(String refresh_key) {
            this.refresh_key = refresh_key;
        }

        public String getClient_id_secret_json_path() {
            return client_id_secret_json_path;
        }

        public void setClient_id_secret_json_path(String client_id_secret_json_path) {
            this.client_id_secret_json_path = client_id_secret_json_path;
        }
    }




}