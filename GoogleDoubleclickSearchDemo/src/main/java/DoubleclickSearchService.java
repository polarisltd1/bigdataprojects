import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponseException;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonError;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStoreFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.doubleclicksearch.Doubleclicksearch;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


public class DoubleclickSearchService {


    private static final String SCOPE =
            //"https://www.googleapis.com/auth/doubleclicksearch";
            "https://www.googleapis.com/auth/doubleclicksearch";

    private static final Collection<String> SCOPES =
            Collections.unmodifiableCollection(Arrays.asList("https://www.googleapis.com/auth/doubleclicksearch"));

    YamlHelper.Config config = new YamlHelper().getConfig();

    YamlHelper.AUTH_TYPE authType = config.getAuth_type();



    /**
     * File for storing user credentials.
     */
    private static final java.io.File DATA_STORE_FILE =
            new File(System.getProperty("user.home"), ".credentials/doubleclicksearch.json");




    /**
     * Global instance of the {DataStoreFactory}. The best practice is to make it a single
     * globally shared instance across your application.
     */
    private static FileDataStoreFactory dataStoreFactory;

    /**
     * Sets up a new DoubleClick Search service. The builder for the service
     * requires a Credential object, which provides the credentials needed to
     * interact with DoubleClick Search.
     */
    public Doubleclicksearch getService() throws Exception {

        JsonFactory jsonFactory = new JacksonFactory();
        NetHttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();

        //Credential credential = readCredentialFromCommandLine(jsonFactory, transport);

        GoogleCredential credential=null;


        System.out.println("Using Auth Type:  "+authType);


        try{


        if(authType == YamlHelper.AUTH_TYPE.ONE)
            credential = GoogleCredential
                .getApplicationDefault()
                .createScoped(Collections.singleton(SCOPE));


        if(authType == YamlHelper.AUTH_TYPE.TWO)
            credential = authByIdSecretRefreshToken(jsonFactory, transport);




        if(authType == YamlHelper.AUTH_TYPE.THREE) {

            String filenameP12="/Users/robertspolis/projects/2017_tumra/streams/googleDoubleclickSearchAPI/project_key/BetfairDCMEMEA-8235be13c158.serviceAccount_1.p12";
            String client_email="1061152754063-2rivho5c6sghncimsjdvairkh4orcfhv.apps.googleusercontent.com";
            String userEmail="feeds@tumra.com";

// TokenResponseException: Report request was rejected.
// com.google.api.client.auth.oauth2.TokenResponseException -- 401 Unauthorized

            credential = authServAccByP12(
                    jsonFactory,
                    transport,
                    filenameP12,
                    client_email,
                    userEmail
            );




        }

         if(authType == YamlHelper.AUTH_TYPE.FOUR){

             String file1 = "/Users/robertspolis/projects/2017_tumra/streams/googleDoubleclickSearchAPI/project_key/BetfairDCMEMEA-cf8ba90fc0b8.service_account_1.json";
             String file2 = "/Users/robertspolis/projects/2017_tumra/streams/googleDoubleclickSearchAPI/project_key/BetfairDCMEMEA-b6175c5afad4.json";


             credential = authFromServiceAccountJson("/Users/robertspolis/projects/2017_tumra/streams/googleDoubleclickSearchAPI/mediacom_zip/mediacom.json");


         }


            if(authType == YamlHelper.AUTH_TYPE.FIVE) {

                // providing .json with client_id and client_secret and additionally refresh key
                credential =   generateCredentialWithUserApprovedToken(config.getClient_id_secret_json_path(), config.getRefresh_key());

            }


        } catch (GoogleJsonResponseException e) {
            GoogleJsonError error = e.getDetails();
            System.out.println(error.getMessage()+" pretty: "+error.toPrettyString());
        }catch(TokenResponseException e){
            System.err.println(">>> TokenResponseException: Report request was rejected. "+e.getMessage()
                    +" \n "+e.getStatusMessage()
                            +" \n "+e.getContent()
                            +" \n "+e.toString()
             );
            if(e.getDetails()!=null){
                System.err.println(">>> TokenResponseException: Report request was rejected. "
                        +e.getDetails().getError()
                        +e.getDetails().getErrorDescription()
                );
            }
        } catch (Exception e) {
            //GoogleJsonError error = e.getDetails();
            System.out.println("Exception "+e.getClass().getName()+" getting oauth " + e.getMessage());

        }


        //Collection COMPUTE_SCOPES =
        //        Collections.singletonList(ComputeScopes.COMPUTE);


        //        if (credential.createScopedRequired()) {
        //            credential = credential.createScoped(Arrays.asList(SCOPES));
        //        }

        return new Doubleclicksearch
                .Builder(transport, jsonFactory, credential)
                .setApplicationName("Sample")
                .build();
    }

    /**
     * option 1 to access credentials
     * ==============================
     * Generates a credential by reading client id, client secret and refresh
     * token from the command line. You could update this method to read the
     * data from a database or other secure location.
     */
    private GoogleCredential authByIdSecretRefreshToken(
            JsonFactory jsonFactory,
            NetHttpTransport transport) {

        GoogleCredential gc1 = new GoogleCredential.Builder()

                .setJsonFactory(JacksonFactory.getDefaultInstance())
                .setTransport(transport)
                .setClientSecrets(config.getClient_id(), config.getClient_secret())

                .build()
                .setRefreshToken(config.getRefresh_key())
                //.
                .createScoped(SCOPES)
                //.createScopedRequired()
                ;


                String accessToken = gc1.getAccessToken();
                System.out.printf("!!!! Using Access token %s %n",accessToken);

        return gc1;



/*
        return new GoogleCredential.Builder()

                .setJsonFactory(JacksonFactory.getDefaultInstance())
                .setTransport(transport)
                .setClientSecrets(clientId, clientSecret)
                .build()
                .setRefreshToken(refreshToken)
                .createScoped(Collections.singleton(SCOPE));
        */
    }

    /**
     * option 2 to access credentials
     * ==============================
     */
    private static GoogleCredential authFromServiceAccountJson(
            String jsonFilePath) throws Exception {



        GoogleCredential credential = GoogleCredential

                .fromStream(new FileInputStream(jsonFilePath))
                .createScoped(Collections.singleton(SCOPE))

                //.createScoped(Collections.singleton(SCOPE));

                //.setServiceAccountScopes(Collections.singleton(SCOPE));

                //.setServiceAccountScopes(Collections.singleton(SCOPE))



                ;

        return credential;


    }


    private GoogleCredential generateCredentialWithUserApprovedToken(String jsonFilePath,String refreshToken) throws IOException,
            GeneralSecurityException {

        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        File json = new File(jsonFilePath);
        InputStreamReader inputStreamReader =
                new InputStreamReader(new FileInputStream(jsonFilePath));

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, inputStreamReader);

        return new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(jsonFactory)
                .setClientSecrets(clientSecrets).build()

                .createScoped(Collections.singleton(SCOPE))
                .setRefreshToken(refreshToken);
    }



    /**
     * option 3 to access credentials
     * ==============================
     * @return
     * @throws Exception
     */
    private Credential accessAppDefaultCredential() throws Exception{

    GoogleCredential credential = GoogleCredential.getApplicationDefault();

    return credential;


    }

    private static GoogleCredential authServAccByP12(
                                                  JsonFactory jsonFactory,
                                                  NetHttpTransport httpTransport,
                                                  String filenameP12,
                                                  String serviceAccEmail,
                                                  String adminAcc) throws Exception {

                GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(httpTransport)
                .setJsonFactory(JacksonFactory.getDefaultInstance())
                .setServiceAccountId(serviceAccEmail)   // <-- SERVICE_ACC_EMAIL
                .setServiceAccountPrivateKeyFromP12File(new File(filenameP12))
                .setServiceAccountScopes(SCOPES)
                .build();


/*

    GoogleCredential credential = new GoogleCredential.Builder()
        .setServiceAccountId(serviceAccountEmail)
        .setServiceAccountScopes(SCOPES)
        .setServiceAccountPrivateKey(SecurityTestUtils.newRsaPrivateKey())
        .setTransport(transport)
        .setJsonFactory(JSON_FACTORY)
        .build();



         */




        return credential;

    }






    public static Credential authorize(JsonFactory jsonFactory,
                                       NetHttpTransport httpTransport,
                                        String jsonPath) throws Exception {
        // Load client secrets.
        InputStream in =
                DoubleclickSearchMain.class.getResourceAsStream(jsonPath);
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(jsonFactory, new InputStreamReader(in));

        // Build flow and trigger user authorization request.

        dataStoreFactory = new FileDataStoreFactory(DATA_STORE_FILE);

        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        httpTransport, jsonFactory, clientSecrets, Collections.singletonList(SCOPE))
                        .setDataStoreFactory(dataStoreFactory )
                        .setAccessType("offline")
                        .build();
        //Changed Part.
        Credential credential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("feeds@tumra.com");
        System.out.println(
                "Credentials saved to " + DATA_STORE_FILE.getAbsolutePath());
        return credential;
    }






    /*


    try {
       // Make your Google API call
} catch (GoogleJsonResponseException e) {
      GoogleJsonError error = e.getDetails();
      // Print out the message and errors
}

     */


/*
    //import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
    //import com.google.api.services.sqladmin.SQLAdminScopes;

// ...

    private static GoogleCredential authServAccByP12(

    GoogleCredential credential = GoogleCredential
            .
            .fromStream(new FileInputStream("MyProject-1234.json"))
            .createScoped(Collections.singleton(SCOPE));

    return credential;

}


*/
/*

// Construct a GoogleCredential object with the service account email
// and p12 file downloaded from the developer console.
HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
GoogleCredential credential = new GoogleCredential.Builder()
    .setTransport(httpTransport)
    .setJsonFactory(JSON_FACTORY)
    .setServiceAccountId(SERVICE_ACCOUNT_EMAIL)
    .setServiceAccountPrivateKeyFromP12File(new File(KEY_FILE_LOCATION))
    .setServiceAccountScopes(AnalyticsScopes.all())
    .build();
*/
/*


GoogleCredential credential = new GoogleCredential.Builder()
    .setTransport(httpTransport)
    .setJsonFactory(JSON_FACTORY)
    .setServiceAccountId(emailAddress)
    .setServiceAccountPrivateKeyFromP12File(new File("MyProject.p12"))
    .setServiceAccountScopes(Collections.singleton(SQLAdminScopes.SQLSERVICE_ADMIN))
    .setServiceAccountUser("user@example.com")
    .build();

*/

}