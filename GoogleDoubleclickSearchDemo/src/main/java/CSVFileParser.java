import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by robertspolis on 02/08/2017.
 */
public class CSVFileParser {


    public static void main(String[] args)throws Exception{

        if(false)new CSVFileParser().run("./report_Keyword_Report0");
        if(false)new CSVFileParser().run("./report_advertiser_Report0");



        if(true)new CSVFileParser().run("/Users/robertspolis/Downloads/keyword_2017_08_01_00.csv");
        if(false)new CSVFileParser().run("/Users/robertspolis/Downloads/keyword_2017_08_02_00.csv");

    }


    void run(String path)throws Exception{


    StringBuffer output = new StringBuffer();

    Reader in = new FileReader(path);
        int i=0,counti=0,countc=0;
    Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
    for (CSVRecord record : records) {

        // write any 10 recs.
        if(i++<10)System.out.println(record.toMap());


        if(path.toLowerCase().contains("keyword")) {

            try {

                if (!record.get("clicks").equals("") && Integer.valueOf(record.get("clicks")) > 0) {
                    //System.out.println(record.toMap());
                    countc++;
                    //System.out.println(record.toMap());
                    //output.append(record.toMap()).append("\n");
                }
                if (!record.get("impr").equals("") && Integer.valueOf(record.get("impr")) > 0) {
                    //System.out.printf("impr= %s%n", record.get("impr"));
                    counti++;
                }
            }catch(Exception e){System.out.printf("exception: %s%n",e.getMessage());}
        }else if(i++<10)System.out.println(record.toMap());



    }

        if(path.toLowerCase().contains("keyword")){
            System.out.printf("counts of positives impr= %d clicks=%d%n", counti,countc);

            Path file = Paths.get(path+".positive.txt");
            System.out.printf("writing positives: %s%n",file.toAbsolutePath());
            try (BufferedWriter writer = Files.newBufferedWriter(file))
            {
                writer.write(output.toString());
            }



        }



    }




}
