
#DoubleClick Search Reports #

 ## To make an asynchronous request ##

<https://developers.google.com/doubleclick-search/v2/how-tos/reporting/asynchronous-requests>

 Call Reports.request() to specify the type of data you want in the report.
 See Report Types for the types of data you can request.
 DS validates the request and returns a report ID—a unique identifier for this request.
 Call Reports.get() with the report id.

 The response from DS indicates:

 Whether the report is ready for download.
 If the report is ready, one or more URLs for downloading the report.

 Call Reports.getFile() to the download the report files encoded, or just directly download from the URLs.

 DS returns the report in a UTF-8 encoded file.


## DoubleClick Search Report types ##


 <https://developers.google.com/doubleclick-search/v2/report-types/>

 This section describes the types of reports you can request from DoubleClick Search, along with the columns that you can request for each type of report.

 For step-by-step instructions and examples, see Request Reports.

 Available report types

 - **account**
 Reporting data for engine accounts, including:
 Performance metrics across all of an engine account's campaigns.
 Configuration attributes for an engine account.
 - **ad**
 Performance metrics and configuration attributes for ads.
 - **advertiser**
 Reporting data for advertisers, including:
 Performance metrics across all of an advertiser's engine accounts and campaigns.
 Configuration attributes for an advertiser.
 - **adGroup**
 Performance metrics and configuration attributes for ad groups.
 - **adGroupTarget**
 Performance metrics for ad group level targets, which include location targets, dynamic ad targets, remarketing list targets, and age/gender targets. Some targets are available only in specific types of engine accounts.
 - **bidStrategy**
 Performance metrics and configuration attributes for bid strategies.
 - **campaign**
 Performance metrics and configuration attributes for campaigns.
 - **campaignTarget**
 Performance metrics for campaign targets, which include location targets (geo-location, proximity, and location extension) and remarketing list targets. Some targets are available only in specific types of engine accounts.
 - **conversion**
 Raw event data about conversions.
 - **feedItem**
 Performance metrics and configuration attributes for feed items (for example, sitelink, location, call or app extensions).
 - **floodlightActivity**
 Configuration attributes for Floodlight activities.
 - **keyword**
 Performance metrics and configuration attributes for keywords.
 - **negativeAdGroupKeyword**
 Configuration attributes for negative keywords that have been created at the ad group level.
 - **negativeAdGroupTarget**
 Configuration attributes for negative targets at the ad group level, which include location targets, dynamic ad targets, remarketing list targets and age/gender targets. Some targets are available only in specific types of engine accounts.
 - **negativeCampaignKeyword**
 Configuration attributes for negative keywords that have been created at the campaign level.
 - **negativeCampaignTarget**
 Configuration attributes for negative targets at the campaign level, which include location targets (geo-location, proximity, and location extension), dynamic ad targets, remarketing list targets and age/gender targets. Some targets are available only in specific types of engine accounts.
 - **paidAndOrganic**
 The paidAndOrganic report shows the search terms that either triggered your ads to appear on Google, triggered Google to display unpaid (organic) search results for your site, or both. The report only returns results if you have already set up the Paid and Organic report for the engine account you are reporting on.
 - **productAdvertised**
 Performance metrics, configuration attributes, and inventory attributes for products defined in an inventory feed and advertised in a shopping campaign.
 - **productGroup**
 Performance metrics and configuration attributes for product groups. Product group reports differ from keyword reports in several ways; Learn more.
 - **productLeadAndCrossSell**
 Performance metrics, configuration attributes, and inventory attributes for products defined in an inventory feed and sold as a result of paid search advertising.
 - **productTarget**
 Performance metrics and configuration attributes for product targets. Product targets were used by AdWords PLA campaigns to specify which products in a Google Merchant Center account should trigger Product Listing Ads (shopping ads) to appear in related searches on Google. Product targets and PLA campaigns have been replaced by AdWords Shopping campaigns. From DS, you can still request reports for historical metrics that were attributed to your PLA campaigns before the upgrade to Shopping campaigns.
 - **visit**
 Raw event data about visits. A visit occurs each time a consumer clicks an ad and is redirected to a landing page.



 ## Code derived from here ##
 <https://developers.google.com/doubleclick-search/v2/requests>
