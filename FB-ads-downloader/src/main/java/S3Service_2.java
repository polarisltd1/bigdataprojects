//package com.tumra.datafeeds.s3;

// documentation and reference
// cool methods to memorise
// FileUtils.copyInputStreamToFile(bis, scratchFile);
// scratchFile = File.createTempFile("prefix", "suffix");
// if(file.exists()) {

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.*;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

//import com.google.inject.Singleton;
//import com.tumra.datafeeds.uploader.service.PropertiesManager;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



/**
 *
 * @author robertspolis
 */


public class S3Service_2 {

    private static final Logger LOG = LogManager.getLogger(S3Service_2.class);

    String awsAccessKey = Config.AWS_ACCESS_KEY;
    String awsSectretKey = Config.AWS_SECRET_KEY;
    String bucket = Config.AWS_BUCKET;


    AmazonS3Client s3Client = null;





    public S3Service_2() {

    }

    public void s3Connect() {

        AWSCredentials credentials = new BasicAWSCredentials(this.awsAccessKey, this.awsSectretKey);
        s3Client = new AmazonS3Client(credentials);
        s3Client.setRegion(Region.getRegion(Regions.EU_WEST_1));

    }

    public void awsStore(String key, String value) {

            System.out.println("writing s3 key"+key);
            PutObjectResult result = s3Client.putObject(this.bucket, key, value);
            LOG.info(  "{} S3STORE-UPLOADED File written to S3 : {}" , getThreadId(),key);


    }


    /**
     * Store object in aws
     * *
     * Important notes:
     *
     * 1. we shouldn't use version of putObject() with inputstream parameter:
     *
     * The inputstream+ObjectMetadata method needs a minimum metadata of Content Length of your inputstream.
     * If you don't, then it will buffer in-memory to get that information, this could cause OOM.
     * So here we write InputStream into File and pass it to PutObjectRequest.
     *
     * 2. This error seems misleading but it points to wrong secretkey/accesskey combination
     *
     * com.amazonaws.services.s3.model.AmazonS3Exception: The request signature we calculated does not match the signature you provided.
     * Check your key and signing method. (Service: Amazon S3; Status Code: 403; Error Code: SignatureDoesNotMatch; Request ID: F43357B22EC04202)
     *
     *
     * @param key
     * @param is
     */

     public void awsStore(String key, InputStream is) throws IOException{



         File scratchFile = File.createTempFile("prefix", "suffix");

         //FileUtils.copyInputStreamToFile(is, scratchFile);


         PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucket, key, scratchFile);
         PutObjectResult result = s3Client.putObject(putObjectRequest);


         LOG.info(  "{} S3STORE-UPLOADED File written to S3 : {}" , getThreadId(),key);

         if(scratchFile.exists()) {
                scratchFile.delete();
         }


    }
    
    
    public void awsStore(String key, File source) {

            PutObjectResult result = s3Client.putObject(bucket, key, source);
            LOG.info(  "{} S3STORE-UPLOADED File written to S3 : {}" , getThreadId(),key);

    }



    /**
     * Version of list Bucket objects that lists entries by prefix and start date.
     * @param contentKeyPrefix
     * @param sinceDay only objects since that day are collected.
     * @return
     */

    public List<PathElement> listBucketObjects(String contentKeyPrefix, LocalDate sinceDay,String datePattern) {

        DateTimeFormatter hourFormatter = DateTimeFormatter.ofPattern(datePattern);
        List<PathElement> elements = new ArrayList<>();

        // do not catch any exceptions so caller do
            final ListObjectsV2Request req = new ListObjectsV2Request()
                    .withBucketName(bucket)
                    .withMaxKeys(200000)
                    .withPrefix(contentKeyPrefix);
            ListObjectsV2Result result;
            do {
                result = s3Client.listObjectsV2(req);

                for (S3ObjectSummary objectSummary
                        : result.getObjectSummaries()) {

                    String key = objectSummary.getKey();
                    String fileHour = PathElement.parsePathElement(key).hour;
                    LocalDate fileHourDate = LocalDate.parse(fileHour,hourFormatter);

                    if(fileHourDate.isAfter(sinceDay)) // adding into list only dates after
                        elements.add(PathElement.parsePathElement(key));
                }
                req.setContinuationToken(result.getNextContinuationToken());
            } while (result.isTruncated());

        LOG.debug("Listing bucket:prefix:keys_sz:keys " + bucket + "  " + contentKeyPrefix + " => sz:" + elements.size() + " []=" + elements);
        return elements;
    }



    /**
     * Version of list Bucket objects that lists entries by prefix. No other filters applied.
     * @param contentKeyPrefix
     * @return
     */

    public List<PathElement> listBucketObjects(String contentKeyPrefix) {

        List<PathElement> elements = new ArrayList<>();

        // do not catch any exceptions so caller do
            final ListObjectsV2Request req = new ListObjectsV2Request()
                    .withBucketName(bucket)
                    .withMaxKeys(200000)
                    .withPrefix(contentKeyPrefix);
            ListObjectsV2Result result;
            do {
                result = s3Client.listObjectsV2(req);

                for (S3ObjectSummary objectSummary
                        : result.getObjectSummaries()) {

                    String key = objectSummary.getKey();

                    elements.add(PathElement.parsePathElement(key));
                }
                req.setContinuationToken(result.getNextContinuationToken());
            } while (result.isTruncated());

        LOG.debug("Listing bucket:prefix:keys_sz:keys " + bucket + "  " + contentKeyPrefix + " => sz:" + elements.size() + " []=" + elements);
        return elements;
    }


    public static class PathElement {



        private String date;
        private String vendor;
        private String dataType;
        private String hour;
        private String timestamp;
        private String downloadDate;
        private int version;
        private String feed;
        private String split;
        private String originalKey; // this is complete string that has been parsed
        public String toString() {
          return ToStringBuilder.reflectionToString(this);
        }

        public static PathElement parsePathElement(String key) {
            // input/DataProvider=APPNEXUS/DataType=standard_feed/Date=2017_01_27/Hour=2017_01_27_06/Version=0/Timestamp=20170127082802/Split=0/standard_feed_2017_01_27_06.csv, Etag: 81f8771ed4c0af465bd41e45f5125a33
            PathElement pe = new PathElement();
            String[] tokens = key.split("/");
            for (String s : tokens) {
                String[] kv = s.split("=");

                switch (kv[0]) {
                    case "DataProvider":
                        pe.setVendor(kv[1]);
                        break;
                    case "Date":
                        pe.setDate(kv[1].replaceAll("_","-"));
                        break;
                    case "DownloadDate":
                        pe.setDownloadDate(kv[1].replaceAll("_","-"));
                        break;
                    case "DataType":
                        pe.setFeed(kv[1]);
                        pe.setDataType(kv[1]);
                        break;
                    case "Hour":
                        pe.setHour(kv[1]);
                        break;
                    case "Timestamp":
                        pe.setTimestamp(kv[1]);
                        break;
                    case "Version":
                        pe.setVersion(new Integer(kv[1]));
                        break;
                    case "Split":
                        pe.setSplit(kv[1]);
                        break;
                };

            }
            pe.setOriginalKey(key);
            return pe;
        }


        public String getHour() {
            return hour;
        }

        public void setHour(String hour) {
            this.hour = hour;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public String getFeed() {
            return feed;
        }

        public void setFeed(String feed) {
            this.feed = feed;
        }

        public String getSplit() {
            return split;
        }

        public void setSplit(String split) {
            this.split = split;
        }

        public String getOriginalKey() {
            return originalKey;
        }

        public void setOriginalKey(String originalKey) {
            this.originalKey = originalKey;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getVendor() {
            return vendor;
        }

        public void setVendor(String vendor) {
            this.vendor = vendor;
        }

        public String getDataType() {
            return dataType;
        }

        public void setDataType(String dataType) {
            this.dataType = dataType;
        }

        public String getDownloadDate() {
            return downloadDate;
        }

        public void setDownloadDate(String downloadDate) {
            this.downloadDate = downloadDate;
        }
    }

    /**
     * Used by logging to display thread id.
     * @return thread id
     */
    private String getThreadId() {
        return Thread.currentThread().getName() + " ";
    }

}
