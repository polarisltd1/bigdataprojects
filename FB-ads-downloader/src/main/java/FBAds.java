import com.facebook.ads.sdk.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;

import net.openhft.chronicle.map.ChronicleMapBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import static com.facebook.ads.sdk.AdsInsights.EnumActionAttributionWindows.*;
//import static com.sun.corba.se.impl.util.RepositoryId.cache;

/**
 * Created by robertspolis on 21/10/2017.
 */




public class FBAds {


    // Notes:
    //
    // 1. You need to generate a user access token for your app and ask for the 'ads_management' permission.
    //
    // 2. The Facebook Ads SDK for Java provides an easy interface and abstraction to the Marketing API.
    //
    // 3. Reminder about pagination if there are too many objects.
    //   Important: Handling pagination: Most edge APIs have default pagination, which returns a limited number of objects (~30 objects) instead of the entire list. If you want to load more, you need to make a separate API call. In our SDK, you can call to nextPage() get next page:
    //
    //   campaigns = campaigns.nextPage();
    //
    // 4. instead of get() we can use fetch()
    //   fetch() is just another shortcut for
    //   campaign = campaign.get().requestAllFields().execute();
    //   campaign.fetch();
    //
    // 5. public static final APIContext context = new APIContext(ACCESS_TOKEN, APP_SECRET).enableDebug(true).setLogger(System.out);
    //    customise logger by calling .setLogger(PrintSteam)

/*

s3://bucket/accounts/accountId=1234/acc.json

s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/campaign.json

s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/adsetId=4567/adset4567.json

s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/adsetId=4567/adId=89/ad89.json

s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/adsetId=4567/adId=89/date=2017-10-89/metrics.json

s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/date=2017-10-19/adsetId=4567/adId=89/date=2017-10-89/attributionWindow=1d/metrics.json (edited)

s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/date=2017-10-19/adsetId=4567/adId=89/date=2017-10-89/attributionWindow=7d/metrics.json (edited)


 */



    public static final String ACCESS_TOKEN = Config.ACCESS_TOKEN;
    public static final String APP_SECRET = Config.APP_SECRET;
    public static final String S3_BUCKET = Config.AWS_BUCKET;

    //public static final File imageFile = new File(Config.IMAGE_FILE);

    //public static final APIContext context = new APIContext(ACCESS_TOKEN, APP_SECRET).enableDebug(true);

    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    S3Service_2 s3 = new S3Service_2();

    Set<KeyObject> lookup = new HashSet();


    AdAccount account;
    APINodeList<Campaign> campaigns;

    public static Map<String, String> pcacheInsightsCampaigns;
    public static Map<String, String> pcacheCampaigns;

    public static Map<String, String> pcacheInsightsAdSets;
    public static Map<String, String> pcacheAdSets;

    public static Map<String, String> pcacheInsightsAds;
    public static Map<String, String> pcacheAds;


    /*

    Campaign having no specific fields. RequestAllFields() should be sufficient.
    List<String> CAMPAIGN_REQUEST_FIELDS = Arrays.asList(
            "id",
            "account_id",
            "adlabels",
            "boosted_object_id",
            "budget_rebalance_flag",
            "buying_type",
            "can_use_spend_cap",
            "configured_status",
            "created_time",
            "effective_status",
            "name","objective",
            "recommendations",
            "spend_cap",
            "start_time",
            "status",
            "stop_time",
            "updated_time");
    */

    List<String> INSIGHTS_REQUEST_FIELDS = Arrays.asList(
            "account_currency",
            ,"account_id"
            ,"account_name"
            ,"action_values"
            ,"actions"
            ,"ad_id"
            ,"ad_name"
            ,"adset_id"
            ,"adset_name"
            ,"buying_type"
            ,"call_to_action_clicks"
            ,"campaign_id"
            ,"campaign_name"
            ,"canvas_avg_view_percent"
            ,"canvas_avg_view_time"
            ,"canvas_component_avg_pct_view"
            ,"clicks"
            ,"comparison_node"
            ,"cost_per_10_sec_video_view\n"
            ,"cost_per_action_type"
            ,"cost_per_estimated_ad_recallers"
            ,"cost_per_inline_link_click"
            ,"cost_per_inline_post_engagement"
            ,"cost_per_outbound_click"
            ,"cost_per_total_action"
            ,"cost_per_unique_action_type"
            ,"cost_per_unique_click"
            ,"cost_per_unique_inline_link_cli"
            ,"cost_per_unique_outbound_click"
            ,"cpc"
            ,"cpm"
            ,"cpp"
            ,"ctr"
            ,"date_start"
            ,"date_stop"
            ,"estimated_ad_recall_rate"
            ,"estimated_ad_recallers"
            ,"frequency"
            ,"impressions"
            ,"inline_link_click_ctr"
            ,"inline_link_clicks"
            ,"inline_post_engagement"
            ,"mobile_app_purchase_roas"
            ,"objective"
            ,"outbound_clicks"
            ,"outbound_clicks_ctr"
            ,"reach"
            ,"relevance_score"
            ,"social_clicks"
            ,"social_impressions"
            ,"social_reach"
            ,"social_spend"
            ,"spend"
            ,"total_action_value"
            ,"total_actions"
            ,"total_unique_actions"
            ,"unique_actions"
            ,"unique_clicks"
            ,"unique_ctr"
            ,"unique_inline_link_click_ctr"
            ,"unique_inline_link_clicks"
            ,"unique_link_clicks_ctr"
            ,"unique_outbound_clicks"
            ,"unique_outbound_clicks_ctr"
            ,"unique_social_clicks"
            ,"video_10_sec_watched_actions"
            ,"video_15_sec_watched_actions"
            ,"video_30_sec_watched_actions"
            ,"video_avg_percent_watched_actio"
            ,"video_avg_time_watched_actions"
            ,"video_p100_watched_actions"
            ,"video_p25_watched_actions"
            ,"video_p50_watched_actions"
            ,"video_p75_watched_actions"
            ,"video_p95_watched_actions"
            ,"website_ctr"
            ,"website_purchase_roas"


            );


    private static final Logger LOG = LogManager.getLogger(FBAds.class);

    private static String S3_PREFIX = "input/test/FB.005";

    public static void configureCache() throws Exception{


        /*
        HazelcastInstance instance = Hazelcast.newHazelcastInstance();

        ccache = instance.getMap("Items");

        if(false)ccache.destroy(); // destroy at the end
        */
        /*

        ChronicleMap Map usage
        ====================

        Points to be noted

    There are few points to be noted about a ChronicleMap.

    It stores the data onto a file for persistency.

    In order to implement, the object (which has to be stored) must implement Serializable/Externalizable

    Two programs can share the same file (Used while creating the map) to share the same data across JVMs.

    Replication can be done using Chronicle Map using TCP/IP. (not detailed in the current post).

    No need to depend on JVM Heap Memory as the data is store Off-Heap (on a file).

    And the last important point is, the performance of the Map is purely dependent on how the Operating System allocates the memory to the file.
    If we clearly observe, all these advantages or features are because of storing a data off-heap using a file.

    How to create

    ChronicleMap is created using ChronicleMapBuilder. See the sample code for creating the map.

        Map<Integer,String> map = ChronicleMapBuilder.of(Integer.class,
                String.class).create();

     If you want to persist the data to a file. Use like this

        File f = new File("/tmp/file.map");
        Map<Integer, String> map = ChronicleMapBuilder.of(Integer.class,
                String.class).
                createPersistedTo(f); Here, we are creating map and persisting to a file /tmp/file.map.

            As its storing the data onto a file, we can limit the size of the Map by specific number of entries it can store. See below

        Map<Integer,String> map = ChronicleMapBuilder.of(Integer.class,
                String.class).
                entries(2000).
                createPersistedTo(new File("/tmp/file.map")); There is a difference between Concurrent Map and Chronicle Map in size. It can't be re-sized once we fix the entries size as re-sizing is a costly operation on Map.

ChronicleMap Interface

There are few specials methods which are provided by ChronicleMap. See below

V getUsing(K key, V value); getUsing is same as get(key) but getUsing will return the value in value parameter without creating a new object whereas get will create a new object for returning the value with key.
V acquireUsing(K key, V value); acquireUsing is again same as getUsing but if there is no value defined with key, it will insert a new entry with key and returns the same value.
ChronicleMap can read and write the data from/to a JSON object. The following methods can be used to do so
void getAll(File toFile) throws IOException; To read map from the file which was created by another ChronicleMap using JSON format.
void putAll(File fromFile) throws IOException; To dump the entire map into a file using a JSON format.
void close(); As the data is stored off-heap, its recommended to close the map to release the heap data and persist the data.
All other methods are same as Map as it implements java.util.Map. Explore more on ChronicleMap

example

@Test
    public void simpleLoggingTest() {
        ChronicleMap<Integer, IntValue> map = ChronicleMapBuilder
                .of(Integer.class, IntValue.class)
                .entries(100)
                .entryOperations(simpleLoggingMapEntryOperations())
                .defaultValueProvider(simpleLoggingDefaultValueProvider())
                .create();

        IntValue value = Values.newHeapInstance(IntValue.class);
        value.setValue(2);
        map.put(1, value);
        map.remove(1);
        map.acquireUsing(3, Values.newNativeReference(IntValue.class)).addAtomicValue(1);
        IntValue value2 = Values.newHeapInstance(IntValue.class);
        value2.setValue(5);
        map.forEachEntry(e -> e.context().replaceValue(e, e.context().wrapValueAsData(value2)));
        map.forEachEntry(e -> e.context().remove(e));

    }


        */


        pcacheCampaigns = ChronicleMapBuilder.of(
                String.class,
                String.class).
                createPersistedTo(new File("/tmp/db_campaigns.map"));


        pcacheInsightsCampaigns = ChronicleMapBuilder.of(
                String.class,
                String.class).
                createPersistedTo(new File("/tmp/db_insightsCampaigns.map"));

        pcacheAdSets = ChronicleMapBuilder.of(
                String.class,
                String.class).
                createPersistedTo(new File("/tmp/db_Adsets.map"));

        pcacheInsightsAdSets = ChronicleMapBuilder.of(
                String.class,
                String.class).
                createPersistedTo(new File("/tmp/db_insightsAdSets.map"));


        pcacheAds = ChronicleMapBuilder.of(
                String.class,
                String.class).
                createPersistedTo(new File("/tmp/db_Ads.map"));

        pcacheInsightsAds = ChronicleMapBuilder.of(
                String.class,
                String.class).
                createPersistedTo(new File("/tmp/db_insightsAds.map"));



    }





    public static void main(String[] args) throws Exception{



        FBAds fb = new FBAds();
        fb.s3connect();
        configureCache();


        while(true) {
            try {

                LOG.info("running at {}", LocalDateTime.now());
                fb.run();
                System.exit(0);

            } catch (com.facebook.ads.sdk.APIException e) {

                LOG.info("com.facebook.ads.sdk.APIException ### {}",e.getMessage());

                // access token issue, exit.
                if(e.getMessage().contains("User request limit reached"))
                    doSleep(5*60*1000);
                else
                   System.exit(-100);  // any other error exit!


            }catch(Exception e2){

                e2.printStackTrace();
                System.exit(-101);

            }

        }

    }


    void s3connect(){
        s3.s3Connect();

    }



    static void doSleep(int msecs){

        try {
            Thread.currentThread().sleep(msecs);
        }catch(Exception e1){
            LOG.info(e1.getMessage());
        }


    }


    void run() throws com.facebook.ads.sdk.APIException, Exception{


        //
        // We want exceptions being handled outside of run so we launch back after delay.
        // This is due to need address user download amount exhausted.
        //

        APIContext context = new APIContext(ACCESS_TOKEN, APP_SECRET).enableDebug(true);

        LOG.info("context {}",context.toString());


        getCampaigns(context); // load ids into cache

        getAdSets(context); // load ids into cache

        getAds(context); // load ids into cache


        for(String campaignId:pcacheCampaigns.values()) {


            if (!pcacheInsightsCampaigns.containsKey(campaignId)) {

                Campaign campaign = new Campaign(campaignId, context);
                // we need basic Campaign object as we fetch it with complete fields inside storeObjectMetrics()

                storeInsights(
                        storeObjectMetrics(campaign, LocalDate.now()),
                        LocalDate.parse("2017-10-20"),
                        LocalDate.parse("2017-10-26"),
                        getActionAttributionWindows(
                                VALUE_1D_CLICK, VALUE_1D_VIEW,
                                VALUE_7D_CLICK, VALUE_7D_VIEW,
                                VALUE_28D_CLICK, VALUE_28D_VIEW
                        )
                        //getEnumBreakdowns(
                        //        //AdsInsights.EnumBreakdowns.VALUE_COUNTRY,
                        //        //AdsInsights.EnumBreakdowns.VALUE_IMPRESSION_DEVICE,
                        //        //AdsInsights.EnumBreakdowns.VALUE_REGION,
                        //        AdsInsights.EnumBreakdowns.VALUE_PRODUCT_ID
                        //),
                        //S3_PREFIX

                );


                pcacheInsightsCampaigns.put(campaignId, campaignId);

            } else {

                LOG.info("skipping existing " + campaignId);


            }

        }



        for(String campaignId:pcacheAdSets.values()) {


            if (!pcacheInsightsAdSets.containsKey(campaignId)) {

                Campaign campaign = new Campaign(campaignId, context);
                // we need basic Campaign object as we fetch it with complete fields inside storeObjectMetrics()


                storeInsights(
                        storeObjectMetrics(campaign, LocalDate.now()),
                        LocalDate.parse("2017-10-20"),
                        LocalDate.parse("2017-10-26"),
                        getActionAttributionWindows(
                                VALUE_1D_CLICK, VALUE_1D_VIEW,
                                VALUE_7D_CLICK, VALUE_7D_VIEW,
                                VALUE_28D_CLICK, VALUE_28D_VIEW
                        )


                );


                pcacheInsightsAdSets.put(campaignId, campaignId);

            } else {

                LOG.info("skipping existing " + campaignId);


            }

        }

        for(String adId:pcacheAds.values()) {


            if (!pcacheInsightsAds.containsKey(adId)) {

                Ad ad = new Ad(adId, context);
                // we need basic Campaign object as we fetch it with complete fields inside storeObjectMetrics()


                storeInsights(
                        storeObjectMetrics(ad, LocalDate.now()),
                        LocalDate.parse("2017-10-20"),
                        LocalDate.parse("2017-10-26"),
                        getActionAttributionWindows(
                                VALUE_1D_CLICK, VALUE_1D_VIEW,
                                VALUE_7D_CLICK, VALUE_7D_VIEW,
                                VALUE_28D_CLICK, VALUE_28D_VIEW
                        )
                        //getEnumBreakdowns(
                        //        //AdsInsights.EnumBreakdowns.VALUE_COUNTRY,
                        //        //AdsInsights.EnumBreakdowns.VALUE_IMPRESSION_DEVICE,
                        //        //AdsInsights.EnumBreakdowns.VALUE_REGION,
                        //        AdsInsights.EnumBreakdowns.VALUE_PRODUCT_ID
                        //),
                        //S3_PREFIX

                );


                pcacheInsightsAds.put(adId, adId);

            } else {

                LOG.info("skipping existing " + adId);


            }

        }




    }



    List<AdsInsights.EnumActionAttributionWindows> getActionAttributionWindows(

            AdsInsights.EnumActionAttributionWindows... args

    ) {


      /*
      public static enum EnumActionAttributionWindows {
      VALUE_1D_VIEW("1d_view"),
      VALUE_7D_VIEW("7d_view"),
      VALUE_28D_VIEW("28d_view"),
      VALUE_1D_CLICK("1d_click"),
      VALUE_7D_CLICK("7d_click"),
      VALUE_28D_CLICK("28d_click"),
      */


        List<AdsInsights.EnumActionAttributionWindows> list = Arrays.asList(args);

        return list;
    }




    List<AdsInsights.EnumBreakdowns> getEnumBreakdowns(AdsInsights.EnumBreakdowns... args ){
        /*
        breakdowns
        list<enum{age, country, dma, gender, frequency_value, hourly_stats_aggregated_by_advertiser_time_zone, 
                  hourly_stats_aggregated_by_audience_time_zone, impression_device, place_page_id, publisher_platform, 
                  platform_position, device_platform, product_id, region}>


        */
        List<AdsInsights.EnumBreakdowns> list = Arrays.asList(args);

        return list;
    }





    String getS3Path(PathElement el){

        // building following path
        // s3://bucket/accounts/accountId=1234/campaigns/campaignId=12345/date=2017-10-19/adsetId=4567/adId=89/date=2017-10-89/attributionWindow=1d/metrics.json (edited)

       StringBuilder sb = new StringBuilder();

      if(el.getPrefix()!=null)
           sb.append(el.getPrefix());

      sb.append(String.format("/accounts/accountId=%s/campaigns/campaignId=%s",
                el.getAccountId(),el.getCampaignId()));

      if(el.getAddsetId()!=null)
          sb.append(String.format("/adsetId=%s",el.addsetId));

      if(el.getAdId()!=null)
            sb.append(String.format("/adId=%s",el.getAdId()));

      sb.append(String.format("/date=%s/%s.json"
              ,el.getDate1(),el.getName()));



      return sb.toString();


    }


    class PathElement{

        private String name;
        private String bucket;
        private String prefix;
        private String accountId;
        private String campaignId;
        private String date1;
        private String date2;
        private String addsetId;
        private String adId;
        private String attribWindow;

        public String getAccountId() {
            return accountId;
        }

        public PathElement setAccountId(String accountId) {
            this.accountId = accountId;
            return this;
        }

        public String getCampaignId() {
            return campaignId;
        }

        public PathElement setCampaignId(String campaignId) {
            this.campaignId = campaignId;
            return this;
        }

        public String getDate1() {
            return date1;
        }

        public PathElement setDate1(String date1) {
            this.date1 = date1;
            return this;
        }

        public String getDate2() {
            return date2;
        }

        public PathElement setDate2(String date2) {
            this.date2 = date2;
            return this;
        }

        public String getAddsetId() {
            return addsetId;
        }

        public PathElement setAddsetId(String addsetId) {
            this.addsetId = addsetId;
            return this;
        }

        public String getAdId() {
            return adId;
        }

        public PathElement setAdId(String adId) {
            this.adId = adId;
            return this;
        }

        public String getAttribWindow() {
            return attribWindow;
        }

        public PathElement setAttribWindow(String attribWindow) {
            this.attribWindow = attribWindow;
            return this;
        }

        public String getBucket() {
            return bucket;
        }

        public PathElement setBucket(String bucket) {
            this.bucket = bucket;
            return this;
        }

        public String getPrefix() {
            return prefix;
        }

        public PathElement setPrefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        public String getName() {
            return name;
        }

        public PathElement setName(String name) {
            this.name = name;
            return this;
        }
    }



   void storeInsights(Campaign campaign
                     ,LocalDate ldFrom,LocalDate ldTo
                     ,List<AdsInsights.EnumActionAttributionWindows> attributionwindows
                    //List<AdsInsights.EnumBreakdowns> breakdowns
   ) throws Exception{


                /*

                example requesting insights

                    node.getInsights
      .setBreakdowns(data.breakdowns)
      .setDatePreset(data.datePreset.toString)
      .setFields(data.fields)
      .setActionAttributionWindows(data.attributionWindow)
      .setTimeIncrement(data.timeIncrement)

               */

       //if(pcacheInsightsCampaigns.containsKey(campaign.getFieldId()))return;

       String timeRange = String.format("{'since':%s,'until':%s}",ldFrom.toString(),ldTo.toString());
       LOG.info("+++ time_range {}",timeRange);



        JsonObject value =
                   campaign
                           .getInsights()
                           .setActionAttributionWindows(attributionwindows)
                           .setDatePreset(AdsInsights.EnumDatePreset.VALUE_LAST_7D) // last 30 days by default.

                           // A single time range object. UNIX timestamp not supported. This param is ignored if time_ranges is provided.
                           // {'since':YYYY-MM-DD,'until':YYYY-MM-DD}
                           //.setTimeRange(timeRange)
                           //
                           //.setActionReportTime(

                           /*
                           action_breakdowns
                           list<enum{action_canvas_component_name, action_carousel_card_id, action_carousel_card_name, action_destination,
                           action_device, action_link_click_destination, action_reaction, action_target_id, action_type, action_video_sound,
                           action_video_type}>
                           */
                           //.setActionBreakdowns()
                           //

                           /*
                           breakdowns
                            list<enum{age, country, dma, gender, frequency_value, hourly_stats_aggregated_by_advertiser_time_zone,
                            hourly_stats_aggregated_by_audience_time_zone, impression_device, place_page_id, publisher_platform,
                            platform_position, device_platform, product_id, region}>

                            How to break down the result. For more than one breakdown, only certain combinations are available
                           */
                           //.setBreakdowns(breakdowns)

                           //.setDefaultSummary()
                           /*

                           level
                           enum {ad, adset, campaign, account}
                           */
                           .setLevel(AdsInsights.EnumLevel.VALUE_CAMPAIGN)

                           /*
                           Time increment - Default value: all_days
                           After you pick a reporting period by using time_range or date_preset, you may choose to have the results
                           for the whole period, or have results for smaller time slices.
                           If "all_days" is used, it means one result set for the whole period.
                           If "monthly" is used, you will get one result set for each calendar month in the given period.
                           */
                           //.setTimeIncrement()
                           .setDefaultSummary(true)
                           .requestFields(INSIGHTS_REQUEST_FIELDS)
                           .execute()
                           .getRawResponseAsJsonObject();

           if(value.has("data") && value.get("data").isJsonArray() && value.get("data").getAsJsonArray().size()>0) {  // do not store empty.

               String dateStart = value.getAsJsonArray("data").get(0).getAsJsonObject().get("date_start").getAsString();
               String dateStop =  value.getAsJsonArray("data").get(0).getAsJsonObject().get("date_stop").getAsString();
               String accountId = value.getAsJsonArray("data").get(0).getAsJsonObject().get("account_id").getAsString();


               String jsonString = gson.toJson(value);
               LOG.info("+++Insight << {}", jsonString);


               PathElement pe = new PathElement()
                       .setName(String.format("insights[%s-%s]_%s",dateStart,dateStop,campaign.getId()))
                       .setBucket(this.S3_BUCKET)
                       .setPrefix(S3_PREFIX)
                       .setAccountId(accountId)
                       .setCampaignId(campaign.getFieldId())
                       //.setAddsetId()
                       //.setAdId()
                       .setDate1(dateStart)
                       .setDate2(dateStop);

               s3.awsStore(getS3Path(pe), jsonString);

               if(false) { // shows how to process pagination. but there is no further
                   JsonObject insights2 = campaign.getInsights().parseResponse(jsonString).getRawResponseAsJsonObject();
                   String jsonString2 = gson.toJson(insights2);
                   LOG.info("+++pagination << {}", jsonString2);
               }

           }

    }


    void storeInsights(AdSet adset
                       ,LocalDate ldFrom,LocalDate ldTo
                       ,List<AdsInsights.EnumActionAttributionWindows> aw
    ) throws Exception{


        String timeRange = String.format("{'since':%s,'until':%s}",ldFrom.toString(),ldTo.toString());
        LOG.info("+++ time_range "+timeRange);




            JsonObject value =
                    adset
                            .getInsights()
                            .setActionAttributionWindows(aw)
                            .setLevel(AdsInsights.EnumLevel.VALUE_ADSET)
                            .setDatePreset(AdsInsights.EnumDatePreset.VALUE_LAST_7D)
                            //.setTimeRange(timeRange)
                            .requestFields(INSIGHTS_REQUEST_FIELDS)
                            .execute()
                            .getRawResponseAsJsonObject();






        if(value.has("data") && value.get("data").isJsonArray() && value.get("data").getAsJsonArray().size()>0) {  // do not store empty.

            JsonObject jo = value.get("data").getAsJsonArray().get(0).getAsJsonObject();
            String dateStart = jo.get("date_start").getAsString();
            String dateStop = jo.get("date_stop").getAsString();
            String accountId = jo.get("account_id").getAsString();


            String jsonString = gson.toJson(value);
            LOG.info("+++Insight << {}", jsonString);


            PathElement pe = new PathElement()
                    .setName(String.format("insights[%s_%s]_%s",dateStart,dateStop,adset.getId()))
                    .setBucket(this.S3_BUCKET)
                    .setPrefix(S3_PREFIX)
                    .setAccountId(accountId)
                    .setCampaignId(adset.getFieldCampaignId())
                    .setAddsetId(adset.getId())
                    //.setAdId()
                    .setDate1(ldFrom.toString())
                    .setDate2(ldTo.toString())
                    .setAttribWindow(aw.get(0).toString());

            s3.awsStore(getS3Path(pe), jsonString);

        }

        }


    void storeInsights(Ad ad
            ,LocalDate ldFrom,LocalDate ldTo
            ,List<AdsInsights.EnumActionAttributionWindows> aw

    ) throws Exception{

        LocalDate ld = LocalDate.now().minusDays(10);
        String timeRange = String.format("{'since':'%s','until':'%s'}",ld.toString(),ld.plusDays(10).toString());
        LOG.info("time_range "+timeRange);


            JsonObject value =
                    ad
                            .getInsights()
                            .setActionAttributionWindows(aw)
                            .setDatePreset(AdsInsights.EnumDatePreset.VALUE_LAST_7D)
                            //.setTimeRange(timeRange)
                            .requestFields(INSIGHTS_REQUEST_FIELDS)
                            .setLevel(AdsInsights.EnumLevel.VALUE_AD)
                            .execute()
                            .getRawResponseAsJsonObject();

        if(value.has("data") && value.get("data").isJsonArray() && value.get("data").getAsJsonArray().size()>0) {  // do not store empty.

            String dateStart = value.getAsJsonArray("data").get(0).getAsJsonObject().get("date_start").getAsString();
            String dateStop = value.getAsJsonArray("data").get(0).getAsJsonObject().get("date_stop").getAsString();
            String accountId = value.getAsJsonArray("data").get(0).getAsJsonObject().get("account_id").getAsString();

            String jsonString = gson.toJson(value);
            LOG.info("+++Insight << {}", jsonString);

            PathElement pe = new PathElement()
                    .setName(String.format("insights[%s_%s]_%s",dateStart,dateStop,ad.getId()))
                    .setBucket(this.S3_BUCKET)
                    .setPrefix(S3_PREFIX)
                    .setAccountId(accountId)
                    .setCampaignId(ad.getFieldCampaignId())
                    .setAddsetId(ad.getFieldAdsetId())
                    .setAdId(ad.getId())
                    .setDate1(ldFrom.toString())
                    .setDate2(ldTo.toString());


            s3.awsStore(getS3Path(pe), jsonString);
        }

        }




    Campaign storeObjectMetrics(Campaign campaign,LocalDate ld) throws Exception {

        //LocalDate ld = LocalDate.now().minusDays(10);
        String timeRange = String.format("{'since':'%s','until':'%s'}", ld.toString(), ld.plusDays(1).toString());
        LOG.info("+++ time_range " + timeRange);

        PathElement pe = new PathElement()
                    .setName("campaign_" + campaign.getId())
                    .setBucket(this.S3_BUCKET)
                    .setPrefix(S3_PREFIX)
                    .setAccountId(campaign.getFieldAccountId())
                    .setCampaignId(campaign.getFieldId())
                    //.setAddsetId(adset.getId())
                    //.setAdId(ad.getId())
                    .setDate1(ld.toString());
                    //.setAttribWindow(aw.get(0).toString());

        campaign = campaign.get().requestAllFields().execute();

        // TODO: Campaign having a statistics edge as well but it not available through this sdk?

        String jsonString = gson.toJson(campaign.getRawResponseAsJsonObject());

        s3.awsStore(getS3Path(pe), jsonString);

        return campaign;


    }

    AdSet storeObjectMetrics(AdSet adset,LocalDate ld) throws Exception {

        //LocalDate ld = LocalDate.now().minusDays(10);
        String timeRange = String.format("{'since':'%s','until':'%s'}", ld.toString(), ld.plusDays(1).toString());
        LOG.info("+++ time_range " + timeRange);

        adset = adset.get().requestAllFields().execute();

        String jsonString = gson.toJson(adset.getRawResponseAsJsonObject());

        PathElement pe = new PathElement()
                    .setName("adset_" + adset.getFieldId())
                    .setBucket(this.S3_BUCKET)
                    .setPrefix(S3_PREFIX)
                    .setAccountId(adset.getFieldAccountId())
                    .setCampaignId(adset.getFieldCampaignId())
                    .setAddsetId(adset.getId())
                    //.setAdId(ad.getId())
                    .setDate1(ld.toString());
            //.setAttribWindow(aw.get(0).toString());

            s3.awsStore(getS3Path(pe), jsonString);

            return adset;

    }

    Ad storeObjectMetrics(Ad ad,LocalDate ld) throws Exception {


        String timeRange = String.format("{'since':'%s','until':'%s'}", ld.toString(), ld.plusDays(1).toString());
        LOG.info("+++ time_range " + timeRange);

        ad = ad.get().requestAllFields().execute();

        String jsonString = gson.toJson(ad.getRawResponseAsJsonObject());


        PathElement pe = new PathElement()
                    .setName("ad_" + ad.getFieldCampaignId())
                    .setBucket(this.S3_BUCKET)
                    .setPrefix(S3_PREFIX)
                    .setAccountId(ad.getFieldAccountId())
                    .setCampaignId(ad.getFieldCampaignId())
                    .setAddsetId(ad.getId())
                    .setAdId(ad.getId())
                    .setDate1(ld.toString()
                    );

        s3.awsStore(getS3Path(pe), jsonString);

        return ad;

    }


       void getCampaigns(APIContext context) throws Exception {


           LOG.info("========= cache size == {}",pcacheCampaigns.size());

           if (pcacheCampaigns.size() >0) return;


           for (long accountId : Config.ACCOUNT_IDS) {

               //com.facebook.ads.sdk.APIException £££ {"error":{"message":"(#100) Tried accessing nonexisting field (accounts) on node type (Business)"

               AdAccount account = new AdAccount(accountId, context);

                APINodeList<Campaign> campaigns
                           = account
                           .getCampaigns()
                           .setEffectiveStatus("[\"ACTIVE\"]")
                           .requestAllFields()
                           .execute();


                 for (Campaign campaign : campaigns) {
                     
                       pcacheCampaigns.put(campaign.getFieldId(), campaign.getFieldId());


                 }




           }

       }



    void getAdSets(APIContext context) throws Exception {


        LOG.info("========= cache size == {}",pcacheAdSets.size());

        if (pcacheCampaigns.size()==0 || pcacheAdSets.size() >0) return;


        for (String campaignId : pcacheCampaigns.values()) {

            Campaign campaign = new Campaign(campaignId, context);

           APINodeList<AdSet> adsets
                    = campaign
                    .getAdSets()
                    .requestIdField()
                    .setEffectiveStatus("[\"ACTIVE\"]")
                    .execute();


            for (AdSet adset : adsets) {


                pcacheAdSets.put(adset.getFieldId(), adset.getFieldId());

            }




        }

    }



void getAds(APIContext context) throws Exception {


    LOG.info("========= cache size == "+pcacheAds.size());

    if (pcacheAdSets.size()==0 || pcacheAds.size() >0) return;


    for (String adsetId : pcacheAdSets.values()) {

        AdSet adset = new AdSet(adsetId, context);

        APINodeList<Ad> ads
                = adset
                .getAds()
                .requestIdField()
                .setEffectiveStatus("[\"ACTIVE\"]")
                .execute();


        for (Ad ad : ads) {

            if(false)storeObjectMetrics(ad, LocalDate.now()); // we store this as part of insights.

            pcacheAds.put(ad.getFieldId(), ad.getFieldId());

        }




    }

}



    }





