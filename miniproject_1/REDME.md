
- Build without running tests

mvn build -DskipTests

- run junit tests from commandline

export CLASSPATH=./target/miniproject_1-0.0-tests.jar:./target/miniproject_1-0.0.jar:/Users/robertspolis/.m2/repository/junit/junit/3.8.2/junit-3.8.2.jar

java -Xmx2G -Xms2G –XX:+UseG1GC junit.textui.TestRunner edu.coursera.parallel.ReciprocalArraySumTest

java -Xmx2G -Xms2G -XX:+UseG1GC -XX:+UnlockCommercialFeatures -XX:+FlightRecorder -XX:StartFlightRecording=name=FJtest,filename=/tmp/FJtest.jfr,settings=profile,dumponexit=true -cp ./target/miniproject_1-0.0-tests.jar:./target/miniproject_1-0.0.jar:/Users/robertspolis/.m2/repository/junit/junit/3.8.2/junit-3.8.2.jar junit.textui.TestRunner   edu.coursera.parallel.ReciprocalArraySumTest


# VM settings #
-Xmx2G -Xms2G -XX:+UseG1GC -XX:MaxGCPauseMillis=200

-XX:+UnlockCommercialFeatures -XX:+FlightRecorder -XX:StartFlightRecording=name=FJtest,filename=/tmp/FJtest.jfr,settings=profile,dumponexit=true