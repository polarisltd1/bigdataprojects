name := "myapp-scala"

version := "1.0"

scalaVersion := "2.11.8"

//resolvers += "spray repo" at "http://repo.spray.io"

//resolvers += "spray nightlies" at "http://nightlies.spray.io"

libraryDependencies ++= Seq(
  // https://mvnrepository.com/artifact/net.minidev/json-smart
//  "net.minidev" % "json-smart" % "1.0.9",
//  "com.novocode"        % "junit-interface"  % "0.7"          % "test->default",
//  "org.scalautils" % "scalautils_2.10" % "2.0",
  "org.scalatest" % "scalatest_2.11" % "3.0.1" % "test"
)

scalacOptions ++= Seq(
  "-unchecked",
  "-deprecation",
  "-Xlint",
  "-Ywarn-dead-code",
  "-language:_",
  "-target:jvm-1.8",
  "-encoding", "UTF-8"
)