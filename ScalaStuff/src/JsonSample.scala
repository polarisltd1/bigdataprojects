



//import scala.io.Source._

//val lines = fromFile("siphon.json").getLines

// http://www.furidamu.org/blog/2012/09/18/beautiful-json-parsing-in-scala/


import JSON._

object Test extends App {
  print("Fetching recent data ...")
  val json = io.Source.fromURL("http://liquid8002.untergrund.net/infinigag/").mkString
  println(" done\n")

  val tree = parseJSON(json)

  println("next page: %d\n".format(tree.attributes.next.toInt))
  println("=== Top %d images from 9gag ===".format(tree.images.length))

  tree.images.foreach {
    case image =>
      println("%s: %s".format(image.title, image.image.thumb))
  }
}