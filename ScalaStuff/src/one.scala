
import java.time._

object One {
  def main(args: Array[String]) {



    def genCmd(dateFrom:LocalDate, dateTo:LocalDate, cmdString:String) = {

      println("#-------------------------------   "+cmdString+" -------------------------------------")
      println()
      println()

      // taken out / keep for later.
      // --driver-java-options "-XX:+UseG1GC -XX:+PrintFlagsFinal -XX:+PrintGC -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -Dorg.apache.log4j.Level=INFO -Dlog4j.configuration=./log4j.properties" \
      //   --conf "spark.driver.extraJavaOptions=-Dspring.profiles.active=emr -Dlog4j.configuration=file:`pwd`/log4j.properties  -Ddm.logging.level=INFO" \


      val cmdStr =
        """
#!/bin/bash
  cmd(){

  spark-submit \
  --master "yarn" \
  --driver-memory 8g \
  --deploy-mode client \
  --class com.tumra.datafeeds.ingest.Application \
  ../TumraDataFeeds-1.0.4-SNAPSHOT-jar-with-dependencies.jar $*
  }

        """.stripMargin

      println(cmdStr)

      var date = dateFrom;

      while(date.isBefore(dateTo)){
        println(cmdString.format(date))
        date = date.plusDays(1)
      }




    }





    genCmd(LocalDate.of(2015,7,27),LocalDate.of(2016,12,31),"cmd -v DCMv1 -d NetworkImpression --ingest-date %s --allowed-lateness-dates 1")

    genCmd(LocalDate.of(2015,7,27),LocalDate.of(2016,12,31),"cmd -v DCMv1 -d NetworkClick --ingest-date %s --allowed-lateness-dates 1")






    genCmd(LocalDate.of(2016,11,25),LocalDate.of(2017,2,10),"cmd -v DCM -d click --ingest-date %s --allowed-lateness-dates 1")


    genCmd(LocalDate.of(2015,8,10),LocalDate.of(2016,12,31),"cmd -v DCMv1 -d NetworkActivity --ingest-date %s --allowed-lateness-dates 1")






    genCmd(LocalDate.of(2017,2,5),LocalDate.of(2017,2,6),"cmd -v APPNEXUS -d reference --ingest-date %s -Ddev=false")


    genCmd(LocalDate.of(2016,12,10),LocalDate.of(2017,2,11),"cmd -v DCM -d activity --ingest-date %s --allowed-lateness-dates 1")

    genCmd(LocalDate.of(2016,11,25),LocalDate.of(2017,2,11),"cmd -v DCM -d impression --ingest-date %s --allowed-lateness-dates 1")

    genCmd(LocalDate.of(2017,2,9),LocalDate.of(2017,2,17),"cmd -v APPNEXUS -d standard_feed --ingest-date %s --allowed-lateness-dates 0")

    genCmd(LocalDate.of(2017,2,6),LocalDate.of(2017,2,16),"cmd -v APPNEXUS -d segment_feed --ingest-date %s --allowed-lateness-dates 5")


  }
}



