
import scala.xml._





val someXMLInAString = """
<sammich>
<bread>wheat</bread>
211
<meat>salami</meat>
<condiments>
<condiment expired="true">mayo</condiment>
<condiment expired="false">mustard</condiment>
</condiments>
</sammich>
                       """

val someXMLa = XML.loadString(someXMLInAString)
assert(someXMLa.isInstanceOf[scala.xml.Elem])




val someXMLb =
  <sammich>
    <bread>wheat</bread>
    <meat>salami</meat>
    <condiments>
      <condiment expired="true">mayo</condiment>
      <condiment expired="false">mustard</condiment>
    </condiments>
  </sammich>

assert(someXMLb.isInstanceOf[scala.xml.Elem])


val s2 =  someXMLb \ "bread"


(someXMLb \ "bread").text

someXMLb \ "condiment"

// The \ function doesn’t descend into child elements of an XML
// structure. To do that, we use its sister function, \\ (two backslashes):

(someXMLb \\ "condiment")(0) \ "@expired"

for (condiment <- (someXMLb \\ "condiment")) {
  if ((condiment \ "@expired").text == "true")
    println("the " + condiment.text + " has expired!")
}
