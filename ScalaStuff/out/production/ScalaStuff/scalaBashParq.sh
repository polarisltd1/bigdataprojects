#!/bin/sh
exec scala "$0" "$@"
!#

import java.time._

def genCmd(dateFrom:LocalDate, dateTo:LocalDate, cmdString:String) = {

      println("#-------------------------------   "+cmdString+" -------------------------------------")
      println()
      println()

      val cmdStr =
        """
#!/bin/bash
  cmd(){

  spark-submit \
  --master "yarn" \
  --driver-memory 8g \
  --deploy-mode client \
  --conf "spark.driver.extraJavaOptions=-Dspring.profiles.active=emr -Dlog4j.configuration=file:`pwd`/log4j.properties  -Ddm.logging.level=DEBUG" \
  --class com.tumra.datafeeds.ingest.Application \
  --driver-java-options "-XX:+UseG1GC -XX:+PrintFlagsFinal -XX:+PrintGC -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -Dorg.apache.log4j.Level=DEBUG -Dlog4j.configuration=./log4j.properties" \
  ../TumraDataFeeds-1.0.2-SNAPSHOT-jar-with-dependencies.jar $*
  }

        """.stripMargin

      println(cmdStr)

      var date = dateFrom;

      while(date.isBefore(dateTo)){
        println(cmdString.format(date))
        date = date.plusDays(1)
      }




    }




    genCmd(LocalDate.of(2015,7,31),LocalDate.of(2016,12,31),"cmd -v DCMv1 -d NetworkActivity --ingest-date %s --allowed-lateness-dates 1")

    genCmd(LocalDate.of(2015,7,27),LocalDate.of(2016,12,31),"cmd -v DCMv1 -d NetworkImpression --ingest-date %s --allowed-lateness-dates 1")

    genCmd(LocalDate.of(2015,7,27),LocalDate.of(2016,12,31),"cmd -v DCMv1 -d NetworkClick --ingest-date %s --allowed-lateness-dates 1")



    genCmd(LocalDate.of(2016,11,24),LocalDate.of(2017,2,11),"cmd -v DCM -d impression --ingest-date %s --allowed-lateness-dates 1")


    genCmd(LocalDate.of(2016,12,2),LocalDate.of(2017,2,11),"cmd -v DCM -d activity --ingest-date %s --allowed-lateness-dates 1")

    genCmd(LocalDate.of(2016,11,25),LocalDate.of(2017,2,10),"cmd -v DCM -d click --ingest-date %s --allowed-lateness-dates 1")



