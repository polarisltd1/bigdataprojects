/**
  * This is working example decoding Json in a most simplest and efficient matter
  * as is using beans aka case class to embed decoded data and uses DSL style path operation \
  * to retrieve Json elements. This example shows how to process list based json structure.
  * There is list into list so there is implicit construct used inside object with same name as case class.
  */

import play.api.libs.json._


object app extends App {


  val source = scala.io.Source.fromFile("/Users/robertspolis/java/bitbucket.polarisltd1/BigDataProjects/SparcJavaExamples/siphon_pretty.json")
  val text = try source.mkString finally source.close()

  val json = Json.parse(text)

  case class Split(part: String, status: String, checksum:String)

  object Split{
    implicit val splitsReader = Json.reads[Split]
  }

  case class Siphon(name: String, hour: String, timestamp: String, splits: List[Split])

  object Siphon {
    implicit val siphonReader = Json.reads[Siphon]
  }

  val list = (json \ "response" \ "siphons").as[List[Siphon]]

  list.foreach(println)

}
