
import play.api.libs.json._

/**
  * This is very basic example showing idea how Play framework Json library can be used.
  * References:
  * https://www.playframework.com/documentation/2.1.1/ScalaJson
  * https://gist.github.com/tdrozdowski/7348647
  */


object Main extends App {


  val jsonValue = Json.parse("""{"id": 1}""")
  val id = (jsonValue \ "id").as[Int]

  println (id)


  val jsonValue1 = Json.parse(
    """
    {
        "address": {
            "street": "mystreet",
            "city": "mycity"
        }
    }
    """)
  val id1 = (jsonValue1 \ "address" \ "city").as[String]


  val json: JsValue = Json.parse(
    """
{
  "user": {
    "name" : "toto",
    "age" : 25,
    "email" : "toto@jmail.com",
    "isAlive" : true,
    "friend" : {
  	  "name" : "tata",
  	  "age" : 20,
  	  "email" : "tata@coldmail.com"
    }
  }
}
    """)

  val jsonString: String = Json.stringify(json)
  println(jsonString)

  //val name: JsValue = json \ "user" \ "name"
  //println(name)

  val name1: String = (json \ "user" \ "name").as[String]
  println(name1)

  val maybeName: Option[String] = (json \ "user" \ "name").asOpt[String]

///////
// construct a json
val user = Json.obj("email" -> "foo@bar.com", "firstName" -> "Foo", "lastName" -> "Bar")


  // > user: play.api.libs.json.JsObject = {"email":"foo@bar.com","firstName":"Foo","lastName":"Bar"}
  println(s"user -> $user")
  // > user -> {"email":"foo@bar.com","firstName":"Foo","lastName":"Bar"}

  // get just the email from the JsValue
  val email = (user \ "email").asOpt[String].getOrElse("No email!")
  // > email: String = foo@bar.com
  println(s"email -> $email")

  /////////////

  // create a case class to serialize to
  case class User(email : String, firstName : String, lastName :String, middleName : Option[String])
  // define a formats using JSON 'inception'
  implicit val userFormats = Json.format[User]


  // ask for an instance of the case class
  val userFromJson = Json.fromJson(user)


  // > userFromJson: play.api.libs.json.JsResult[User] = JsSuccess(User(foo@bar.com,Foo,Bar,None),)
  println(s"user class ->  $userFromJson")




}
