/**
  * This is example on how to decode Json with Play library containing list structure.
  * Second way seems weird. What exactly it do? Its related to play.api.libs.functional.syntax
  */
object Main2 extends App {


  import play.api.libs.json._

  import play.api.libs.functional.syntax._

  val json = Json.parse("""{"people": [ {"name":"Jack", "age": 19}, {"name": "Tony", "age": 26} ] }""")

  case class People(name: String, age: Int)

  implicit val peopleReader = Json.reads[People]

  val peoples = (json \ "people").as[List[People]]

  peoples.foreach(println)

//////



  implicit val personReader: Reads[(String, Int)] = (
    (__ \ "name").read[String] and
      (__ \ "age").read[Int]
    ).tupled
  val peoples2 = (json \ "people").as[List[(String, Int)]]
  peoples2.foreach(println)




}