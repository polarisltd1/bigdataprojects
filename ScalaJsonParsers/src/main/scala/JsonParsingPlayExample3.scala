/**
  * This is example to follow doc article
  * https://www.playframework.com/documentation/2.5.x/ScalaJsonCombinators
  * not yet finished.
  */
object Main3 extends App {


  import play.api.libs.json._

  import play.api.libs.functional.syntax._

  example3()



  def example3() = {


    val json: JsValue = Json.parse("""
{
  "name" : "Watership Down",
  "location" : {
    "lat" : 51.235685,
    "long" : -1.309197
  },
  "residents" : [ {
    "name" : "Fiver",
    "age" : 4,
    "role" : null
  }, {
    "name" : "Bigwig",
    "age" : 6,
    "role" : "Owsla"
  } ]
}
                                   """)




    // next shows examples of paths
    //
    val latPath = JsPath \ "location" \ "lat"   // this is /location/lat

    // Recursive path
    val namesPath = JsPath \\ "name"     // this is  //name

    // Indexed path
    val firstResidentPath = (JsPath \ "residents")(0)   // this is /residents(0)

    val longPath = __ \ "location" \ "long"        // this is /location/long





    val one = latPath.read[String]

    val nameReads: Reads[String] = (JsPath \ "name").read[String]


    println(one)
    println(nameReads)



  }


}