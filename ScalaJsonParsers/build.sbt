name := "ScalaJsonParsers"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
//  "com.typesafe.play" % "play-json_2.12" % "2.6.0-M3"
  "com.typesafe.play" % "play-ws_2.11" % "2.5.12"
//
)

// https://mvnrepository.com/artifact/com.typesafe.play/play-ws_2.11
libraryDependencies += "com.typesafe.play" % "play-ws_2.11" % "2.6.0-M1"
