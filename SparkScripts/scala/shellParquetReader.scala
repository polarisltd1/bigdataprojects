
/*




:load PATH_TO_FILE

or

spark-shell -i file.scala

*/

/*
import org.apache.spark.sql.*
   ssc = new SQLContext(sc)
   ssc.sql("select * from mytable").collect
*/
val sqlContext = new org.apache.spark.sql.SQLContext(sc)
/*

Robertss-MBP:s3prod robertspolis$ ls -la /Users/robertspolis/java/bitbucket.polarisltd1/TumraDataFeeds/mae-data-feed-dev/base/APPNEXUS/base_APPNEXUS_STANDARD_FEED
total 8
drwxr-xr-x   5 robertspolis  staff  170 Feb  7 13:04 .
drwxr-xr-x   3 robertspolis  staff  102 Feb  7 12:36 ..
-rw-r--r--   1 robertspolis  staff    8 Feb  7 13:04 ._SUCCESS.crc
drwxr-xr-x  10 robertspolis  staff  340 Feb  7 13:04 DeliveryDateID=2017-02-05
-rw-r--r--   1 robertspolis  staff    0 Feb  7 13:04 _SUCCESS
*/
df = sqlContext.read.parquet("/Users/robertspolis/java/bitbucket.polarisltd1/TumraDataFeeds/mae-data-feed-dev/base/APPNEXUS/base_APPNEXUS_STANDARD_FEED/DeliveryDateID=2017-02-05/")

df.printSchema()
/*
root
 |-- BatchLoadId: long (nullable = true)
 |-- Vendor: string (nullable = true)
 |-- VendorDataType: string (nullable = true)
 |-- AuctionID64: long (nullable = true)
 |-- Datetime: timestamp (nullable = true)
 |-- UserTzOffset: integer (nullable = true)
 |-- Width: integer (nullable = true)
 |-- Height: integer (nullable = true)
 |-- MediaType: string (nullable = true)
 |-- FoldPosition: string (nullable = true)
 |-- EventType: string (nullable = true)
 |-- ImpType: integer (nullable = true)
 |-- PaymentType: integer (nullable = true)
 |-- MediaCostDollarsCPM: double (nullable = true)
 |-- RevenueType: integer (nullable = true)
 |-- BuyerSpend: double (nullable = true)
 |-- BuyerBid: double (nullable = true)
 |-- ECP: double (nullable = true)
 |-- EAP: double (nullable = true)
 |-- IsImp: integer (nullable = true)
 |-- IsLearn: integer (nullable = true)
 |-- PredictTypeRev: integer (nullable = true)
 |-- UserID64: long (nullable = true)
 |-- IpAddress: string (nullable = true)
 |-- IpAddressTrunc: string (nullable = true)
 |-- GeoCountry: string (nullable = true)
 |-- GeoRegion: string (nullable = true)
 |-- OperatingSystem: integer (nullable = true)
 |-- Browser: integer (nullable = true)
 |-- Language: integer (nullable = true)
 |-- VenueID: integer (nullable = true)
 |-- SellerMemberID: integer (nullable = true)
 |-- PublisherID: integer (nullable = true)
 |-- SiteID: integer (nullable = true)
 |-- SiteDomain: string (nullable = true)
 |-- TagID: integer (nullable = true)
 |-- ExternalInvID: integer (nullable = true)
 |-- ReservePrice: double (nullable = true)
 |-- SellerRevenueCPM: double (nullable = true)
 |-- MediaBuyRevSharePct: double (nullable = true)
 |-- PubRuleID: integer (nullable = true)
 |-- SellerCurrency: string (nullable = true)
 |-- PublisherCurrency: string (nullable = true)
 |-- PublisherExhangeRate: double (nullable = true)
 |-- ServingFeesCPM: double (nullable = true)
 |-- ServingFeesRevShare: double (nullable = true)
 |-- BuyerMemberID: integer (nullable = true)
 |-- AdvertiserID: integer (nullable = true)
 |-- BrandID: integer (nullable = true)
 |-- AdvertiserFrequency: integer (nullable = true)
 |-- AdvertiserRecency: integer (nullable = true)
 |-- InsertionOrderID: integer (nullable = true)
 |-- CampaignGroupID: integer (nullable = true)
 |-- CampaignID: integer (nullable = true)
 |-- CreativeID: integer (nullable = true)
 |-- CreativeFreq: integer (nullable = true)
 |-- CreativeRec: integer (nullable = true)
 |-- CadenceModifier: double (nullable = true)
 |-- CanConvert: integer (nullable = true)
 |-- UserGroupID: integer (nullable = true)
 |-- IsControl: integer (nullable = true)
 |-- ControllerPct: double (nullable = true)
 |-- ControllerCreativePct: integer (nullable = true)
 |-- IsClick: integer (nullable = true)
 |-- PixelID: integer (nullable = true)
 |-- IsRemarketing: integer (nullable = true)
 |-- PostClickConv: integer (nullable = true)
 |-- PostViewConv: integer (nullable = true)
 |-- PostClickRevenue: double (nullable = true)
 |-- PostViewRevenue: double (nullable = true)
 |-- OrderID: string (nullable = true)
 |-- ExternalData: string (nullable = true)
 |-- PricingType: string (nullable = true)
 |-- BookedRevenueDollars: double (nullable = true)
 |-- BookedRevenueAdvCurr: double (nullable = true)
 |-- CommissionCPM: double (nullable = true)
 |-- CommissionRevShare: double (nullable = true)
 |-- AuctionServiceDeduction: double (nullable = true)
 |-- AuctionServiceFees: double (nullable = true)
 |-- CreativeCoverageFees: double (nullable = true)
 |-- ClearFees: double (nullable = true)
 |-- BuyerCurrency: string (nullable = true)
 |-- AdvertiserCurrency: string (nullable = true)
 |-- AdvertiserExchangeRate: double (nullable = true)
 |-- Latitude: string (nullable = true)
 |-- Longitude: string (nullable = true)
 |-- DeviceUniqueID: string (nullable = true)
 |-- DeviceID: integer (nullable = true)
 |-- CarrierID: integer (nullable = true)
 |-- DealID: integer (nullable = true)
 |-- ViewResult: integer (nullable = true)
 |-- Application: string (nullable = true)
 |-- SupplyType: integer (nullable = true)
 |-- SDKVersion: string (nullable = true)
 |-- OzoneID: integer (nullable = true)
 |-- BillingPeriodID: integer (nullable = true)
 |-- ViewNonMeasurableReason: integer (nullable = true)
 |-- ExternalUID: string (nullable = true)
 |-- RequestUUID: string (nullable = true)
 |-- GeoDMA: integer (nullable = true)
 |-- GeoCity: integer (nullable = true)
 |-- MobileAppInstanceID: integer (nullable = true)
 |-- TrafficSourceCode: string (nullable = true)
 |-- ExternalRequestID: string (nullable = true)
 |-- DealType: integer (nullable = true)
 |-- YmFloorID: integer (nullable = true)
 |-- YmBiasID: integer (nullable = true)
 |-- IsFilteredRequest: integer (nullable = true)
 |-- Age: integer (nullable = true)
 |-- Gender: string (nullable = true)
 |-- IsExclusive: integer (nullable = true)
 |-- BidPriority: integer (nullable = true)
 |-- CustomModelID: integer (nullable = true)
 |-- CustomModelLastModified: integer (nullable = true)
 |-- LeafName: string (nullable = true)
 |-- DataCostsCPM: double (nullable = true)
 |-- MediaTypeOrig: integer (nullable = true)
 |-- FoldPositionOrig: integer (nullable = true)
 |-- GeoCountryOrig: string (nullable = true)
 |-- GeoRegionOrig: string (nullable = true)
 |-- CampaignIDOrig: integer (nullable = true)
 |-- CreativeIDOrig: integer (nullable = true)
 |-- SiteDomainOrig: string (nullable = true)
 |-- PixelIDOrig: integer (nullable = true)
 |-- OrderIDOrig: string (nullable = true)
 |-- ViewResultOrig: integer (nullable = true)
 |-- DeliveryDateID: date (nullable = true)
 */
 
 
 df.createOrReplaceTempView("addnexus_1")

 spark.sql("select count(*) from addnexus_1").show();

/*
17/02/07 14:08:23 WARN Utils: Truncated the string representation of a plan since it was too large. This behavior can be adjusted by setting 'spark.debug.maxToStringFields' in SparkEnv.conf.
17/02/07 14:08:24 WARN CorruptStatistics: Ignoring statistics because created_by could not be parsed (see PARQUET-251): parquet-mr (build 32c46643845ea8a705c35d4ec8fc654cc8ff816d)
*/





