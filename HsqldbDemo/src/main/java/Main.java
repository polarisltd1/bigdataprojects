import com.sun.tools.javap.TypeAnnotationWriter;
import org.hsqldb.jdbcDriver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.stream.IntStream;

/**
 * Created by robertspolis on 01/07/2017.
 *

 starting inmemory server

 java -cp lib/hsqldb.jar org.hsqldb.Server -databe.0 mem:testdb --dbname.0 testdb

 notice this line, it tells database having alias:
 [Server@1f32e575]: Database [index=0, id=0, db=mem:testdb, alias=testdb] opened successfully in 377 ms.


 connect to it via jdbc

 url
 jdc:hsqldb:hsql://localhost/testdb
 or
 jdbc:hsqldb:hsql://localhost:9001/testdb

driver
 org.hsqldb.jdbc.JDBCDriver

 starting server manager

 java -cp lib/hsqldb.jar org.hsqldb.util.DatabaseManagerSwing


 ========================== information ===================


 https://stackoverflow.com/questions/1497855/how-to-run-a-hsqldb-server-in-memory-only-mode

 How do I run a memory-only HSQLDB server?

 java -cp .\hsqldb-1.8.0.10.jar org.hsqldb.Server -database.0 mem:aname

 In memory mode is specified by the connection url - so if you want, you can just have a server.properties file in the same directory, and set the connection url to use the mem protocol - or if you are using hsqldb in another application that allows you to specify the connection url such as jdbc, specify jdbc:hsqldb:mem:aname.



 // start the server
 Server server = new Server();
 server.setDatabaseName(0, "mainDb");
 server.setDatabasePath(0, "mem:mainDb");
 server.setDatabaseName(1, "standbyDb");
 server.setDatabasePath(1, "mem:standbyDb");
 server.setPort(9001); // this is the default port
 server.start();


 // connect to it.
 String url="jdbc:hsqldb:hsql://192.168.5.1:9001/mainDb";
 Class.forName("org.hsqldb.jdbc.JDBCDriver");
 Connection conn = DriverManager.getConnection(url, "SA", "");


 // DatabaseManagerSwing

 java -cp hsqldb-x.x.x.jar org.hsqldb.util.DatabaseManagerSwing
 Select "HSQL Database Engine Server" from the Type drop down and give "jdbc:hsqldb:hsql://192.168.5.1:9001/mainDb" as the URL. This will connect you to the remote HSQL in-memory Server instance.

creating schema
---------------
 sql> CREATE SCHEMA STACKOVERFLOW;
 sql> SET SCHEMA STACKOVERFLOW;
 sql> CREATE TABLE SO2406470 (pk1 INT NOT NULL, pk2 INT NOT NULL, data VARCHAR(64), PRIMARY KEY(pk1, pk2));
 sql> \d SO2406470

=================

 remote db takes 50 sec.
 local db takes 10 sec.


 */
public class Main {

    String TABLE_NAME = "TEST2";
    Connection db = null;
    String URL1 = "jdbc:hsqldb:mem://localhost:9001/aname"; // embedded database.
    String URL2="jdbc:hsqldb:mem:.";  // embedded database.
    String URL3 = "jdbc:hsqldb:hsql://localhost:9001/testdb"; // real remote database!

    /*
    static {
        try {
            DriverManager.registerDriver(new jdbcDriver());
        } catch (Exception e) {}
    }
*/


public static void main(String[] args)throws Exception{

    new Main().execute();
}


  void execute()throws Exception{
      connectDb(URL1);
      dropTable();
      createTable();
      System.out.println(LocalTime.now());
      IntStream.range(1, 1_000_000).forEach(i->insertRow(i));
      System.out.println(LocalTime.now());

  }

  void  connectDb(String url)throws Exception{
      // connect to it.
      System.out.println("Connecting to: "+url);
      //Class.forName("org.hsqldb.jdbc.JDBCDriver");
      this.db = DriverManager.getConnection(url, "SA", "");
      System.out.println("OK");










    }


   void dropTable(){
       try {
           Statement stmt = db.createStatement ();
           stmt.executeUpdate ("DROP TABLE "+TABLE_NAME);
       }
       catch (SQLException e) {
           System.out.println("Exception: "+e.getMessage());
       }

   }
    void createTable(){
        try {
            Statement stmt = db.createStatement ();
            stmt.executeUpdate ("CREATE TABLE "+TABLE_NAME+"(time varchar(1000),duration integer)");
        }
        catch (SQLException e) {
            System.out.println("Exception: "+e.getMessage());
        }

    }

    void insertRow(Integer i){

        try {
            Statement stmt = db.createStatement ();
            stmt.executeUpdate ("insert into "+TABLE_NAME+" values('xxxxxxxxxxxxxxx',"+i.toString()+")");
        }
        catch (SQLException e) {
            System.out.println("Exception: "+e.getMessage());
        }

    }


}
