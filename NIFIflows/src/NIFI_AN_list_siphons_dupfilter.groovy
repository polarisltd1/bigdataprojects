




/**

InvokeScriptedProcessor - Hello World!

http://funnifi.blogspot.com/2016/02/invokescriptedprocessor-hello-world.html

Learning : what is def in Groovy
 ================================

Purpose of def keyword

The main purpose of def is to introduce dynamic types in Groovy. As you can see in the above example, this is still old Java Object type.

The good practice is to use static types whenever you use static type on purpose. Your public API should definitely use static types for documentation reasons.
 I personally use def from time to time in two cases:

in local variables that has very limited scope and variable name provides all information about the variable itself
in Spock unit tests in test case name e.g. def "should do somthing"() { }

Javadoc :
==========
 https://static.javadoc.io/org.apache.nifi/nifi-api/1.0.0/org/apache/nifi/flowfile/FlowFile.html

 Examples of property descriptors
================================
 def DBCP_SERVICE = new PropertyDescriptor.Builder()
 .name('Database Connection Pooling Service')
 .description("The Controller Service that is used to obtain connection to database.")
 .required(true)
 .identifiesControllerService(DBCPService.class)  // refer to controller
 .build()



 def DB_QUERY = new PropertyDescriptor.Builder()
 .name('SQL Query')
 .description('SQL query to be executed.')
 .required(true)
 .expressionLanguageSupported(true)
 .addValidator(Validator.VALID)
 .build()



 def FILENAME = new PropertyDescriptor.Builder()
 .name('File Name')
 .description('Sets the filename attribute of the flowfile (do not include the extension).')
 .required(true)
 .expressionLanguageSupported(true)
 .addValidator(Validator.VALID)
 .build()

CONVERTING INPUTSTREAM to OUTPUTSTREAM
 =====================================

 void feedInputToOutput(InputStream in, OutputStream out) {
 IOUtils.copy(in, out);
 }
 and be done with it?

 from jakarta apache commons i/o library which is used by a huge amount of projects already so you probably already have the jar in your classpath already.

 okhttp3 consume response.body as bytearray
 ==========================================

 InputStream	response.body().byteStream()

 after that response.body().close()

 Groovy restful examples
 =========================
 https://github.com/jgritman/httpbuilder/wiki/POST-Examples

----------- post example ----------

 import groovyx.net.http.HTTPBuilder
 import static groovyx.net.http.ContentType.URLENC

 def http = new HTTPBuilder( 'http://restmirror.appspot.com/' )
 def postBody = [name: 'bob', title: 'construction worker'] // will be url-encoded

 http.post( path: '/', body: postBody,
 requestContentType: URLENC ) { resp ->

 println "POST Success: ${resp.statusLine}"
 assert resp.statusLine.statusCode == 201
 }

 -------- GET examole ------------

 def http = new HTTPBuilder('http://www.google.com')

 def html = http.get( path : '/search', query : [q:'Groovy'] )

 assert html instanceof groovy.util.slurpersupport.GPathResult
 assert html.HEAD.size() == 1
 assert html.BODY.size() == 1

*/



import org.apache.nifi.components.PropertyDescriptor
import org.apache.nifi.components.ValidationContext
import org.apache.nifi.components.ValidationResult
import org.apache.nifi.components.Validator
import org.apache.nifi.flowfile.FlowFile
import org.apache.nifi.logging.ComponentLog
import org.apache.nifi.processor.ProcessContext
import org.apache.nifi.processor.ProcessSession
import org.apache.nifi.processor.ProcessSessionFactory
import org.apache.nifi.processor.Processor;
import org.apache.nifi.processor.ProcessorInitializationContext
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException
import org.apache.nifi.processor.io.StreamCallback
import org.apache.nifi.components.state.Scope
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat
import org.apache.nifi.components.state.StateMap;


class GroovyProcessor implements Processor {




    private String URL_GET_LISTOFFEEDS = "https://api.appnexus.com/siphon";


    public static final String CURRENT_TIMESTAMP = "currentTimestamp";
    public static final String CURRENT_KEY_PREFIX = "key-";
    long currentTimestamp = 0L;
    Set<String> currentKeys;

    private Set<String> extractKeys(final StateMap stateMap) {
        //return stateMap.//.toMap().entrySet().//..parallelStream()
        //.filter(x -> x.getKey().startsWith(CURRENT_KEY_PREFIX))
        // .map(Map.Entry::getValue)
        /// .collect(Collectors.toSet());
        //
        def v = stateMap.toMap().entrySet().findAll ({x -> x.getKey().startsWith(CURRENT_KEY_PREFIX)}).collectEntries(){it->it.value}
        log.debug("extractKeys : ${v.getClass().getName()} : ${v}")
       return v



     }

    void restoreState(final ProcessContext context) throws IOException {
        final StateMap stateMap = context.getStateManager().getState(Scope.CLUSTER);
        if (stateMap.getVersion() == -1L || stateMap.get(CURRENT_TIMESTAMP) == null || stateMap.get(CURRENT_KEY_PREFIX+"0") == null) {
            currentTimestamp = 0L;
            currentKeys = new HashSet<>();
        } else {
            currentTimestamp = Long.parseLong(stateMap.get(CURRENT_TIMESTAMP));
            currentKeys = extractKeys(stateMap);
        }
    }

    void persistState(final ProcessContext context) {
        Map<String, String> state = new HashMap<>();
        state.put(CURRENT_TIMESTAMP, String.valueOf(currentTimestamp));
        int i = 0;
        for (String key : currentKeys) {
            state.put(CURRENT_KEY_PREFIX+i, key);
            i++;
        }
        try {
            context.getStateManager().setState(state, Scope.CLUSTER);
        } catch (IOException ioe) {
            getLogger().error("Failed to save cluster-wide state. If NiFi is restarted, data duplication may occur", ioe);
        }
    }





    // not sure can this help
    public static final PropertyDescriptor USERNAME = new PropertyDescriptor.Builder()
            .name("username")
            .description("username")
            .displayName("username")
            .required(true)
            .addValidator(Validator.VALID)
            .build();
    public static final PropertyDescriptor PASSWORD = new PropertyDescriptor.Builder()
            .name("password")
            .description("password")
            .displayName("password")
            .sensitive(true)
            .required(true)
            .addValidator(Validator.VALID)
            .build();

    public static final PropertyDescriptor FEED = new PropertyDescriptor.Builder()
            .name("feed")
            .description("feed")
            .displayName("feed")
            .required(true)
            .addValidator(Validator.VALID)
            .build();



    List<PropertyDescriptor> propList = Arrays.asList(USERNAME,PASSWORD,FEED)
    //return [DBCP_SERVICE, DB_QUERY, FILENAME] as List // Groovy Way


    def REL_SUCCESS = new Relationship.Builder()
    .name("success")
    .description("FlowFiles that were successfully processed")
    .build();

    ComponentLog log

    @Override
    void initialize(ProcessorInitializationContext context) {
       log = context.getLogger()
    }

    @Override

    Set<Relationship> getRelationships() {
        return [REL_SUCCESS] as Set
    }

    @Override
    void onTrigger(ProcessContext context, ProcessSessionFactory sessionFactory) throws ProcessException {
        try {

            log.debug("================01 Hello onTrigger() ===================")

            try {
                restoreState(context);
            } catch (IOException e) {
                getLogger().error("Failed to restore processor state; yielding", e);
                context.yield();
                return;
            }


            ProcessSession session  = sessionFactory.createSession()


            def username = context.getProperty(USERNAME)?.evaluateAttributeExpressions()?.getValue()
            def password = context.getProperty(PASSWORD)?.evaluateAttributeExpressions()?.getValue()
            def feed = context.getProperty(FEED)?.evaluateAttributeExpressions()?.getValue()



            log.debug("user/pas  ${username} xxxxx")


            //def username = "paul@tumra.com"
            //def password = "Bstlf8be1234!!"
            def token = authAN(username,password);

            log.debug("TOKEN => ${token}")

            updateState(context,"ANtoken",token)
            String updatedSince  = new SimpleDateFormat("yyyyMMdd").format(new Date())
            updateState(context,"updated_since",updatedSince)

            def json = getSiphonList(token,feed)

            if(false)json.response.siphons.each({log.debug("siphon: "+it.hour+it.timestamp)})

            log.debug "SIPHONS size:"+json.response.siphons.size


            // we will send this out as FlowFile

            FlowFile flowFile = session.create()

            flowFile = session.putAttribute(flowFile, "filename", "siphons_${feed}.txt")

            flowFile = session.write(flowFile,

                    { inputStream, outputStream ->

                        json.response.siphons.each({

                            outputStream.write("${it.name},${it.hour},${it.timestamp}\n".getBytes('UTF-8'))


                        })


                    } as StreamCallback)

            log.debug("FlowFile created/routed  by iterating siphons!")

            session.transfer(flowFile, REL_SUCCESS)
            session.commit()





        }
        catch (e) {
            log.error("Exception: ",e)
            throw new ProcessException(e)
        }
    }

    @Override
    Collection<ValidationResult> validate(ValidationContext context) { /*log.debug("validate event ...");*/return null }

    @Override
    PropertyDescriptor getPropertyDescriptor(String name) {
        log.debug("getPropertyDescriptor ${name} ");
        propList.each({if(it.name==name)return it})
        log.debug("getPropertyDescriptor ${name} not found!");

        return null
    }

    @Override

    void onPropertyModified(PropertyDescriptor descriptor, String oldValue, String newValue) {/*log.debug("onPropertyModified ${descriptor}:${oldValue}:${newValue} ...");*/ }

    @Override

    List<PropertyDescriptor> getPropertyDescriptors() { /*log.debug("getPropertyDescriptors event ...");*/return propList}

    @Override

    String getIdentifier() {log.debug("getIdentifier event ..."); return null }

    /**
     * Retrieve authentification token.
     * @param username
     * @param pass
     * @return
     */
    String authAN(username,pass){


        def authMessage = "{\"auth\": {\"username\" : \"${username}\",\"password\" : \"${pass}\"}}"
        def authUrl = "https://api.appnexus.com/auth";


        log.info("sending AN post ${authUrl} => ${authMessage}")

        def json = doPostRequest(authUrl,authMessage)

        def token = json?.response?.token

        log.debug("1token => ${token}")

        return token

    }

/**
 * Get Siphon List
 */

    def getSiphonList(token,feed){

        /**
         *
         *
         *
         *
         Map<String, String> headers = new HashMap<>();
         headers.put("Authorization", authToken);


         // construct siphon list url
         StringBuilder urlSb = new StringBuilder().append(props.getParams().getItem("siphons_url", URL_GET_LISTOFFEEDS));
         String sep = "?";
         if (feedFilter != null && !feedFilter.equals("")) {
         urlSb.append(sep).append("siphon_name=").append(feedFilter);
         sep = "&";
         }
         if (!updatedSince.trim().equals("")) {
         urlSb.append(sep).append("updated_since=").append(updatedSince);
         }


         */


        def url = "${URL_GET_LISTOFFEEDS}?siphon_name=${feed}";

        def json = doGetRequestAuth(url,token)


        assert json.response.status == "OK","Response not found"

        return json


    }

    /**
     * Example of state management. Kind of global variable between processor calls
     * we can store here lastUpdated and lastToken
     * @param context
     * @return
     *
     *
     * unmodifable map can be created:
     * Collections.unmodifiableMap(myMap);
     */
    def updateState(context,String k,String v){

        def stateMap = context.stateManager.getState(Scope.CLUSTER).toMap()


        log.debug("stateMap.class2: "+stateMap.getClass().getName())

        Map newStateMap = [:]
        newStateMap.putAll(stateMap) // make a copy from UnmodifableMap
        newStateMap[k] = v

        log.debug("updateState ${k} ${v} 1=> stateMap: ${newStateMap}")

        context.stateManager.setState(newStateMap, Scope.CLUSTER);

    }


    def doPostRequest(String url, String json) throws IOException {

        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();

        def res = response.body().string()  // this shouldnt be accessed twice!

        log.debug("=> ${res}")

        def jsonObj = new JsonSlurper().parseText(res)

        return jsonObj;

    }



    def doGetRequestAuth(String url,String token) throws IOException {

        log.info("getRequest url:${url} token:${token}")
        assert token!=null,"Request with Null token"

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .addHeader("Authorization",token)
                .url(url)
                .build();

        Response response = client.newCall(request).execute()

        def res = response.body().string()

        def jsonObj = new JsonSlurper().parseText(res)

        return jsonObj;

    }


}

processor = new GroovyProcessor()