import org.apache.nifi.components.PropertyDescriptor
import org.apache.nifi.components.ValidationContext
import org.apache.nifi.components.ValidationResult
import org.apache.nifi.components.Validator
import org.apache.nifi.flowfile.FlowFile
import org.apache.nifi.logging.ComponentLog
import org.apache.nifi.processor.ProcessContext
import org.apache.nifi.processor.ProcessSession
import org.apache.nifi.processor.ProcessSessionFactory
import org.apache.nifi.processor.Processor;
import org.apache.nifi.processor.ProcessorInitializationContext
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException
import org.apache.nifi.processor.io.StreamCallback
import org.apache.nifi.components.state.Scope
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat



class GCSListFiles implements Processor {


    def REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("The flowfile with the specified query results was successfully transferred.")
            .build();


    ComponentLog log

    @Override
    void initialize(ProcessorInitializationContext context) {
        log = context.getLogger()
    }

    @Override

    Set<Relationship> getRelationships() {
        return [REL_SUCCESS] as Set
    }

    @Override
    void onTrigger(ProcessContext context, ProcessSessionFactory sessionFactory) throws ProcessException {
        try {



            ProcessSession session  = sessionFactory.createSession()


            FlowFile flowFile = session.get()

            //flowFile = session.putAttribute(flowFile, "filename", "siphons_${feed}.txt")


            log.debug("=> ${flowFile}")

            session.transfer(flowFile, REL_SUCCESS)
            session.commit()





        }
        catch (e) {
            log.error("Exception: ",e)
            throw new ProcessException(e)
        }
    }

    @Override
    Collection<ValidationResult> validate(ValidationContext context) { /*log.debug("validate event ...");*/return null }

    @Override
    PropertyDescriptor getPropertyDescriptor(String name) {
        log.debug("getPropertyDescriptor ${name} ");
        propList.each({if(it.name==name)return it})
        log.debug("getPropertyDescriptor ${name} not found!");

        return null
    }

    @Override

    void onPropertyModified(PropertyDescriptor descriptor, String oldValue, String newValue) {/*log.debug("onPropertyModified ${descriptor}:${oldValue}:${newValue} ...");*/ }

    @Override

    List<PropertyDescriptor> getPropertyDescriptors() { /*log.debug("getPropertyDescriptors event ...");*/return propList}

    @Override

    String getIdentifier() {log.debug("getIdentifier event ..."); return null }


}

processor = new GCSListFiles()
