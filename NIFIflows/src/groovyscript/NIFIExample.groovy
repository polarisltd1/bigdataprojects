
/**

InvokeScriptedProcessor - Hello World!

http://funnifi.blogspot.com/2016/02/invokescriptedprocessor-hello-world.html

Learning : what is def in Groovy

Purpose of def keyword

The main purpose of def is to introduce dynamic types in Groovy. As you can see in the above example, this is still old Java Object type.

The good practice is to use static types whenever you use static type on purpose. Your public API should definitely use static types for documentation reasons. I personally use def from time to time in two cases:

in local variables that has very limited scope and variable name provides all information about the variable itself
in Spock unit tests in test case name e.g. def "should do somthing"() { }

Javadoc : https://static.javadoc.io/org.apache.nifi/nifi-api/1.0.0/org/apache/nifi/flowfile/FlowFile.html


*/




import org.apache.nifi.components.PropertyDescriptor
import org.apache.nifi.components.ValidationContext
import org.apache.nifi.components.ValidationResult
import org.apache.nifi.flowfile.FlowFile
import org.apache.nifi.logging.ComponentLog
import org.apache.nifi.processor.ProcessContext
import org.apache.nifi.processor.ProcessSession
import org.apache.nifi.processor.ProcessSessionFactory
import org.apache.nifi.processor.Processor;
import org.apache.nifi.processor.ProcessorInitializationContext
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.exception.ProcessException
import org.apache.nifi.processor.io.StreamCallback



class GroovyProcessor implements Processor {


    def REL_SUCCESS = new Relationship.Builder()
    .name("success")
    .description("FlowFiles that were successfully processed")
    .build();

    ComponentLog log

    @Override
    void initialize(ProcessorInitializationContext context) {
       log = context.getLogger()
    }

    @Override

    Set<Relationship> getRelationships() {
        return [REL_SUCCESS] as Set
    }

    @Override
    void onTrigger(ProcessContext context, ProcessSessionFactory sessionFactory) throws ProcessException {
        try {

            ProcessSession session  = sessionFactory.createSession()
            FlowFile flowFile = session.get()
            //session
            if (!flowFile) return
            log.debug("Hello from Groovy ScriptRunner")
            def selectedColumns = ''
            flowFile = session.write(flowFile,
                    { inputStream, outputStream ->
                        String line
                        final BufferedReader inReader = new BufferedReader(new InputStreamReader(inputStream, 'UTF-8'))
                        line = inReader.readLine()
                        String[] header = line?.split(',')
                        selectedColumns = "${header[1]},${header[2]}"                  
                        while (line = inReader.readLine()) {
                            String[] cols = line.split(',')
                            outputStream.write("${cols[2].capitalize()} ${cols[3].capitalize()}\n".getBytes('UTF-8'))
                        }
                    } as StreamCallback)

            flowFile = session.putAttribute(flowFile, "selected.columns", selectedColumns)
            flowFile = session.putAttribute(flowFile, "filename", "split_cols_invoke.txt")
            // transfer
            session.transfer(flowFile, REL_SUCCESS)
            session.commit()
        }
        catch (e) {
            throw new ProcessException(e)
        }
    }

    @Override
    Collection<ValidationResult> validate(ValidationContext context) { log.debug("validate event ...");return null }

    @Override
    PropertyDescriptor getPropertyDescriptor(String name) {log.debug("getPropertyDescriptor ${name} event ..."); return null }

    @Override

    void onPropertyModified(PropertyDescriptor descriptor, String oldValue, String newValue) {log.debug("validate event ${descriptor}:${oldValue}:${newValue} ..."); }

    @Override

    List<PropertyDescriptor> getPropertyDescriptors() { log.debug("getPropertyDescriptors event ...");return null }

    @Override

    String getIdentifier() {log.debug("getIdentifier event ..."); return null }
}

processor = new GroovyProcessor()