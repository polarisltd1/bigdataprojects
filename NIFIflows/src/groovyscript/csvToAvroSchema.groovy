// source reference: github mattyb149/csv_to_avroschema.groovy 
// A Groovy script for NiFi ExecuteScript to extract the schema from the header line of a CSV file
import groovy.json.*


def flowFile = session.get()
if(!flowFile) return

def delim = ','
try {
   delim = delimiter?.value ?: ','
} catch (MissingPropertyException mpe) { }

try {
def line
def inputStream = session.read(flowFile)
inputStream.withReader { line = it.readLine() }
inputStream.close()

def json = new JsonBuilder()

json {
  type('record')
  name('csv_record')
  fields(line.tokenize(delim).collect { col ->
    ['name': col, 'type': ["null","string"]]
  })
}

flowFile = session.putAttribute(flowFile, 'avro.schema', json.toString())
session.transfer(flowFile, REL_SUCCESS)
} catch(Exception e) {
  log.error('Error processing file, transferring to failure', e)
  session.transfer(flowFile, REL_FAILURE)
}