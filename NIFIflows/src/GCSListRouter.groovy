import org.apache.nifi.flowfile.FlowFile


FlowFile flowFile = session.get()

log.debug("=> ${flowFile}")

if(!flowFile) return

try {
    session.transfer(flowFile, REL_SUCCESS)
} catch(Exception e) {
    log.error('Error processing file, transferring to failure', e)
    session.transfer(flowFile, REL_FAILURE)
}
