/*


This example shows how to define and access properties.


http://funnifi.blogspot.com/2016/02/writing-reusable-scripted-processors-in.html


*/

import groovy.json.JsonBuilder
import groovy.sql.Sql
import org.apache.nifi.components.PropertyDescriptor
import org.apache.nifi.components.ValidationContext
import org.apache.nifi.components.ValidationResult
import org.apache.nifi.components.Validator
import org.apache.nifi.dbcp.DBCPService
import org.apache.nifi.logging.ComponentLog
import org.apache.nifi.processor.*
import org.apache.nifi.processor.exception.ProcessException
import org.apache.nifi.processor.io.OutputStreamCallback

class MySQLToJSON implements Processor {


    def REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("The flowfile with the specified query results was successfully transferred.")
            .build();

    // TODO: figure out how to get exception messages to show in the bulletins or go out to a failure relationship

    def REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("An error occured while running the specified query.")
            .build();


    def DBCP_SERVICE = new PropertyDescriptor.Builder()
            .name('Database Connection Pooling Service')
            .description("The Controller Service that is used to obtain connection to database.")
            .required(true)
            .identifiesControllerService(DBCPService.class)
            .build()


    def DB_QUERY = new PropertyDescriptor.Builder()
            .name('SQL Query')
            .description('SQL query to be executed.')
            .required(true)
            .expressionLanguageSupported(true)
            .addValidator(Validator.VALID)
            .build()


    def FILENAME = new PropertyDescriptor.Builder()

            .name('File Name')

            .description('Sets the filename attribute of the flowfile (do not include the extension).')

            .required(true)

            .expressionLanguageSupported(true)

            .addValidator(Validator.VALID)

            .build()


    def ComponentLog log


    @Override

    void initialize(ProcessorInitializationContext context) {

        log = context.getLogger()

    }


    @Override



    Set<Relationship> getRelationships() {
        return [REL_SUCCESS, REL_FAILURE] as Set
    }


    @Override

    void onTrigger(ProcessContext context, ProcessSessionFactory sessionFactory) throws ProcessException {

        try {


            def session = sessionFactory.createSession()
//            def flowFile = session.get() // use existing flowfile if one is there
            def flowFile = session.create() // create new flowfile if not
            log.info("Flowfile created")



            def filename = context.getProperty(FILENAME)?.evaluateAttributeExpressions()?.getValue()
            log.info("Filename set")

            // TODO: Evaluate expression langauge for getProperty calls

            def dbcpService = context.getProperty(DBCP_SERVICE).asControllerService(DBCPService.class)

            log.info("dbcpService set")

            def conn = dbcpService.getConnection()

            log.info("getConnection() called")

            def sql = new Sql(conn)

            log.info("Sql(conn) called set")

            def dbResults = sql.rows(context.getProperty(DB_QUERY).value)

            log.info("dbResults set")

            def jsonResults = new JsonBuilder(dbResults).toPrettyString()

            log.info("jsonResults set")

            flowFile = session.write(flowFile, { outputStream ->

                outputStream.write(jsonResults.getBytes('UTF-8'))

                log.info("outputStream written")

            } as OutputStreamCallback)

            //flowFile = session.putAttribute(flowFile, "executesql.row.count", dbResults.size())

            flowFile = session.putAttribute(flowFile, "mime.type", "application/json")

            log.info("mime.type attribute set")

            flowFile = session.putAttribute(flowFile, "filename", "test")

            log.info("filename attribute set")

            // transfer

            session.transfer(flowFile, REL_SUCCESS)

            log.info("session.transfer called")

            session.commit()

            log.info("session.commit called")

        }

        catch (e) {

            log.error("Processor error: ${e.getMessage()}")

            session.transfer(flowFile, REL_FAILURE)

            session.commit()

        }

    }


    @Override

    Collection<ValidationResult> validate(ValidationContext context) { return null }


    @Override

    PropertyDescriptor getPropertyDescriptor(String name) {

        switch (name) {

            case 'Database Connection Pool Service': return DBCP_SERVICE

            case 'SQL Query': return DB_QUERY

            case 'File Name': return FILENAME

            default: return null

        }

    }


    @Override

    void onPropertyModified(PropertyDescriptor descriptor, String oldValue, String newValue) {}


    @Override

    List<PropertyDescriptor> getPropertyDescriptors() {

        return [DBCP_SERVICE, DB_QUERY, FILENAME] as List

    }


    @Override

    String getIdentifier() { return 'MySQLToJSON-InvokeScriptedProcessor' }

}


processor = new MySQLToJSON()
