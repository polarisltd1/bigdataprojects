import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.util.stream.IntStream;

/**
 * we calling here code created by
 * protoc --java_out=./java tags.proto
 *
 *
 *
 *
 message TagEntrada {
 optional uint64 tagId = 1;
 optional uint32 situacao = 4;
 optional uint32 motivoBloqueio = 5;
 optional uint32 temPassagem = 8;
 }

 message Tag {
 optional uint32 concessionariaId = 1;
 optional uint64 sequencial = 2;
 repeated TagEntrada tag = 3;
 optional uint32 osaId = 4;
 optional uint32 serie = 5;
 }
 */
public class Application {





    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage:  filename to decode");
            System.exit(-1);
        }

        // Read the existing address book.
        Tags1.Tag origTag = Tags1.Tag.parseFrom(new FileInputStream(args[0]));

        System.out.println(origTag);
        System.out.printf("size of taglist: %s ",origTag.getTagList().size());







        // write new output file
        /*

        Person john =
  Person.newBuilder()
    .setId(1234)
    .setName("John Doe")
    .setEmail("jdoe@example.com")
    .addPhones(
      Person.PhoneNumber.newBuilder()
        .setNumber("555-4321")
        .setType(Person.PhoneType.HOME))
    .build();

void writeTo(OutputStream output);: serializes the message and writes it to an OutputStream.

        */
        Tags1.Tag.Builder b = Tags1.Tag.newBuilder()
                .setConcessionariaId(5)
                .setSequencial(6);
        IntStream.range(0, 581).forEach(
               i-> b.addTag(origTag.getTag(i))
        );
        // copy old tags

                // create new tags
                b
                 .addTag(Tags1.TagEntrada.newBuilder().setTagId(1).setSituacao(5).setTemPassagem(6))
                .addTag(Tags1.TagEntrada.newBuilder().setTagId(2))
                .addTag(Tags1.TagEntrada.newBuilder().setTagId(3))
                .setOsaId(2)
                .setSerie(1);


        Tags1.Tag newTag = b.build();

        System.out.println(newTag);
        System.out.printf("size of taglist: %s \n",newTag.getTagList().size());
        
        
        FileOutputStream os = new FileOutputStream("output"+ LocalDateTime.now()+".bin");
        newTag.writeTo(os);

    }




}
