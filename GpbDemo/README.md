## Google protocol buffer ##

This very similar to ASN.1 decoding. Fields are identified by tags which has been used by text description to describe fields and types.
Purpose of this is to reduce in size and increase performance for xml and json format usage.

Read this article to get clear overview:

<https://www.javacodegeeks.com/2012/06/google-protocol-buffers-in-java.html>



using --decode_raw it will decode just with tag numbers

```
protoc --decode_raw < msg.bin
```
```
protoc --decode_raw <tags.1-10-6-707.bin
```
command to decode is following:

protoc --decode TYPE PROTOFILE.proto <INPUTFILE.bin

after --decode provide type name and then .proto file . input binary file has been read from standard input

example:
```
protoc --decode Tag tags1.proto <tags.1-10-6-707.bin
```

In this example Tag is a following structure:
```
message Tag {
  optional uint32 concessionariaId = 1;
  optional uint64 sequencial = 2;
  repeated TagEntrada tag = 3;
  optional uint32 osaId = 4;
  optional uint32 serie = 5;
}
```
the format is Type name = TAG_ID

Those types are defined possible as Enum and then enum value is being decoded.

Compiling into java
```
 protoc --java_out=$DST_DIR addressbook.proto
```
in my example
```
mkdir ./java
protoc --java_out=./java tags.proto
```
it creates one .java source file into ./java folder





## Overview ##

Briefly, on the wire, protobufs are encoded as 3-tuples of <key,type,value>, where the key is the field number assigned to the field in the .proto schema. The type is one of <Varint, int32, length-delimited, start-group, end-group,int64>. 
It contains just enough information to decode the value of the 3-tuple, namely it tells you how long the value is.


Basically, you define how you want your data to be structured once using a .proto specification file.

This is analogous to an IDL file or a specification language to describe a software component. This file is consumed by the 

protocol buffer compiler (protoc) 

which will generate supporting methods so that you can write and read objects to and from a variety of streams.



## HOW TO DOWNLOAD protoc binary: ##

For non-C++ users, the simplest way to install the protocol compiler is to download a pre-built binary from our release page:

<https://github.com/google/protobuf/releases>

in this page scroll to most bottom and find following:

In the downloads section of each release, you can find pre-built binaries in zip packages: protoc-$VERSION-$PLATFORM.zip. It contains the protoc binary as well as a set of standard .proto files distributed along with protobuf.

In Ubuntu if u try to use protoc it will point out to install command.

protoc --decode_raw <tags.1-10-6-707.bin

The program 'protoc' is currently not installed. You can install it by typing:
sudo apt install protobuf-compiler





