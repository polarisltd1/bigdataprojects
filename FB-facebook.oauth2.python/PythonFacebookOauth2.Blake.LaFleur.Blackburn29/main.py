import fbauth
import os

fbAuth = fbauth.TokenHandler(os.environ['FB_APP_ID'],
                os.environ['FB_APP_SECRET'])

access_token = fbAuth.get_access_token()