// http://crunchify.com/how-to-create-restful-java-client-with-jersey-client-example/
package com.tumra.datafeeds.restful;

import com.google.gson.JsonElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


   

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientResponse;

/**
 * @author Crunchify
 *
 */
public class RestfulService {


     private static final Logger LOG = LogManager.getLogger(RestfulService.class);

    /**
     * This is main method supplied for testing purpose.
     *
     * @param args
     */
    
        
    public Response getR(String url,Map<String,String> headers){
        
        
    Client client = ClientBuilder.newClient(new ClientConfig());        
 

 
   Invocation.Builder builder = client
                         .target(url)
                         .request(MediaType.APPLICATION_JSON);
   
   if(headers!=null){
           for(String key: headers.keySet()){
               builder.header(key, headers.get(key));
           }
   }

   Response response = builder.get();
   
        if ((response.getStatus() < 200 || response.getStatus() > 299) && response.getStatus()!=302) {
            // this will be just informational
            LOG.debug("GET Request failed : HTTP error code : " + response.getStatus()+
                    "...\n "+url+" headers: "+headers            
            );
        } 
        return response; // response should be null in case of error
    }
    
    
    public String get(String url,Map<String,String> headers){

        Response response = getR(url,headers); 
        String output = null;
        if(response!=null){
          output = response.readEntity(String.class);
          LOG.debug("============get restfulResponse "+url+"============");
          //logger.info(output);
        }
        return output;
    }
    
    public MultivaluedMap<String,Object> getH(String url,Map<String,String> headers){
       Response response = getR(url,headers); 
       if(response!=null){
          return response.getHeaders(); 
       }else return null;
    }
    
    
    public String post(String url,String data, Map<String,String> headers){
        
        
    Client client = ClientBuilder.newClient(new ClientConfig());        
 

 
   Invocation.Builder builder = client
                         .target(url)
                         .request(MediaType.APPLICATION_JSON);
   
   if(headers!=null){
           for(String key: headers.keySet()){
               builder.header(key, headers.get(key));
           }
   }

   Response response = builder
           //.post(ClientResponse.class, data);
           .post(Entity.json(data));
   
        if ((response.getStatus() >= 200 && response.getStatus() <= 299) || response.getStatus()==302 ) {
            
                String output2 = response.readEntity(String.class);
            LOG.debug("============post "+url+"restfulResponse============");
            LOG.debug(output2);
            //dumpJson(output2);
            return output2;
        } else{        
            
            

            LOG.debug("GET Request failed : HTTP error code : " + response.getStatus()+
                    "...\n "+url+" headers: "+headers
            
            );
            
            return null;
            
        }

    }
    
    
    
    /**
     * This is test method to recursively parse Json
     *
     * @param json
     */
    void dumpJson(String json) {
        JsonParser parser = new JsonParser();
        JsonElement jsonTree = parser.parse(json);
        if(jsonTree.isJsonObject()) jobj("", jsonTree.getAsJsonObject());
        else if(jsonTree.isJsonArray()) jarr("", jsonTree.getAsJsonArray());
    }

    /**
     * Walk through json map.
     *
     * @param key
     * @param obj
     */
    void jobj(String key, JsonObject obj) {

        Set<Map.Entry<String, JsonElement>> entries = obj.entrySet();
        for (Map.Entry<String, JsonElement> entry : entries) {
            JsonElement e = entry.getValue();
            if (e.isJsonObject()) {
                jobj(key + "->" + entry.getKey(), e.getAsJsonObject());
            } else if (e.isJsonArray()) {
                jarr(key + "->" + entry.getKey(), e.getAsJsonArray());
            } else if (e.isJsonPrimitive()) {
                LOG.debug(key + "->" + entry.getKey() + " = " + e.getAsString());
            }
        }
    }

    /**
     * Walk through json array
     *
     * @param key
     * @param arr
     */
    void jarr(String key, JsonArray arr) {

        for (int i = 0; i < arr.size(); i++) {
            JsonElement e = arr.get(i);
            if (e.isJsonObject()) {
                jobj(key + "[" + i + "]", e.getAsJsonObject());
            } else if (e.isJsonArray()) {
                jarr(key + "[" + i + "]", e.getAsJsonArray());
            } else if (e.isJsonPrimitive()) {
                LOG.debug(key + "[" + i + "] = " + e.getAsString());
            }
        }

    }

    /*  ====== left here for reference purpose ========
    public String parse(String jsonLine) {
    JsonElement jelement = new JsonParser().parse(jsonLine);
    JsonObject  jobject = jelement.getAsJsonObject();
    jobject = jobject.getAsJsonObject("data");
    JsonArray jarray = jobject.getAsJsonArray("translations");
    jobject = jarray.get(0).getAsJsonObject();
    String result = jobject.get("translatedText").toString();
    return result;
    }
     */
}
