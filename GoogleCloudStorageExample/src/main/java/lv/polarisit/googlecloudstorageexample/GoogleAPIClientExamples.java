/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.polarisit.googlecloudstorageexample;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.IOUtils;
import com.google.api.gax.core.CredentialsProvider;
/*
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;
//import com.google.cloud.storage.StorageOptions;
import com.google.common.collect.Lists;
 */

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import com.google.logging.type.HttpRequest;
import com.tumra.datafeeds.restful.RestfulService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Iterator;
import java.util.zip.GZIPOutputStream;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//
/*
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
//import com.google.api.services.samples.storage.util.CredentialsProvider;
import com.google.api.services.storage.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
 */

/**
 *
 * @author robertspolis
 */
public class GoogleAPIClientExamples {

    private static final String STORAGE_SCOPE
            = "https://www.googleapis.com/auth/devstorage.read_write";

    private static final Logger LOG = LogManager.getLogger(GoogleAPIClientExamples.class);
    String bucketName = "dcdt_-dcm_account177203";

    public void download(String bucketName,String filename) throws Exception {

        // https://github.com/GoogleCloudPlatform/java-docs-samples/blob/master/storage/xml-api/cmdline-sample/src/main/java/StorageSample.java     
        // Build an account credential.
        GoogleCredential credential = GoogleCredential.getApplicationDefault()
                .createScoped(Collections.singleton(STORAGE_SCOPE));

// Set up and execute a Google Cloud Storage request.
        String uri = "https://storage.googleapis.com/"
                + URLEncoder.encode(bucketName, "UTF-8")+"/"+URLEncoder.encode(filename, "UTF-8");
        
        LOG.info("download url is: {}",uri);
        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        HttpRequestFactory requestFactory = httpTransport.createRequestFactory(
                credential);
        GenericUrl url = new GenericUrl(uri);

        com.google.api.client.http.HttpRequest request = requestFactory.buildGetRequest(url);
        HttpResponse response = request.execute();
        if (false) {
            String content = response.parseAsString();
        }
        InputStream is = response.getContent();
        if(false)readInputStream(is);
        FileOutputStream fop = new FileOutputStream(new File("/tmp/"+filename));
        isToOs(is,fop);
        LOG.info("download OK");
    }
public void readInputStream(InputStream is) throws Exception{
    

final int bufferSize = 50*1024;  // 50k
final char[] buffer = new char[bufferSize];
final StringBuilder out = new StringBuilder();
Reader in = new InputStreamReader(is, "UTF-8");
for (; ; ) {
    int rsz = in.read(buffer, 0, buffer.length);
    if (rsz < 0)
        break;
    LOG.info("reading buffer");
    //out.append(buffer, 0, rsz);
}
 
}    
    
    
public void isToOs(InputStream is,OutputStream os) throws Exception{
    //StringWriter writer = new StringWriter();
// this is apache commons
    IOUtils.copy(is, os);
    //return writer.toString();
}    
    
    
    public static String listBucket(final String bucketName)
            throws IOException, GeneralSecurityException {
        /*
    https://github.com/GoogleCloudPlatform/java-docs-samples/blob/master/storage/xml-api/cmdline-sample/src/main/java/StorageSample.java
    
Exception in thread "main" java.io.IOException: The Application Default Credentials are not available. They are available if running in Google Compute Engine. Otherwise, the environment variable GOOGLE_APPLICATION_CREDENTIALS must be defined pointing to a file defining the credentials. See https://developers.google.com/accounts/docs/application-default-credentials for more information.

    
         */
        //[START snippet]
        // Build an account credential.
        GoogleCredential credential = GoogleCredential.getApplicationDefault()
                .createScoped(Collections.singleton(STORAGE_SCOPE));

        // Set up and execute a Google Cloud Storage request.
        String uri = "https://storage.googleapis.com/"
                + URLEncoder.encode(bucketName, "UTF-8");

        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
        HttpRequestFactory requestFactory = httpTransport.createRequestFactory(
                credential);
        GenericUrl url = new GenericUrl(uri);

        com.google.api.client.http.HttpRequest request = requestFactory.buildGetRequest(url);
        HttpResponse response = request.execute();
        String content = response.parseAsString();
        //[END snippet]
        LOG.info("listbucket() output: {}", content);

        return content;
    }

    public void test7() throws Exception {
        /*
    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
    Credential credential = CredentialsProvider.authorize(httpTransport, jsonFactory);
    Storage storage = new Storage.Builder(httpTransport, jsonFactory, credential)
        .setApplicationName("Google-ObjectsDownloadExample/1.0").build();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    downloadZ(storage, "BUCKET_NAME", "OBJECT_NAME");
    System.out.println("Downloaded " + out.toByteArray().length + " bytes");
  }

public static InputStream downloaadZ(Storage storage, String bucketName, String objectName)
      throws IOException {
    Storage.Objects.Get getObject = storage.objects().get(bucketName, objectName);
    getObject.getMediaHttpDownloader().setDirectDownloadEnabled(!IS_APP_ENGINE);
    return getObject.executeMediaAsInputStream(); 
  }

         */
    }

    public void test8() throws Exception {
        /*
    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
    Credential credential = CredentialsProvider.authorize(httpTransport, jsonFactory);
    Storage storage = new Storage.Builder(httpTransport, jsonFactory, credential)
        .setApplicationName("Google-ObjectsDownloadExample/1.0").build();
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    downloadZ(storage, "BUCKET_NAME", "OBJECT_NAME");
    System.out.println("Downloaded " + out.toByteArray().length + " bytes");
  }

public static InputStream downloaadZ(Storage storage, String bucketName, String objectName)
      throws IOException {
    Storage.Objects.Get getObject = storage.objects().get(bucketName, objectName);
    getObject.getMediaHttpDownloader().setDirectDownloadEnabled(!IS_APP_ENGINE);
    return getObject.executeMediaAsInputStream(); 
  }

         */
    }

    public void test2(String url) {

        Response r = new RestfulService().getR(url, null);
        LOG.info("test2() url worked! => " + r.getStatus());

    }

}
