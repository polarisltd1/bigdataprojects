/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.polarisit.googlecloudstorageexample;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;
import com.google.common.collect.Lists;

import static com.google.api.client.util.IOUtils.copy;
import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.cloud.Page;
import com.google.cloud.RetryParams;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
//import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.logging.type.HttpRequest;
import com.tumra.datafeeds.restful.RestfulService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.channels.Channels;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Iterator;
import java.util.zip.GZIPOutputStream;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
//import com.google.api.services.samples.storage.util.CredentialsProvider;
import com.google.api.services.storage.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @author robertspolis
 */
public class GCSExamples {

    private static final String STORAGE_SCOPE
            = "https://www.googleapis.com/auth/devstorage.read_write";

    private static final Logger LOG = LogManager.getLogger(GCSExamples.class);
    String bucketName = "dcdt_-dcm_account177203";


    /*


//  https://cloud.google.com/appengine/docs/java/googlecloudstorageclient/javadoc/com/google/appengine/tools/cloudstorage/GcsOutputChannel
//  http://stackoverflow.com/questions/26777491/save-image-in-google-cloud-storage

     */
    public void test3GcsWrite() throws Exception {

        LOG.info("***** Gcs example! ********");

        final GcsService gcsService = GcsServiceFactory
                .createGcsService();
        GcsOutputChannel gcsOutputChannel = gcsService.createOrReplace(new GcsFilename("my-bucket", "log.json.gz"),
                new GcsFileOptions.Builder().build());
        GZIPOutputStream outputStream = new GZIPOutputStream(Channels.newOutputStream(gcsOutputChannel));

        // GcsInputChannel	openReadChannel(GcsFilename filename, long startPosition)
        GcsInputChannel inputChannel = gcsService
                .openReadChannel(new GcsFilename("my-bucket", "log.json"), 0);

        InputStream inStream = Channels.newInputStream(inputChannel);
        byte[] byteArr = new byte[10000];
        while (inStream.read(byteArr) > 0) {
            outputStream.write(byteArr);
        }

        LOG.info("*************");
    }

    public void test5GcsRead(String gcsBucket, String gcsFile) throws Exception {

        LOG.info("***** Gcs example! ********");
        //GcsOutputChannel gcsOutputChannel = gcsService.createOrReplace(new GcsFilename("my-bucket", "log.json.gz"),
        //new GcsFileOptions.Builder().build());
        //GZIPOutputStream outputStream = new GZIPOutputStream(Channels.newOutputStream(gcsOutputChannel));
/*
    
    GcsInputChannel	openPrefetchingReadChannel(GcsFilename filename, long startPosition, int blockSizeBytes)
    Same as openReadChannel, but buffers data in memory and prefetches it before it is required to avoid blocking on most read calls.

    
    createGcsService(RetryParams params) 
    
    GcsFileOptions fileOptions = new GcsFileOptions.Builder().mimeType("image/png").acl("public-read").build();
  GcsFilename filename = new GcsFilename(fileBucket, objectName); // fileBucket="test", objectName="/code/my/yeuei"
  GcsService service = GcsServiceFactory.createGcsService();
  GcsOutputChannel oc = service.createOrReplace(filename,fileOptions); //error happens here. 
    
    
    "GcsFileOptions.Builder.contentEncoding("gzip")"
    
    
    byte[] blobContent = new byte[0];

        try
        {
            GcsFileMetadata metaData = gcsService.getMetadata(fileName);
            int fileSize = (int) metaData.getLength();
            final int chunkSize = BlobstoreService.MAX_BLOB_FETCH_SIZE;

            LOG.info("content encoding: " + metaData.getOptions().getContentEncoding()); // "gzip" here
            LOG.info("input size " + fileSize);  // the size is obviously the compressed size!

            for (long offset = 0; offset < fileSize;)
            {
                if (offset != 0)
                {
                    LOG.info("Handling extra size for " + filePath + " at " + offset); 
                }

                final int size = Math.min(chunkSize, fileSize);

                ByteBuffer result = ByteBuffer.allocate(size);
                GcsInputChannel readChannel = gcsService.openReadChannel(fileName, offset);
                try
                {
                    readChannel.read(result);   <<<< here the exception was thrown
                }
                finally
                {
                    ......
    
    
    ByteArrayOutputStream output = new ByteArrayOutputStream();
try (GcsInputChannel readChannel = svc.openReadChannel(filename, 0)) {
  try (InputStream input = Channels.newInputStream(readChannel))
  {
    IOUtils.copy(input, output);
  }
}
    
   private void copy(InputStream input, OutputStream output) throws IOException {
		try {
			byte[] buffer = new byte[BUFFER_SIZE];
			int bytesRead = input.read(buffer);
			while (bytesRead != -1) {
				output.write(buffer, 0, bytesRead);
				bytesRead = input.read(buffer);
			}
		} finally {
			input.close();
			output.close();
		}
	}

    ----------
    
    
    GcsFilename keyAsAFileName = new GcsFilename(contentBucketName, key);
GcsFileMetadata gcsFileMetadata = gcsService.getMetadata(keyAsAFileName);
            ByteBuffer result = null;
            if (gcsFileMetadata != null) {
                result = ByteBuffer.allocate((int)gcsFileMetadata.getLength());
                if(gcsFileMetadata.getLength()>0){
                    readChannel = gcsService.openReadChannel(keyAsAFileName, 0);
                    readChannel.read(result);
                }
            }
    
    could there be authentification problem?
    
    https://cloud.google.com/storage/docs/authentication
    
    
	
9/16/15

    https://groups.google.com/forum/#!msg/google-appengine-stackoverflow/mVl-gpyIZdY/QZPvGkWeHgAJ

How do you run this code? This GCS client is specific for App Engine and should run by either a deployed app or locally using the AE dev appserver or unit-tests (which should configure the AE runtime environment using LocalServiceTestHelper).
    
   As @ozarov mentioned the client I was using is specific for App Engine. It was added through dependency

com.google.appengine.tools:appengine-gcs-client:0.5
Instead REST API client should be used. Its dependency is
=========================================================
    
    
com.google.apis:google-api-services-storage:v1-rev44-1.20.0
    
    javadoc:

https://developers.google.com/resources/api-libraries/documentation/storage/v1/java/latest/

https://developers.google.com/api-client-library/java/
    
    
    
Then the code to fetch files list may look as follows

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;
import com.google.api.services.storage.model.Objects;
import com.google.api.services.storage.model.StorageObject;
import com.google.common.collect.Lists;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.LinkedList;
import java.util.List;


class GCSFileStorage {
    String bucket = "bucket_name";
    String remoteDirectoryPath = "remote/path";
    Storage storage

    public GCSFileStorage() throws GeneralSecurityException, IOException {
        storage = setupStorage();
    }

    List<String> list() throws IOException {
        List<String> allItems = new LinkedList<String>();
        Objects response = storage.objects().list(bucket).
            setPrefix(remoteDirectoryPath).execute();
        for (StorageObject obj: response.getItems()) {
            allItems.add(obj.getName());
        }
        while (response.getNextPageToken() != null) {
            String pageToken = response.getNextPageToken();
            response = storage.objects().list(bucket).
                setPrefix(remoteDirectoryPath).setPageToken(pageToken).execute();
            for (StorageObject obj: response.getItems()) {
                allItems.add(obj.getName());
            }
        }
        return allItems;
    }


    Storage setupStorage() throws GeneralSecurityException, IOException {
        GoogleCredential credential = new GoogleCredential.Builder().
            setTransport(new NetHttpTransport()).
            setJsonFactory(new JacksonFactory()).
            setServiceAccountId("your_account_id").
            setServiceAccountScopes(
                Lists.newArrayList(StorageScopes.DEVSTORAGE_FULL_CONTROL)).
            setServiceAccountPrivateKeyFromP12File(
                new File("/local/path/to/private/key.p12")).
            build();

        return new Storage.
            Builder(new NetHttpTransport(),
                new JacksonFactory(), credential).
            setApplicationName("foo").build();
    }
} 
    
    
         */
        LOG.info("*1*");
        // GcsInputChannel	openReadChannel(GcsFilename filename, long startPosition)
        RetryParams params = RetryParams.newBuilder()
                .setInitialRetryDelayMillis(600000L)
                .setTotalRetryPeriodMillis(600000L)
                .setMaxRetryDelayMillis(600000L)
                .build();
        LOG.info("*2*");
        final GcsService gcsService = GcsServiceFactory
                .createGcsService();
        LOG.info("*3*");

        ListResult result = gcsService.list(gcsBucket, null);
        while (result.hasNext()) {
            ListItem item = result.next();
            LOG.info(item.getName());
        }

        GcsFilename fileName = new GcsFilename(gcsBucket, gcsFile);
        LOG.info("*4*");
        GcsInputChannel inputChannel = gcsService
                .openReadChannel(fileName, 0);

        LOG.info("about to request metadata");
        GcsFileMetadata metaData = gcsService.getMetadata(fileName);
        int fileSize = (int) metaData.getLength();
        LOG.info("Got metadata - GCS file size: {}", fileSize);

        File file = new File("/tmp/f");
        FileOutputStream fos = new FileOutputStream(file);
        InputStream inStream = Channels.newInputStream(inputChannel);
        byte[] byteArr = new byte[10000];
        while (inStream.read(byteArr) > 0) {
            fos.write(byteArr);
            LOG.info("fos.write!");
        }

        LOG.info("*************");

    }

    public void test4(String filePathStr) {

        LOG.info("***** Gcs example! ********");

        try {
            final GcsService gcsService = GcsServiceFactory
                    .createGcsService();

            File file = new File(filePathStr);
            FileInputStream fis = new FileInputStream(file);
            GcsFilename fileName = new GcsFilename("test1213", "test.jpg");
            GcsOutputChannel outputChannel;
            outputChannel = gcsService.createOrReplace(fileName, GcsFileOptions.getDefaultInstance());
            copy(fis, Channels.newOutputStream(outputChannel));
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOG.info("*************");

    }

}
