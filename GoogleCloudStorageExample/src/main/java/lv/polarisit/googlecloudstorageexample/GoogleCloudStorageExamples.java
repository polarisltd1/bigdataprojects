/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.polarisit.googlecloudstorageexample;

import com.google.cloud.Page;
import com.google.cloud.ReadChannel;





import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Blob.BlobSourceOption;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import java.nio.Buffer;
import java.nio.ByteBuffer;


import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//





/**
 *
 * @author robertspolis
 */
public class GoogleCloudStorageExamples {
    
        private static final String STORAGE_SCOPE =
      "https://www.googleapis.com/auth/devstorage.read_write";

         private static final Logger LOG = LogManager.getLogger(GoogleCloudStorageExamples.class);
    String bucketName = "dcdt_-dcm_account177203";

   


public void listBuckets(String bucketName){
    // Instantiates a client
    Storage storage = StorageOptions.getDefaultInstance().getService();

    // The name for the new bucket
    

    
    // Creates the new bucket
    if(false){ // we will not create buckets
        
       Bucket bucket = storage.create(BucketInfo.of(bucketName));

       LOG.info("Bucket {} created.%n", bucket.getName());

    }
    
    if(true){
        Page<Blob>  blist =	storage.list(bucketName); // Lists the bucket's blobs.
       Iterator<Blob> b = blist.iterateAll();
       Blob firstBlob = null;
       while(b.hasNext()) {
         Blob element = b.next();
         LOG.info( "file: {} size: {}",element.getName(),element.getSize());
         //if (firstBlob == null)firstBlob = element;
      }
     
       

    }
    
    
}



public void downloadFile(String bucketName, String filename)throws Exception{
   
        Storage storage = StorageOptions.getDefaultInstance().getService();
        
        
        //storage.
    
       //
        
       // java.lang.Object
       //    com.google.cloud.storage.BlobInfo
       //        com.google.cloud.storage.Blob
       //  
       //  Notice that Blob superclass is BlobInfo
        
        
       //  com.google.cloud.storage.BlobInfo
 
       //       String	getName()
       //      
       //    
       //  Returns the blob's name.
        
       // 
       
       BlobId myBlobId = BlobId.of(bucketName, filename);
       
       Blob myBlob = storage.get(myBlobId);
       
       LOG.info("About to download file:  {} {}",myBlob.getName(),myBlob.getSize());
       
       
       if(myBlob!=null){
            //byte[] content = myBlob.getContent();  // this is 3 GB
            
            //ReadChannel rc = myBlob.reader(null);

            //byte[] wholeContent = storage.readAllBytes(bucketName, myBlob,
        //null);
            read(myBlob);
            LOG.info("Samble blow download completed! name: {}  ", myBlob.getName());
       }
       

}

/**
   * Example of reading the blob's content through a reader.
   */
  // [TARGET reader(BlobSourceOption...)]
  public void read(Blob blob) throws Exception {
    // [START reader]
    try (ReadChannel reader = blob.reader()) {
      ByteBuffer bytes = ByteBuffer.allocate(64 * 1024*1024);
      while (reader.read(bytes) > 0) {        
        bytes.flip();
        byte[] buf = bytes.array();  
        LOG.info("Buffer received ...");
        bytes.clear();
      }
    }
    // [END reader]
  }





}


