package lv.polarisit.googlecloudstorageexample;

import com.google.cloud.Page;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by robertspolis on 06/02/17.
 */
public class MainProtoDCMv1 {

    public static void main(String... args) throws Exception {

        MainProtoDCMv1 m = new MainProtoDCMv1();



        List<String> filenames = m.listBucketFiles("dfa_-33d77f474eeacf3d0ddb5aac9de1b3e4f877e5ae");

        StringBuilder sb = new StringBuilder();

        StringBuilder sb2 = new StringBuilder();


        Set<String> names = new HashSet();


        for(String f:filenames){
          sb.append(f)
                  .append("\n");
          names.add(takeFilenameOfPath(f));

        }
        Path ps = Paths.get("./misc","dcs_v1_list.txt");
        System.out.println("=> "+ps.toAbsolutePath().toString()+" sz:"+sb.length());
        Files.write(ps, sb.toString().getBytes());


        //sb2.append(takeFilenameOfPath(f));

        List<String> namesL2=asSortedList(names);

        Path ps2 = Paths.get("./misc","dcs_v1_list_names.txt");
        System.out.println("=> "+ps2.toAbsolutePath().toString()+" sz:"+namesL2.size());
        Files.write(ps2, namesL2);


    }





    public static
    <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<T>(c);
        Collections.sort(list);
        return list;
    }


    public static String takeFilenameOfPath(String file) {

        String name = "";

        String[] parts = file.split("\\.");

        if (parts.length == 0) {
            System.out.println("cant split . in " + file);
        } else {

            String[] items = parts[0].split("_");
            int start = -1, stop = -1;
            List<String> namesIn = new ArrayList<>();
            for (int pos = 0; pos < items.length; pos++) {

                Pattern pattern = Pattern.compile(".*[a-zA-Z]+.*");
                Matcher matcher = pattern.matcher(items[pos]);

                if (matcher.matches())
                    namesIn.add(items[pos]);

            }


            String d = "";
            for (String s :namesIn) {
                name = name + d + s;
                d = "_";
            }

        }
        return name;
    }


    public List<String> listBucketFiles(String bucketName/*, String filePattern, Date updatedSince*/) throws Exception {
        // Instantiates a client
        Storage storage = StorageOptions.getDefaultInstance().getService();
        //
        List<String> listSelected = new ArrayList<>();
        Page<Blob> blist = storage.list(bucketName); // Lists the bucket's blobs.
        Iterator<Blob> b = blist.iterateAll();
        while (b.hasNext()) {
            Blob element = b.next();
            String filename = element.getName();

            //System.out.println(""+filename);

            //if (filename.contains(filePattern)) {
            //    if (updatedSince == null || updatedSince.compareTo(getFileTimestamp(filename, filePattern)) < 0) {
            //        // LOG.info("selected file: {} size: {}", element.getName(), element.getSize());
                    listSelected.add(filename);
            //    }
            //}
        }

        return listSelected;

    }


}
