/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lv.polarisit.googlecloudstorageexample;


// [START storage_quickstart]
// Imports the Google Cloud client library
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/*


How do I authenticate my Java application with Google Cloud Storage?
I am writing a Java application to interact with files in Google Cloud Storage. I found gcloud-java which I'm trying to get working.

Looking at their examples it seems I should be able to simply run them after having logged-in with gcloud, but it doesn't seem to be working. I'm trying to run StorageExample, which says "logged-in project will be used if not supplied", but despite logging in I cannot access my project whether I specify it or not.

gcloud auth login
Your browser has been opened to visit:

    https://accounts.google.com/o/oauth2/auth?....


Saved Application Default Credentials.

You are now logged in as [...].
Your current project is [...].  You can change this setting by running:
  $ gcloud config set project PROJECT_ID

$ java -cp GCloudDemo.jar com.google.gcloud.examples.storage.StorageExample list
Exception in thread "main" java.lang.IllegalArgumentException: A project ID is required for this service but could not be determined from the builder or the environment.  Please set a project ID using the builder.
        at com.google.common.base.Preconditions.checkArgument(Preconditions.java:122)
        at com.google.gcloud.ServiceOptions.<init>(ServiceOptions.java:317)
        ...
        at com.google.gcloud.examples.storage.StorageExample.main(StorageExample.java:566)

$ java -cp GCloudDemo.jar com.google.gcloud.examples.storage.StorageExample ... list
Exception in thread "main" com.google.gcloud.storage.StorageException: Login Required
        at com.google.gcloud.spi.DefaultStorageRpc.translate(DefaultStorageRpc.java:94)
        at co        

...........

The reason gcloud auth login isn't working is because Java doesn't play well with Cygwin, specifically with regards 
to absolute paths.

gcloud stores your settings and credentials in your Cygwin home directory, ~/.config/gcloud, but Java is going to 
look for them in your Windows home directory via System.getProperty("user.home"). You can specify where the application 
should look for this directory via the CLOUDSDK_CONFIG environment variable, like so (be sure to only specify it 
when calling java; setting it for your shell will in turn break gcloud):

$ CLOUDSDK_CONFIG=$(cygpath -w ~/.config/gcloud) \
  java -cp GCloudDemo.jar com.google.gcloud.examples.storage.StorageExample list 
Exception in thread "main" com.google.gcloud.storage.StorageException: Login Required
    at ....
Notice that that we didn't have to specify a project, but we're still not logged in for some reason. It turns out the demo app doesn't do a great job of reporting authentication errors. Adding an explicit call to AuthCredentials.createApplicationDefaults() at the start of the main() method reports a much more helpful error message:

Exception in thread "main" java.io.IOException: The Application Default Credentials are
not available. They are available if running in Google Compute Engine. Otherwise, the
environment variable GOOGLE_APPLICATION_CREDENTIALS must be defined pointing to a
file defining the credentials. See https://developers.google.com/accounts/docs/application-default-credentials
for more information.
So now we at least have the GOOGLE_APPLICATION_CREDENTIALS environment variable

---------------------------------------------------------

How to set up Project ID

https://cloud.google.com/appengine/docs/java/googlecloudstorageclient/setting-up-cloud-storage#setting_up_your_project

----------------------------------------------------------

Install gcloud (Google SDK)

https://cloud.google.com/sdk/downloads#interactive

Enter the following at a command prompt:
curl https://sdk.cloud.google.com | bash
Restart your shell:
exec -l $SHELL
Run gcloud init to initialize the gcloud environment:
gcloud init


---------------------------------------

After logging in with 'gloud init' getting following exception:

--- exec-maven-plugin:1.2.1:exec (default-cli) @ GoogleCloudStorageExample ---
Exception in thread "main" com.google.cloud.storage.StorageException: 401 Unauthorized

this is due to wrong bucket name! 

------------------------------

gcloud auth login

----------------------------

Robertss-MBP:009_GSDK robertspolis$ gcloud auth login
Your browser has been opened to visit:

    https://accounts.google.com/o/oauth2/auth?redirect_uri=http%3A%2F%2Flocalhost%3A8085%2F&prompt=select_account&response_type=code&client_id=32555940559.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcloud-platform+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fappengine.admin+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcompute+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Faccounts.reauth&access_type=offline


WARNING: `gcloud auth login` no longer writes application default credentials.
          !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
If you need to use ADC, see:
  gcloud auth application-default --help

You are now logged in as [feeds@tumra.com].
Your current project is [betfair-dcm-emea].  You can change this setting by running:
  $ gcloud config set project PROJECT_ID

=====================================

gcloud beta auth application-default login
//////////////////////////////////////////

Robertss-MBP:009_GSDK robertspolis$ gcloud beta auth application-default login
You do not currently have this command group installed.  Using it 
requires the installation of components: [beta]


Your current Cloud SDK version is: 141.0.0
Installing components from version: 141.0.0

┌─────────────────────────────────────────────┐
│     These components will be installed.     │
├──────────────────────┬────────────┬─────────┤
│         Name         │  Version   │   Size  │
├──────────────────────┼────────────┼─────────┤
│ gcloud Beta Commands │ 2016.01.12 │ < 1 MiB │
└──────────────────────┴────────────┴─────────┘

For the latest full release notes, please visit:
  https://cloud.google.com/sdk/release_notes

Do you want to continue (Y/n)?  Y

╔════════════════════════════════════════════════════════════╗
╠═ Creating update staging area                             ═╣
╠════════════════════════════════════════════════════════════╣
╠═ Installing: gcloud Beta Commands                         ═╣
╠════════════════════════════════════════════════════════════╣
╠═ Creating backup and activating new installation          ═╣
╚════════════════════════════════════════════════════════════╝

Performing post processing steps...done.                                                                                                                        

Update done!

Restarting command:
  $ gcloud beta auth application-default login


/////////////////////////////////

Sticking around this issue!

03:31:01.068 [pool-2-thread-1] ERROR com.tumra.datafeeds.service.ServiceGCS - auth error1:403 Forbidden
<?xml version='1.0' encoding='UTF-8'?><Error><Code>AccessDenied</Code><Message>Access denied.</Message><Details>Caller does not have storage.objects.list access to bucket dcdt_-dcm_account177203.</Details></Error>
03:31:01.611 [pool-2-thread-1] ERROR com.tumra.datafeeds.service.ServiceGCS - auth error2:Caller does not have storage.objects.list access to bucket dcdt_-dcm_account177203.
[ec2-user@ip-10-0-1-72 tumra-data-feeds]$ 

This permission is in list bellow:

https://cloud.google.com/storage/docs/access-control/iam-with-json-and-xml

only way to work around is to use magical gcloud

Install gcloud (Google SDK)

curl https://sdk.cloud.google.com | bash

Restart your shell:

Run gcloud init to initialize the gcloud environment:
gcloud init

then execute bellow, it will show url, paste it in any host web browser, get code and paste back

$ gcloud beta auth application-default login

Do not provide service account json through env var as it will trigger "Caller does not have storage.objects.list access to bucket "
#export GOOGLE_APPLICATION_CREDENTIALS=/home/ec2-user/tumra-data-feeds/BetfairDCMEMEA-d399ec40aa37.json

not sure why this way it did not work.


*/
public class Main {
    private static final Logger LOG = LogManager.getLogger(GoogleAPIClientExamples.class);
    private static final String bucketName = "dcdt_-dcm_account177203";
    
    
    private static final String downloadFileWithSmallSize = "dcm_account177203_click_2016112501_20161125_084126_501682067.csv.gz";
    
  public static void main(String... args) throws Exception {

      
      // GCS examples
      
      
      LOG.info("*** To make this running successfully please login into Google Cloud with following commandline which will open web browser authentification page");
      LOG.info("*** ");
      LOG.info("gcloud beta auth application-default login");
      LOG.info("*** ");
      LOG.info("*** API docs: http://googlecloudplatform.github.io/google-cloud-java/0.8.0/apidocs/index.html ");
      LOG.info("*** Docs: https://github.com/GoogleCloudPlatform/google-cloud-java/tree/master/google-cloud-storage");
      LOG.info("*** Read authentific section section  https://github.com/GoogleCloudPlatform/google-cloud-java#authentication");
      
      String file = "dcm_account177203_match_table_states_20170128_20170129_031324_521993439.csv.gz";
      //try{new GoogleAPIClientExamples().test5GcsRead(bucketName,file);}catch(Exception e){e.printStackTrace();}
      //try{new GoogleAPIClientExamples().test4();}catch(Exception e){e.printStackTrace();}

      
      //try{new GoogleAPIClientExamples().listBucket(bucketName);}catch(Exception e){e.printStackTrace();}
      try{new GoogleCloudStorageExamples().downloadFile(bucketName,downloadFileWithSmallSize);}catch(Exception e){e.printStackTrace();}
      //try{new GoogleAPIClientExamples().someTest();}catch(Exception e){e.printStackTrace();}
      
      // following url is taken from web console 
      String url = "https://storage.cloud.google.com/dcdt_-dcm_account177203/dcm_account177203_activity_20161125_20161126_035344_501824580.csv.gz?_ga=1.180999535.937671090.1485806432";
      try{new GoogleAPIClientExamples().download(bucketName,downloadFileWithSmallSize);}catch(Exception e){e.printStackTrace();}
      
    }
  }
