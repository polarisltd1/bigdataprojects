package lv.polarisit.javafx;



        import javafx.application.Application;
        import javafx.collections.FXCollections;
        import javafx.collections.ObservableList;
        import javafx.event.ActionEvent;
        import javafx.geometry.Insets;
        import javafx.geometry.Pos;
        import javafx.scene.Scene;
        import javafx.scene.control.*;
        import javafx.scene.image.Image;
        import javafx.scene.image.ImageView;
        import javafx.scene.layout.FlowPane;
        import javafx.scene.layout.HBox;
        import javafx.scene.layout.VBox;
        import javafx.scene.paint.Color;
        import javafx.stage.Modality;
        import javafx.stage.Stage;
        import lv.polarisit.yp.Main;
        import org.apache.logging.log4j.LogManager;
        import org.apache.logging.log4j.Logger;
        import org.jsoup.Jsoup;
        import org.jsoup.nodes.Document;
        import org.jsoup.nodes.Element;

        import java.io.FileInputStream;
        import java.io.InputStream;
        import java.net.URL;
        import java.util.List;

        import static lv.polarisit.yp.Main.getCrawlUrl;
        import static org.jsoup.nodes.Document.OutputSettings.Syntax.html;

/**
 * This code demonstrate simplest JavaFx to use to build basic GUI
 * It explores Java8 event handlers
 * and is used for application launch
 */


/**
 *
 * There is JavaFX showcase
 *
 * http://docs.oracle.com/javafx/2/ui_controls/overview.htm#
 *
 * Refcard:
 *
 * https://dzone.com/refcardz/javafx-8-1


*/
public class GUI extends Application {

    private static final Logger LOG = LogManager.getLogger(GUI.class);

    private Main crawler;

    final String INTRO =
            "This is YP webcrawling application dedicated for  Billy. Enjoy!\n\n"
           +"First make window wider so you can access entire window and see listbox with cities.\n\n"
            +"Just first start entering some letters into text box and see what we found in listbox, then choose listox item and press on Crawl button. Then sit peacefully while it runs. Couldnt be quick!";

    public void test(ActionEvent e){
        System.out.println("Hello World method references");
    }

    public static void testStatic(ActionEvent e){
        LOG.debug("Hello World method references static");
    }

    @Override
    public void start(Stage primaryStage) throws Exception{


        primaryStage.setOnCloseRequest(event -> {
            LOG.debug("REQUEST TO CLOSE APP");
            System.exit(1);
        });


        primaryStage.setMinHeight(600.0);
        primaryStage.setMinWidth(100.0);
        crawler = new Main();

        // textfield
        Label labelCitySearch = new Label("Name:");
        TextField textCitySearch = new TextField();



        // listbox
        ListView<String> list = new ListView<String>();
        ObservableList<String> items = FXCollections.observableArrayList (
                "There", "is", "nothing", "yet!");
        list.setItems(items);
        list.setPrefWidth(50);
        list.setPrefHeight(300);
        // buttons
        Button btnCitySearch = new Button();
        Button btn2 = new Button();


        textCitySearch.setOnAction(s->LOG.info("City Search Field: {}",textCitySearch.getText()));


        textCitySearch.textProperty().addListener((observable, oldValue, newValue) -> {
            LOG.info("City Search Field from {} to {}" , oldValue , newValue);
            if(newValue.length()>2){
                List<String> cities = crawler.parseCities(newValue);
                cities.forEach(it->LOG.info("lookup: {}",it));

                ObservableList<String> items1 = FXCollections.observableArrayList (
                        cities);
                list.setItems(items1);
                btnCitySearch.setDisable(false);

            }else{
                btnCitySearch.setDisable(true);
            }
        });


        HBox hbCitySearch = new HBox();
        hbCitySearch.getChildren().addAll(labelCitySearch, textCitySearch);
        hbCitySearch.setSpacing(10);




        btnCitySearch.setText("Proceed YP Crawling");
        btn2.setText("Say 'Hello World Webcrawling'");

        // Processing city search
        btnCitySearch.setOnAction(e->{
            LOG.info("City Search! {}",textCitySearch.getText());
            // LEARN: access selected index
            //int selectedInd = list.getSelectionModel().getSelectedIndex();
            String selected = list.getSelectionModel().getSelectedItem();
            try {
                LOG.info("Going for Crawling with city pattern = '{}'",selected);
                ////
                FlowPane pane2;
                pane2=new FlowPane();
                pane2.setMinWidth(150);
                pane2.setVgap(10);
                //set background color of each Pane

                pane2.setStyle("-fx-background-color:tan;-fx-padding:10px;");
                //add everything to panes
                Label lblscene2 = new Label("Crawling is running.\nPlease press Stop to terminate!");
                Button  btnscene2Stop=new Button("Stop");
                btnscene2Stop.setAlignment(Pos.CENTER);
                VBox scene2vbox = new VBox();
                scene2vbox.setSpacing(10);
                //scene2vbox.setPadding(new Insets(50, 30, 50, 30));


                scene2vbox.getChildren().addAll( lblscene2,btnscene2Stop);



                pane2.getChildren().add(scene2vbox);



                //////
                Scene scene2 = new Scene(pane2, 200, 100);
                //make another stage for scene2
                Stage newStage = new Stage();
                newStage.setScene(scene2);
                //tell stage it is meannt to pop-up (Modal)
                newStage.initModality(Modality.APPLICATION_MODAL);
                newStage.setTitle("Pop up window");




                Runnable task2 = () -> {

                    try{crawler.execute(selected);}catch(Exception ex){LOG.error("Exception {}",ex);}
                    newStage.close(); // close popup!

                };
                // start the thread
                new Thread(task2).start();

                btnscene2Stop.setOnAction(s->{LOG.info("Requested stop!");
                    System.exit(0);
                });


                newStage.showAndWait();

                //LOG.info("Finished!");
                //newStage.close();

            }catch(Exception ex){
                LOG.error("Exception while crawling {} {}",selected,ex);
            }
        });

        // method references
        btn2.setOnAction(this::test);
        //// method references (static method)
        //btn2.setOnAction(ButtonJavaFX8Lambda::testStatic);


        MenuItem menuItem1 = new MenuItem("We do this");
        MenuItem menuItem2 = new MenuItem("We do that");
        MenuItem menuItem3 = new MenuItem("And even more");
        MenuItem menuItem4 = new MenuItem("For You");

        //menuItem1.setOnAction(event -> new PivotTableWithLambdas1().execute());
        //menuItem2.setOnAction(event -> new Java8Streams1().execute());
        //menuItem3.setOnAction(event -> new OcpPrepStreams().execute());
        //menuItem4.setOnAction(event -> new Java8DateTimeAPI().execute());




        /**
         * download icons at https://iconmonstr.com/spotify-5/?png
         */



        ImageView imageView = new ImageView(new Image(getResourceStream("iconmonstr-tag-19-32.png")));

        MenuButton menuButton = new MenuButton("Options", imageView, menuItem1, menuItem2, menuItem3,menuItem4);


        Label labelIntro = new Label(INTRO);
        labelIntro.setTextFill(Color.web("#0076a3"));
        labelIntro.setTooltip(new Tooltip("Read this ..."));
        labelIntro .setWrapText(true);


        VBox root = new VBox();
        root.setSpacing(10);
        root.setPadding(new Insets(50, 30, 50, 30));


        root.getChildren().add(labelIntro );

        root.getChildren().add(hbCitySearch);




        root.getChildren().add(list);

        root.getChildren().add(btnCitySearch);


        root.getChildren().add(menuButton);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("Yellow Pages WebCrawler!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * access resources folder content through classloader
     * @param path
     * @return
     */
    InputStream getResourceStream(String path){
       return Thread.currentThread().getContextClassLoader().getResourceAsStream(path);
    }




}
