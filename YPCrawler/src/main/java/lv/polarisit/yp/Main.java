package lv.polarisit.yp;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import static java.util.stream.Collectors.joining;
import static org.jsoup.nodes.Document.OutputSettings.Syntax.html;

/**
 * Created by robertspolis on 17/06/2017.
 *

 *
 */
public class Main {

    private boolean requestStop = false;

    private String cityArg;

    private static final Logger LOG = LogManager.getLogger(Main.class);

    static String[] CITIES = {"Minneapolis+KS","Los+Angeles+CA"};

    String  ROOT_PATH = "/Users/robertspolis/001/crawler/";

    List<String> ascii = new ArrayList<>();

    public static void main(String[] args) throws Exception {



        new Main().execute(CITIES[1]);


    }


    public void execute(String cityArg) throws Exception{

        this.cityArg = cityArg.replaceAll(", ","+").replaceAll(" ","+");
        crawl(getCrawlUrl(cityArg));
    }

    public void setRequestStop(){
        requestStop=true;
    }

    public static String getCrawlUrl(String cityPattern) throws Exception{

        return String.format("https://www.yellowpages.com/search?search_terms=restaurant&geo_location_terms=%s",cityPattern);

    }


    public void csvFormat(StringBuilder output,String... fields){

        String line = Arrays.asList(fields).stream().collect(joining("|", "", ""));
        output.append(line).append("\n");

    }

    public void csvReset(StringBuilder output){
        output.delete(0,output.length());
    }

    public void crawl(String url) throws IOException {

        LOG.info("Crawl URL: {}",url);
        StringBuilder lines = new StringBuilder();

        //if (requestStop){
        //    requestStop=false;
        //    return;
        //}

        if(url==null)return; // we done!

        final List<Element> result = new ArrayList<>();


        //Path csvPath = Paths.get(pathStr+".csv");

        //**LEARN:
        //try {
        //    Files.delete(l); // we are overwriting
        //}catch(Exception e){}

        LOG.info("NEXT SEARCH PAGE: {}",url);

        Document doc = Jsoup.connect(url).timeout(10*1000).get();


        StringBuilder sb = new StringBuilder();

        //sb.append(debugElements1(doc));

        //sb.append("\n============================================================================ \n\n");


        String[] EXPR1 = new String[]{"div[itemtype=\"//schema.org/Restaurant\"]",  "div.info"};

        result.clear();
        doExpr(doc,Arrays.asList(EXPR1),result); // return elements for given expression list

        LOG.debug(  "restaurant items size: " , result.size());


        result.forEach(d2->


        {

            String websitePageUrl=null;
            String bookATable="-";
            String orderOnline="-";

            Element landingPage = d2.select("a.business-name").first();
            Element websitePage = d2.select("a.track-visit-website").first();

            //sb.append("\n");
            if(websitePage!=null) {
                websitePageUrl = websitePage.attr("abs:href");
                LOG.info(" Website: {}",((websitePage!=null)?websitePageUrl:"n/a"));
            }
            if(landingPage!=null) {
                String landingPageUrl = landingPage.attr("abs:href");
                LOG.info(  " LandingPage: {}",((landingPage!=null)?landingPageUrl:"n/a")+"\n");
                //writeToFileHtml("land_",landingPageUrl); // save to disk for debugg


                // parse landing page
                // - check there is book a table <section id="book-a-table">
                // check there is Order Online
                // if yes, take referenced page
                //    if page.text() contains www.grubhub.com
                //    if page.text() contains www.bringmethat.com

                final List<Element> result2 = new ArrayList<>();

                try {

                    Document landingPageDoc = Jsoup.connect(landingPageUrl).timeout(10 * 1000).get();

                    // find book a table
                     final String[] EXPR_BOOK_A_TABLE = new String[]{"section#book-a-table"};

                    result2.clear();
                    doExpr(landingPageDoc,Arrays.asList(EXPR_BOOK_A_TABLE),result2);

                    if(result2.size()>0){LOG.info(" -BOOK A TABLE- ");bookATable="BOOK_A_TABLE";}

                    // find order online link
                    String[] EXPR_ORDER_ONLINE = new String[]{"a.order-online"};


                    result2.clear();
                    doExpr(landingPageDoc,Arrays.asList(EXPR_ORDER_ONLINE),result2); // return elements for given expression list

                    if(result2.size()>0){


                        String orderOnlineUrl = result2.get(0).attr("abs:href");
                        LOG.info(  " -ORDER ONLINE- {}",orderOnlineUrl);
                        Document orderOnlineDoc = Jsoup.connect(orderOnlineUrl).timeout(10 * 1000).get();
                        if(orderOnlineDoc.html().contains("www.grubhub.com")){LOG.info(  " -GRUBHUB- ");orderOnline="GRUBHUB";}
                        if(orderOnlineDoc.html().contains("www.bringmethat.com")){LOG.info(" -BRINGMETHAT- ");orderOnline="BRINGMEAT";}

                    }

                }catch(Exception e){
                    e.printStackTrace();
                }
                if(websitePageUrl!=null)csvFormat(lines,websitePageUrl,bookATable,orderOnline);


            }



        }
        );

        appendCsv(lines.toString());

        // check do we got pagination
        String[] EXPR2 = new String[]{"div.pagination","a.next"};

        result.clear();
        doExpr(doc,Arrays.asList(EXPR2),result); // return elements for given expression list

        if(result.size()>0){ // we got pagination

           Element el = result.get(0);  // there should be only single page
           String nextSearchResultPageUrl = el.attr("abs:href");
           crawl(nextSearchResultPageUrl);

        }LOG.info(  "NO PAGINATION FOLLOWS ...");


    }




    /**
     * Parsing Restaurants from City Page
     * @param doc
     * @return
     */
    StringBuilder debugElements1(Document doc) {

        StringBuilder sb = new StringBuilder();

        String expr = "div[itemtype=\"//schema.org/Restaurant\"]";

        Elements links = doc.select(expr);

        for (Element link : links) {
            LOG.debug("1. {} {}",expr , link.text());

            // can we list all attributes?

            link.attributes().forEach(s -> LOG.debug("{} = {}",s.getKey() , s.getValue()));


            Elements links2 = link.select("div[class=\"info\"]");

            if(links2!=null){
                Element d2 = links2.first();
                Elements links3 = d2.select("a[href]"); // looking for a under div/info
                links3.forEach(el->  {el.attributes().forEach(a->LOG.debug("s: {} = {} ",a.getKey(),a.getValue()));
                });

                Element landingPage = d2.select("a[class=\"business-name\"]").first();
                Element websitePage = d2.select("a[class=\"track-visit-website\"]").first();

                //sb.append("\n");
                LOG.debug(" Website: "+((websitePage!=null)?websitePage.attr("href"):"n/a"));
                LOG.debug(" LandingPage: "+((landingPage!=null)?landingPage.attr("href"):"n/a"));
                //sb.append("\n");

            }
        }

        return sb;
    }

//**learn!
//Arrays.stream(expressions).forEach(


    /**
     * Resolve document through given expressions
     * @param e
     * @param exprl
     * @param result
     */

    public void doExpr(Element e,List<String> exprl,List<Element> result){

        Elements els = e.select(exprl.get(0));
        LOG.debug("doExpr {} = {}",exprl.get(0),els.size());
        els.forEach(el -> {
                if(exprl.size()==1){/*add result to list*/result.add(el);}
                else doExpr(el,tail(exprl),result);
                 }
        );


    }


    public List head(List list) {
        return list.subList(0, 1);
    }

    public List tail(List list) {
        return list.subList(1, list.size());
    }




    public void appendCsv(String data) {
        Path basePath = Paths.get(System.getProperty("user.dir"),"crawler");
        if (!Files.exists(basePath)){
            try {
                Files.createDirectory(basePath);
            } catch (IOException e) {
                LOG.error("Exception creating directory {} {}", basePath, e);
            }
        }

        Path csvPath=null;
        try {
            csvPath = Paths.get(basePath.toString(), URLEncoder.encode(cityArg, "UTF-8") + ".csv");
        }catch(Exception e){
            LOG.error("Error encoding url :{}",e);
        }

        LOG.info("Writing CSV {}",csvPath.toAbsolutePath().toString());
        try (OutputStream os = new BufferedOutputStream(
                        Files.newOutputStream(csvPath, StandardOpenOption.CREATE, StandardOpenOption.APPEND))) { //
            os.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void writeToFileHtml(String filenamePfx,Document doc){
        try {
            String url = doc.location();
            String pathStr = ROOT_PATH + filenamePfx+URLEncoder.encode(url, "UTF-8");
            Path f = Paths.get(pathStr + ".html");
            Files.write(f, doc.html().getBytes());
        }catch(Exception e){ e.printStackTrace();
        }

    }


    public void writeToFileHtml(String filenamePfx,String url){
        try {
            if(url==null)return;
            Document doc = Jsoup.connect(url).timeout(10*1000).get();
            if(doc==null)return;
            String pathStr = ROOT_PATH + filenamePfx+ URLEncoder.encode(url, "UTF-8");
            Path f = Paths.get(pathStr + ".html");
            Files.write(f, doc.html().getBytes());
        }catch(Exception e){ e.printStackTrace();
        }

    }


    public List<String> parseCities(String text){
        /**
         *
         * query to find usa cities
         * ========================
         * http://geobytes.com/free-ajax-cities-jsonp-api/
         *
         * http://gd.geobytes.com/AutoCompleteCity?q=Ali&q=US
         * http://gd.geobytes.com/GetCityDetails?fqcn=Alice,%20TX,%20United%20States
         *
         * Just use same as YP is using
         * ============================
         * https://www.yellowpages.com/autosuggest/location.html?location=ALt
         */
        List<String> ls = new ArrayList<>();
        try {

            String url = String.format("https://www.yellowpages.com/autosuggest/location.html?location=%s", text);
            Document urlDoc = Jsoup.connect(url).timeout(10 * 1000).get();
            //Document doc = Jsoup.parseBodyFragment(html);
            Elements items = urlDoc.select("li[data-value]");

            items.forEach(
                    (Element s) -> {ls.add(s.attr("data-value"));
                    }
            );
            return ls;
        }catch(Exception e){
            LOG.error("Exception thrown processing cities: ",e);
            ls.add("Sorry error happened ...");
            ls.add("Try later or call me ... roberts.polis@gmail.com");
            ls.add(e.getMessage());
            return ls;
        }


    }


}



