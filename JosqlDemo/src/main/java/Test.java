public class Test{

    private String f1;
    private String f2;
    private Integer n;

    public Test(String f1,String f2,Integer n){
        this.setF1(f1);
        this.setF2(f2);
        this.setN(n);
    }


    public String getF1() {
        return f1;
    }

    public void setF1(String f1) {
        this.f1 = f1;
    }

    public String getF2() {
        return f2;
    }

    public void setF2(String f2) {
        this.f2 = f2;
    }

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }
}