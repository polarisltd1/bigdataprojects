https://www.lightbend.com/activator/download



create projects from templates
$ activator ui


Create a new project from the command line:
 $ activator new

  
Run an existing project from its directory:
 $ activator run


Enter the interactive cli (within a project directory):
 $ activator shell 


Read the complete documentation to learn more about the Activator CLI.