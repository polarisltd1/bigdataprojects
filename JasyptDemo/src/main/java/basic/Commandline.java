package basic;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;

import java.util.List;

/**
 * Command-line options definition for example server.
 */
public class Commandline extends OptionsBase {

    @Option(
            name = "demo",
            //abbrev = 'd',
            help = "demo encr/decr",
            defaultValue = "false"
    )
    public boolean isDemo;

    @Option(
            name = "encrypt",
            abbrev = 'e',
            help = "encrypt",
            defaultValue = "false"
    )
    public boolean isEncrypt;


    @Option(
            name = "decrypt",
            abbrev = 'd',
            help = "decrypt",
            defaultValue = "false"
    )
    public boolean isDecrypt;




    @Option(
            name = "input",
            abbrev = 'i',
            help = "input string",
            category = "startup",
            defaultValue = ""
    )
    public String input;



}
