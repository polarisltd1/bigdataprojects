package basic;

import com.google.devtools.common.options.OptionsParser;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;

import java.util.Collections;

import static com.sun.corba.se.spi.activation.IIOP_CLEAR_TEXT.value;

/**
 * Created by robertspolis on 30/06/2017.
 *
 * execute example
 *
 *
 *
 *
 * java -jar JasyptDemo-1.0-SNAPSHOT-jar-with-dependencies.jar -e -i blablabla
 *
 *  Input source message : blablabla
 *  3pmzrb77YwPFMAHrWN4X95xd07yvZ2ud
 *
 * java -jar JasyptDemo-1.0-SNAPSHOT-jar-with-dependencies.jar -d -i 3pmzrb77YwPFMAHrWN4X95xd07yvZ2ud
 *
 * Input crypted message : 3pmzrb77YwPFMAHrWN4X95xd07yvZ2ud
 * blablabla
 *
 * and similar from commandline
 *
 * ./encrypt.sh input="This is my message to be encrypted" password=MYPAS_WORD
 *
 * ./decrypt.sh input="k1AwOd5XuW4VfPQtEXEdVlMnaNn19hivMbn1G4JQgq/jArjtKqryXksYX4Hl6A0e" password=MYPAS_WORD


 https://wiki.jasig.org/display/CASUM/HOWTO+Use+Jasypt+to+encrypt+passwords+in+configuration+files

 documentation
 http://www.jasypt.org/howtoencryptuserpasswords.html
 commandline help
 http://www.jasypt.org/cli.html

 java -cp jasypt-1.9.1.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input="XXXX" password=mulesoft algorithm=PBEWithMD5AndDES


 <dependency>
 <groupId>org.jasypt</groupId>
 <artifactId>jasypt</artifactId>
 <version>1.9.1</version>
 <scope>provided</scope>
 </dependency>

 There are two important points to remember:
 1. Tell Jasypt that the value is encrypted by wrapping it in the ENC "function" -

 dataSource.password=ENC(1oqwimyLNS+hRJ240Rhkgm9FYZudI5qs)
 dataSource.username=ENC(jd5ZREpBqxuN9ok0IhnXabgw7V3EoG2p)

 2. Use the same PBE password for all encrypted values.

 *
 */
public class EncryptionDecryptionUsingJASYPT  extends OptionsBase  {

    private static String mpCryptoPassword = "JAVA9";

    public static void main(String[] args) {


        OptionsParser parser = OptionsParser.newOptionsParser(Commandline.class);
        parser.parseAndExitUponError(args);
        Commandline options = parser.getOptions(Commandline.class);

        printUsage(parser);

        if(options.isDemo) {

            String value = "Original Text: Eclipse";

            System.out.println("Original Value : " + value);
            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword(mpCryptoPassword);
            String encryptedPassword = encryptor.encrypt(value);
            System.out.println(encryptedPassword);

            StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
            decryptor.setPassword(mpCryptoPassword);
            System.out.println(decryptor.decrypt(encryptedPassword));

        }else if(options.isEncrypt){
            System.out.println("Input source message : " + options.input);
            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword(mpCryptoPassword);
            String encryptedPassword = encryptor.encrypt(options.input);
            System.out.println(encryptedPassword);
        }else if(options.isDecrypt){
            System.out.println("Input crypted message : " + options.input);

            StandardPBEStringEncryptor decryptor = new StandardPBEStringEncryptor();
            decryptor.setPassword(mpCryptoPassword);
            System.out.println(decryptor.decrypt(options.input));

        }


    }

    private static void printUsage(OptionsParser parser) {
        System.out.println("Usage: java -jar my.jar OPTIONS");
        System.out.println(parser.describeOptions(Collections.<String, String>emptyMap(),
                OptionsParser.HelpVerbosity.LONG));
    }



}
