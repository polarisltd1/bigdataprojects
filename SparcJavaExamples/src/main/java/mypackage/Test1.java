/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

import java.util.Arrays;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.apache.avro.Schema; 
import org.apache.log4j.Logger; 
import org.apache.spark.SparkConf; 
import org.apache.spark.api.java.JavaPairRDD; 
import org.apache.spark.api.java.JavaRDD; 
import org.apache.spark.api.java.JavaSparkContext; 
import org.apache.spark.broadcast.Broadcast; 
//import org.apache.spark.sql.DataFrame; 
import org.apache.spark.sql.Row; 
import org.apache.spark.sql.RowFactory; 
import org.apache.spark.sql.SQLContext; 
import org.apache.spark.sql.types.DataType; 
import org.apache.spark.sql.types.DataTypes; 
import org.apache.spark.sql.types.StructField; 
import org.apache.spark.sql.types.StructType; 
 import java.util.Arrays;
import java.util.Collections;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.spark.api.java.function.Function;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
//import com.databricks.spark.avro.AvroSaver; 

/**
 *
 * @author robertspolis
 */
public class Test1  implements Serializable{
    
    
    
public void downloadS3(JavaSparkContext sc){
   
}    
 
public void test1(JavaSparkContext sc) throws Exception{
    
    
        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 2, 3, 4));
        
        //
        //
        //
        
        Person person = new Person();
        person.setName("Andy");
        person.setAge(32);

         
        
        /*
        JavaRDD<Integer> result = rdd.map(
                new Function<Integer, Integer>() {
            public Integer call(Integer x) {
                return x * x;
            }
        });
        System.out.println(StringUtils.join(result.collect(), ","));
        */
 
    
    
    
    
}    
 


private void artistsAsDataFrame()  throws Exception{
	//String input = TestUtils.sampleArtistsDat();
	//JavaRDD<String> data = sc.textFile(input);

	/*
	JavaRDD<Row> rowData = data.map(new Function<String, String[]>() {
		@Override
		public String[] call(String line) throws Exception {
			return line.split("\t");
		}
	}).map(new Function<String[], Row>() {
		@Override
		public Row call(String[] r) throws Exception {
			return RowFactory.create(Integer.parseInt(r[0]), r[1], r[2], r[3],
					new Timestamp(DatatypeConverter.parseDateTime(r[4]).getTimeInMillis()));
		}
	});

	return sqc.createDataFrame(rowData, schema);
        */
}
 /*   
private static void createWeather(JavaSparkContext sc, SQLContext sqlContext) {
	JavaRDD<Weather> weather = sc.textFile("../173328.csv").map((String line) -> {
		return WeatherParser.parseWeather(line);
	});

	StructType schema = getStructType(new Schema[] { Weather.SCHEMA$ });

	JavaRDD<Row> rowRDD = weather.map((Weather weatherRow) -> {
		return RowFactory.create(weatherRow.get(0), weatherRow.get(1), weatherRow.get(2), weatherRow.get(3),
				weatherRow.get(4), weatherRow.get(5), weatherRow.get(6), weatherRow.get(7), weatherRow.get(8),
				weatherRow.get(9), weatherRow.get(10), weatherRow.get(11), weatherRow.get(12), weatherRow.get(13),
				weatherRow.get(14), weatherRow.get(15), weatherRow.get(16), weatherRow.get(17), weatherRow.get(18),
				weatherRow.get(19), weatherRow.get(20), weatherRow.get(21), weatherRow.get(22), weatherRow.get(23),
				weatherRow.get(24), weatherRow.get(25), weatherRow.get(26), weatherRow.get(27), weatherRow.get(28),
				weatherRow.get(29), weatherRow.get(30), weatherRow.get(31), weatherRow.get(32), weatherRow.get(33),
				weatherRow.get(34), weatherRow.get(34), weatherRow.get(36), weatherRow.get(37), weatherRow.get(38),
				weatherRow.get(39), weatherRow.get(40), weatherRow.get(41), weatherRow.get(42), weatherRow.get(43),
				weatherRow.get(44), weatherRow.get(45), weatherRow.get(46), weatherRow.get(47), weatherRow.get(48));
	});

	// Apply the schema to the RDD.
	DataFrame weatherFrame = sqlContext.createDataFrame(rowRDD, schema);
	weatherFrame.registerTempTable("weather");
}
 
*/




private static void createStadiums(JavaSparkContext sc, SQLContext sqlContext)  throws Exception{
    /*
	JavaRDD<Stadium> stadiums = sc.textFile("../stadiums.csv").map((String line) -> {
		return StadiumParser.parseStadium(line);
	});

	StructType schema = getStructType(new Schema[] { Stadium.SCHEMA$ });

	JavaRDD<Row> rowRDD = stadiums.map((Stadium stadiumRow) -> {
		return RowFactory.create(stadiumRow.get(0), stadiumRow.get(1), stadiumRow.get(2), stadiumRow.get(3),
				stadiumRow.get(4), stadiumRow.get(5), stadiumRow.get(6), stadiumRow.get(7), stadiumRow.get(8),
				stadiumRow.get(9), stadiumRow.get(10));
	});
*/
	// Apply the schema to the RDD.
        
        StructType schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("id", DataTypes.IntegerType, false),
					DataTypes.createStructField("name", DataTypes.StringType, false),
					DataTypes.createStructField("url", DataTypes.StringType, true),
					DataTypes.createStructField("pictures", DataTypes.StringType, true),
					DataTypes.createStructField("time", DataTypes.TimestampType, true) });

        
        
	//DataFrame stadiumFrame = sqlContext.createDataFrame(rowRDD, schema);
	//stadiumFrame.registerTempTable("stadium");
}
 

////////
public void javabeanDS(SparkSession spark) throws Exception{
    

// Create an instance of a Bean class
Person person = new Person();
person.setName("Andy");
person.setAge(32);

// Encoders are created for Java beans
Encoder<Person> personEncoder = Encoders.bean(Person.class);
Dataset<Person> javaBeanDS = spark.createDataset(
  Collections.singletonList(person),
  personEncoder
);
javaBeanDS.show();
// +---+----+
// |age|name|
// +---+----+
// | 32|Andy|
// +---+----+

// Encoders for most common types are provided in class Encoders
Encoder<Integer> integerEncoder = Encoders.INT();
Dataset<Integer> primitiveDS = spark.createDataset(Arrays.asList(1, 2, 3), integerEncoder);
Dataset<Integer> transformedDS = primitiveDS.map(new MapFunction<Integer, Integer>() {
  @Override
  public Integer call(Integer value) throws Exception {
    return value + 1;
  }
}, integerEncoder);
transformedDS.collect(); // Returns [2, 3, 4]

// DataFrames can be converted to a Dataset by providing a class. Mapping based on name
String path = "src/main/resources/people.json";
Dataset<Person> peopleDS = spark.read().json(path).as(personEncoder);
peopleDS.show();

}
////////

public void programaticSchema(SparkSession spark) throws Exception{
    // Create an RDD
JavaRDD<String> peopleRDD = spark.sparkContext()
  .textFile("src/main/resources/people.txt", 1)
  .toJavaRDD();

// The schema is encoded in a string
String schemaString = "name age";

// Generate the schema based on the string of schema
List<StructField> fields = new ArrayList<>();
for (String fieldName : schemaString.split(" ")) {
  StructField field = DataTypes.createStructField(fieldName, DataTypes.StringType, true);
  fields.add(field);
}
StructType schema = DataTypes.createStructType(fields);

// Convert records of the RDD (people) to Rows
JavaRDD<Row> rowRDD = peopleRDD.map(new Function<String, Row>() {
  @Override
  public Row call(String record) throws Exception {
    String[] attributes = record.split(",");
    return RowFactory.create(attributes[0], attributes[1].trim());
  }
});

// Apply the schema to the RDD
Dataset<Row> peopleDataFrame = spark.createDataFrame(rowRDD, schema);

// Creates a temporary view using the DataFrame
peopleDataFrame.createOrReplaceTempView("people");

// SQL can be run over a temporary view created using DataFrames
Dataset<Row> results = spark.sql("SELECT name FROM people");

// The results of SQL queries are DataFrames and support all the normal RDD operations
// The columns of a row in the result can be accessed by field index or by field name
Dataset<String> namesDS = results.map(new MapFunction<Row, String>() {
  @Override
  public String call(Row row) throws Exception {
    return "Name: " + row.getString(0);
  }
}, Encoders.STRING());
namesDS.show();
// +-------------+
// |        value|
// +-------------+
// |Name: Michael|
// |   Name: Andy|
// | Name: Justin|
// +-------------+
}

//////////////
public void test5(SparkSession spark){
    
    // Can this be fixed?
    
    
    //JavaRDD<String> lines = spark.read().text("file").javaRDD();
    //JavaRDD<DataPoint> points = lines.map(new ParsePoint()).cache();
   /*
    int ITERATIONS = Integer.parseInt(args[1]);
    // Initialize w to a random value
    double[] w = new double[D];
    for (int i = 0; i < D; i++) {
        w[i] = 2 * rand.nextDouble() - 1;
    }
    System.out.print("Initial w: ");
    //printWeights(w);
    for (int i = 1; i <= ITERATIONS; i++) {
        System.out.println("On iteration " + i);
        double[] gradient = points.map(new ComputeGradient(w)).reduce(new VectorSum());
        for (int j = 0; j < D; j++) {
            w[j] -= gradient[j];
        }
    }
    System.out.print("Final w: ");
    //printWeights(w);
*/
/*

   
about Java 8 usage
   
https://databricks.com/blog/2014/04/14/spark-with-java-8.html   

RDD Basics
An RDD in Spark is simply an immutable distributed collection of objects. Each RDD is split into multiple partitions, which may be 
computed on different nodes of the cluster. RDDs can contain any type of Python, Java, or Scala objects, including user-defined classes.

Users create RDDs in two ways: by loading an external dataset, or by distributing a collection of objects (e.g., a list or set) in their 
driver program. We have already seen loading a text file as an RDD of strings using SparkContext.textFile()

-----------
   
If you’re new to Spark, JavaRDD is a distributed collection of objects, in this case lines of text in a file. 
We can apply operations to these objects that will automatically be parallelized across a cluster.  
 
Examples   
   
Plain reading should be done like this :

spark.read().text("/path/to/spark/README.md")   
   
   

i.   
   
Dataset<Row> peopleDF;  
peopleDF.toJavaRDD(); // what are we getting JavaRDD out. that not interesting ...  
 
ii.

Dataset<Row> peopleDataFrame = spark.createDataFrame(rowRDD, schema);  
   
example can be like this

JavaRDD<Weather> weather = sc.textFile("../173328.csv").map((String line) -> {
		return WeatherParser.parseWeather(line);
});

	StructType schema = getStructType(new Schema[] { Weather.SCHEMA$ });

	JavaRDD<Row> rowRDD = weather.map((Weather weatherRow) -> {
		return RowFactory.create(weatherRow.get(0), weatherRow.get(1), weatherRow.get(2), weatherRow.get(3),
				weatherRow.get(4), weatherRow.get(5), weatherRow.get(6), weatherRow.get(7), weatherRow.get(8),
				weatherRow.get(9), weatherRow.get(10), weatherRow.get(11), weatherRow.get(12), weatherRow.get(13),
				weatherRow.get(14), weatherRow.get(15), weatherRow.get(16), weatherRow.get(17), weatherRow.get(18),
				weatherRow.get(19), weatherRow.get(20), weatherRow.get(21), weatherRow.get(22), weatherRow.get(23),
				weatherRow.get(24), weatherRow.get(25), weatherRow.get(26), weatherRow.get(27), weatherRow.get(28),
				weatherRow.get(29), weatherRow.get(30), weatherRow.get(31), weatherRow.get(32), weatherRow.get(33),
				weatherRow.get(34), weatherRow.get(34), weatherRow.get(36), weatherRow.get(37), weatherRow.get(38),
				weatherRow.get(39), weatherRow.get(40), weatherRow.get(41), weatherRow.get(42), weatherRow.get(43),
				weatherRow.get(44), weatherRow.get(45), weatherRow.get(46), weatherRow.get(47), weatherRow.get(48));
	});
   
   
   

	// Apply the schema to the RDD.
	DataFrame weatherFrame = sqlContext.createDataFrame(rowRDD, schema);
	weatherFrame.registerTempTable("weather");
   
   
   III. another example
   
   JavaRDD<String> data = sc.textFile(input);
   
   JavaRDD<Row> rowData = data.map(new Function<String, String[]>() {
		@Override
		public String[] call(String line) throws Exception {
			return line.split("\t");
		}
	}).map(new Function<String[], Row>() {
		@Override
		public Row call(String[] r) throws Exception {
			return RowFactory.create(Integer.parseInt(r[0]), r[1], r[2], r[3],
					new Timestamp(DatatypeConverter.parseDateTime(r[4]).getTimeInMillis()));
		}
	});

	dataset = sqc.createDataFrame(rowData, schema);
   
   
*/   
   
}

/////////////

}
