/**
 * Illustrates a simple map in Java
 */
package mypackage;


import org.apache.spark.sql.SparkSession;

public class MainDcmv1 {
    final String TSV_SEGMENT_FEED = "src/main/resources/segment_feed_2017_01_29_23.tsv";
    final String TSV_STANDARD_FEED = "src/main/resources/standard_feed_2017_01_30_11.tsv";

    public static void main(String[] args) throws Exception {
        String master;
        if (args.length > 0) {
            master = args[0];
        } else {
            master = "local";
        }

        // this is SparkContext
        /*
        ***** Spark 1.6
           SparkConf sparkConf = new SparkConf();
           JavaSparkContext sc = new JavaSparkContext(sparkConf);
           SQLContext sqlContext = new SQLContext(sc);
           DataFrame df = sqlContext.read().json("data.json");
           DataFrame tables = sqlContext.tables();
        ***** Spark 2.x
           SparkSession spark = SparkSession.builder().getOrCreate();
           Dataset<Row> df = spark.read().json("data.json");
           Dataset<Row> tables = spark.catalog().listTables();
        
        */
        /*
        The SparkSession class is a new feature of Spark 2.0 which streamlines the number of configuration and helper 
        classes you need to instantiate before writing Spark applications. SparkSession provides a single entry point 
        to perform many operations that were previously scattered across multiple classes, and also provides accessor 
        methods to these older classes for maximum compatibility.

        In interactive environments, such as the Spark Shell or interactive notebooks, a SparkSession will already be 
        created for you in a variable named spark. For consistency, you should use this name when you create one in your 
        own application. You can create a new SparkSession through a Builder pattern which uses a "fluent interface" style 
        of coding to build a new object by chaining methods together. Spark properties can be passed in, as shown in 
        these examples:
        
        
        
         */
           
     SparkSession spark = SparkSession.builder()
    .master("local[*]")
    .config("spark.driver.cores", 1)
    .appName("JUnderstandingSparkSession")
    .getOrCreate();
         
        
        /*
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL user-defined DataFrames aggregation example")
                .getOrCreate();
        */


        String f1 = "/Users/robertspolis/Downloads/spark/NetworkActivity_2016-01-01-00.csv";
        String f2 = "/Users/robertspolis/Downloads/spark/NetworkClick_2015-07-31-00.csv";
        String f3 = "/Users/robertspolis/Downloads/spark/NetworkImpression_2015-07-31-00.csv";

        String g1 = "/Users/robertspolis/Downloads/spark/orig/NetworkClick_177203_01-01-2016.log.gz";
        String g2 = "/Users/robertspolis/Downloads/spark/orig/NetworkImpression_177203_01-01-2016.log.gz";


        DCMv1printSchema ps = new DCMv1printSchema();
        ps.readAciiAndPrintSchema(f1,spark);
        ps.readAciiAndPrintSchema(f2,spark);
        ps.readAciiAndPrintSchema(f3,spark);
        ps.readAciiAndPrintSchema(g1,spark);
        ps.readAciiAndPrintSchema(g2,spark);

        
        
       /*
        https://sparkour.urizone.net/recipes/understanding-sparksession/
        // Spark 1.6

        SparkConf sparkConf = new SparkConf();
JavaSparkContext sc = new JavaSparkContext(sparkConf);
HiveContext hiveContext = new HiveContext(sc);
DataFrame df = hiveContext.sql("SELECT * FROM hiveTable");
             
// Spark 2.x
SparkSession spark = SparkSession.builder().enableHiveSupport().getOrCreate();
Dataset<Row> df = spark.sql("SELECT * FROM hiveTable");
        
        
        
     */   
    /*
        
    At the end of your application, calling stop() on the SparkSession will implicitly stop any nested Context classes.
        
        
    */
        
    spark.stop();

    }
}
