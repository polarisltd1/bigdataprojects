/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

/**
 *
 * @author robertspolis
 */



import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;


public class SegmentFeedSchema {

    /*
    
    datetime
timestamp
The date and time when the segment pixel fired in YYYY-MM-DD HH:MM:SS (UTC).
2	
user_id_64
bigint
The AppNexus 64-bit user ID stored in the AppNexus cookie store. This field will be 0 when AppNexus does not have a match for this user or the user's browser doesn't accept cookies. It will be -1 for opt-out users.
3	
member_id
int
The ID of the member that dropped the pixel.
4	
segment_id
int
The ID of the segment pixel.
5	
is_daily_unique
tinyint
Whether or not the pixel fire is the first for a given user that day.
6	
is_monthly_unique
tinyint
Whether or not the pixel fire is the first for a user in the past 30 days.
7	
value
int
Optional value passed with pixel.
    
    
    */
    public static final StructType schema = new StructType(new StructField[] {
    new StructField("datetime", DataTypes.TimestampType, true, Metadata.empty()), //1
    new StructField("user_id_64", DataTypes.LongType, true, Metadata.empty()), //2    
    new StructField("member_id", DataTypes.IntegerType, true, Metadata.empty()), //3        
    new StructField("segment_id", DataTypes.IntegerType, true, Metadata.empty()), //4        
    new StructField("is_daily_unique", DataTypes.IntegerType, true, Metadata.empty()), //5    
    new StructField("is_monthly_unique", DataTypes.IntegerType, true, Metadata.empty()), //6
    new StructField("value", DataTypes.StringType, true, Metadata.empty()) //7
});
    
 
    
public static String[] colnames = new String[]{

    
    "datetime",
    "user_id_64", 
    "member_id",       
    "segment_id",      
    "is_daily_unique",   
    "is_monthly_unique", 
    "value" 




};


}    
    
    
    
    
    

