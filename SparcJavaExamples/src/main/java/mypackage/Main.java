/**
 * Illustrates a simple map in Java
 */
package mypackage;


import org.apache.spark.sql.SparkSession;

public class Main {
    final String TSV_SEGMENT_FEED = "src/main/resources/segment_feed_2017_01_29_23.tsv";
    final String TSV_STANDARD_FEED = "src/main/resources/standard_feed_2017_01_30_11.tsv";

    public static void main(String[] args) throws Exception {
        String master;
        if (args.length > 0) {
            master = args[0];
        } else {
            master = "local";
        }

        // this is SparkContext
        /*
        ***** Spark 1.6
           SparkConf sparkConf = new SparkConf();
           JavaSparkContext sc = new JavaSparkContext(sparkConf);
           SQLContext sqlContext = new SQLContext(sc);
           DataFrame df = sqlContext.read().json("data.json");
           DataFrame tables = sqlContext.tables();
        ***** Spark 2.x
           SparkSession spark = SparkSession.builder().getOrCreate();
           Dataset<Row> df = spark.read().json("data.json");
           Dataset<Row> tables = spark.catalog().listTables();
        
        */
        /*
        The SparkSession class is a new feature of Spark 2.0 which streamlines the number of configuration and helper 
        classes you need to instantiate before writing Spark applications. SparkSession provides a single entry point 
        to perform many operations that were previously scattered across multiple classes, and also provides accessor 
        methods to these older classes for maximum compatibility.

        In interactive environments, such as the Spark Shell or interactive notebooks, a SparkSession will already be 
        created for you in a variable named spark. For consistency, you should use this name when you create one in your 
        own application. You can create a new SparkSession through a Builder pattern which uses a "fluent interface" style 
        of coding to build a new object by chaining methods together. Spark properties can be passed in, as shown in 
        these examples:
        
        
        
         */
           
     SparkSession spark = SparkSession.builder()
    .master("local[*]")
    .config("spark.driver.cores", 1)
    .appName("JUnderstandingSparkSession")
    .getOrCreate();
         
        
        /*
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL user-defined DataFrames aggregation example")
                .getOrCreate();
        */

        DCMv1printSchema ps = new DCMv1printSchema();
        ps.manualParse(spark);
        
        
        Test2 t2 = new Test2();
        
        try{
           t2.readJsonAndWriteParquet(spark);
        }catch(Exception e){
           e.printStackTrace();
        }
        
        try{
           t2.readCsvAndWriteSchemaParquet(spark);
        }catch(Exception e){
           e.printStackTrace();
        }
        
       Test1 t1= new Test1(); 
        
        try{
               t1.javabeanDS(spark);
               t1.programaticSchema(spark);
        }catch(Exception e){
               e.printStackTrace();
        }
        
        Test3 t3= new Test3(); 
        
        try{
               t3.testAppnexusStandardfeedSchema_1(spark);
        }catch(Exception e){
               e.printStackTrace();
        }
        
        try{
               t3.testAppnexusSegmentfeedSchema_2(spark);
        }catch(Exception e){
               e.printStackTrace();
        }
        
        
        
        
       /*
        https://sparkour.urizone.net/recipes/understanding-sparksession/
        // Spark 1.6

        SparkConf sparkConf = new SparkConf();
JavaSparkContext sc = new JavaSparkContext(sparkConf);
HiveContext hiveContext = new HiveContext(sc);
DataFrame df = hiveContext.sql("SELECT * FROM hiveTable");
             
// Spark 2.x
SparkSession spark = SparkSession.builder().enableHiveSupport().getOrCreate();
Dataset<Row> df = spark.sql("SELECT * FROM hiveTable");
        
        
        
     */   
    /*
        
    At the end of your application, calling stop() on the SparkSession will implicitly stop any nested Context classes.
        
        
    */
        
    spark.stop();

    }
}
