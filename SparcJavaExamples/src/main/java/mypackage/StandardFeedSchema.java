/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

/**
 *
 * @author robertspolis
 */



import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;


public class StandardFeedSchema {
/* 
    
    
*/ 
/*
01	auction_id_64 bigint     The AppNexus unique auction identifier.
02	datetime      timestamp  The time and date of impression in YYYY-MM-DD HH:MM:SS (UTC).
03	user_tz_offset tinyint   The time zone of user vs. UTC. Useful for daypart targeting.
04	width          smallint  The width of the creative.
05      height        smallint    The height of the creative.
06	  media_type   tinyint   The general display style of the creative, classified by media subtype. To map media subtype IDs to their names and parent media types, use the Media Subtype Service.
07	  fold_position   tinyint   The placement position relative to fold. Possible values: 

    0 = Unknown 
1 = Above 
2 = Below 

Possible values for Facebook impressions: 
11 = "Top FB Ad Slot" 
12 = "2nd FB Ad Slot" 
13 = "3rd FB Ad Slot" 
14 = "4th FB Ad Slot" 
15 = "5th FB Ad Slot" 
16 = "6th FB Ad Slot" 
17 = "7th FB Ad Slot" 
18 = "8th FB Ad Slot" 
19 = "9th FB Ad Slot" 
20 = "10th FB Ad Slot"

08	  event_type (impression) enum

    Possible values:
imp = impression 
click
pc_conv = Post-click conversion
pv_conv = Post-view conversion

    
09	  imp_type  tinyint
The type of impression. Possible values: 
1 = Blank: "200 OK" response (blank) served.
2 = PSA: A public service announcement served because no other creative was eligible.
3 = Default Error: A default creative served due to timeout issue.
4 = Default: A default creative served because no campaigns bid or no other creative was eligible.
5 = Kept: One of your managed advertisers served a creative.
6 = Resold: The impression was sold to a third-party buyer.
7 = RTB: Your creative served on third-party inventory.
8 = PSA Error: A public service announcement served due to timeout issue.
9 = External Impression: An impression from an impression tracker.
10 = External Click: A click from a click tracker.
11 = Insertion: Your creative served on third-party inventory where it persists across page-loads and sessions. This impression type is currently only for Facebook News Feed creatives.
Both  
10	
payment_type
tinyint
The method by which the buyer is paying for the impression. Possible values: 
-1 = No payment 
0 = CPM 
1 = CPC 
2 = CPA 
3 = Owner CPM 
4 = Owner revshare
Both
11	
media_cost_dollars_cpm
numeric(18,6)
For buyers, the dollar amount * 1000 that a network is paying for an impression. For sellers, the dollar amount * 1000 paid to publisher.
Both
12	
revenue_type
tinyint
The method by which the buyer network gets paid by the advertiser, or how the method by which the seller network was paid by the buyer network. Possible values: 
-1 = No payment 
0 = Flat CPM 
1 = Cost Plus CPM 
2 = Cost Plus Margin 
3 = CPC 
4 = CPA 
5 = Revshare 
6 = Flat Fee 
7 = Variable CPM (vcpm) 
8 = Estimated CPM (est_cpm)
Both
13	
buyer_spend
numeric(18,6)
The gross CPM amount paid by the buyer (includes all deductions and price reductions).
Buyer
14	
buyer-bid
numeric(18,6)
This column returns different values for buyers and for sellers measured in CPM.
For buyers, this is the raw value submitted to the auction prior to subtraction of fees, commissions, etc. It will be greater than or equal to the total amount paid by the buyer (buyer_spend).
For sellers, this value is the bid that was evaluated during the auction, after subtraction of fees, commissions, etc.
For managed transactions (imp_type = 5), buyer_bid will equal buyer_spend because bid and price reductions are not applied.
Both
15	
ecp
numeric(18,6)
The estimated clear price for placement tag, in dollars unless another advertiser currency is specified.
Both
16	
eap
numeric(18,6)
The estimated average price for placement tag, in dollars unless another advertiser currency is specified.
Both
17	
is_imp
int
Possible values: 
0 = False 
1 = True
Both
18	
is_learn
tinyint
Possible values: 
0 = Base Bid 
1 = Learn 
2 = Optimized
Both
19	
predict_type_rev
tinyint
The optimization phase of the bid. Possible values: 
-2 = No predict phase 
-1 = Base predict phase 
0 = Learn giveup 
1 = Learn 
2 = Throttled 
3 = Optimized 
4 = Biased 
5 = Optimized 1 
8 = Optimized giveup 
9 = Base bid below giveup
Both
20	
user_id_64
bigint
The AppNexus 64-bit User ID stored in the AppNexus cookie store. This field is 0 when AppNexus does not have a match for this user or the user's browser doesn't accept cookies. It will be -1 for opt-out users.
Both    
*/    
    public static final StructType schema = new StructType(new StructField[] {
    new StructField("auction_id_64", DataTypes.LongType, true, Metadata.empty()),//1
    new StructField("datetime", DataTypes.TimestampType, true, Metadata.empty()), //2
    new StructField("user_tz_offset", DataTypes.IntegerType, true, Metadata.empty()), //3 
    new StructField("width", DataTypes.IntegerType, true, Metadata.empty()), //4
    new StructField("height", DataTypes.IntegerType, true, Metadata.empty()), //5
    new StructField("media_type", DataTypes.IntegerType, true, Metadata.empty()), //6
    new StructField("fold_position", DataTypes.IntegerType, true, Metadata.empty()), //7
    new StructField("event_type", DataTypes.StringType, true, Metadata.empty()), //8
    new StructField("imp_type", DataTypes.LongType, true, Metadata.empty()), //9
    new StructField("payment_type", DataTypes.LongType, true, Metadata.empty()), //10
    new StructField("media_cost_dollars_cpm", DataTypes.DoubleType, true, Metadata.empty()), //11
    new StructField("revenue_type", DataTypes.LongType, true, Metadata.empty()), //12
    new StructField("buyer_spend", DataTypes.DoubleType, true, Metadata.empty()), //13
    new StructField("buyer-bid", DataTypes.DoubleType, true, Metadata.empty()), //14
    new StructField("ecp", DataTypes.LongType, true, Metadata.empty()), //15
    new StructField("eap", DataTypes.LongType, true, Metadata.empty()), //16
    new StructField("is_imp", DataTypes.LongType, true, Metadata.empty()), //17
    new StructField("is_learn", DataTypes.IntegerType, true, Metadata.empty()), //18
    new StructField("predict_type_rev", DataTypes.IntegerType, true, Metadata.empty()), //19
    new StructField("user_id_64", DataTypes.LongType, true, Metadata.empty()), //20
 /*



21	
ip_address
string(40)
The IP address of user.
Both
22	
ip_address_trunc
string(40)
The IP address of user with the last octet removed.
Both
23	
geo_country
string(2)
The geographic country of the impression passed in on the ad call. Uses ISO 3166 country codes.
Both
24	
geo_region
string(2)
The geographic region of the impression passed in on the ad call. Uses ISO-3166-2 codes for US and Canadian regions, and FIPS codes for regions in all other countries.
Both
25	
operating_system
tinyint
The ID of operating system version. To map operating system version IDs to names, use the Operating System Extended Service.
Both
26	
browser
tinyint
The ID of the browser. To map browser IDs to names, use the Browser Service.
Both
27	
language
tinyint
The ID of the language. To map language IDs to names, use the Language Service.
Both
28	
venue_id
int
The venue is a combination of domain, site, tag, and user country which AppNexus' optimization system uses to determine bid valuations.
Both
29	
seller_member_id
int
The member ID of the seller.
Seller
30	
publisher_id
int
The ID for the publisher of the placement. Buyer member may map the Publisher ID to a name using the Inventory Resold Service if the seller has chosen to expose the publisher. Exposure to buyer depends on the seller's visibility profile.
Both
31	
site_id
int
The ID for site associated with placement. Cannot be associated with a name, except by the seller member who owns the site. Exposure to buyer depends on the seller's visibility profile.
Seller
32	
site_domain
string(100)
The most granular identifier of the site that we can offer (This may be the domain of a supply partner, ex: doubleclick.com; network who owns resold inventory; or it may just say resold inventory). Exposure to buyer depends on the seller's visibility profile.
Both
33	
tag_id
int
The ID of placement tag of the impression. Cannot be associated with a name, except by the seller member who owns the tag. Exposure to buyer depends on the seller's visibility profile.
Both
34	
external_inv_id
int
An optional code passed in the query string to further break out inventory. For more details, see External Inventory Code Service.
Seller
35	
reserve_price
numeric(18,6)
The reserve price for the placement, if any.
Seller
36	
seller_revenue_cpm
numeric(18,6)
The amount the seller is paid for the impression. When revenue_type is 3 (CPC) or 4 (CPA), revenue will appear in this field on the corresponding click or conversion row (as determined by event_type). The revenue value will still be in CPM, so to calculate the revenue for the specific click/conversion event, divide by 1000.
This field is also calculated for imp_type = 5 (managed impressions). If you wish to calculate total revenue from resold impressions, sum seller_revenue_cpm only for imp_type = 6.
Seller
37	
media_buy_rev_share_pct
numeric(18,6)
The publisher revenue share or the percent of booked revenue the seller shares with its publisher. This is a fraction between 0 and 1. If you multiply by 100 you get the percentage value.
Seller
38	
pub_rule_id
int
The ID for the publisher rule, which is set up in the UI and defines how the publisher is paid.
Seller
39	
seller_currency
string(3)
The currency used by the seller.
Seller
40	
publisher_currency
string(3)
The currency in which publisher is paid.
Seller
    */
    
    new StructField("ip_address", DataTypes.StringType, true, Metadata.empty()), //21
    new StructField("ip_address_trunc", DataTypes.StringType, true, Metadata.empty()), //22    
    new StructField("geo_country", DataTypes.StringType, true, Metadata.empty()), //23
    new StructField("geo_region", DataTypes.StringType, true, Metadata.empty()), //24    
    new StructField("operating_system", DataTypes.IntegerType, true, Metadata.empty()), //25
    new StructField("browser", DataTypes.IntegerType, true, Metadata.empty()), // 26   
    new StructField("language", DataTypes.IntegerType, true, Metadata.empty()), //
    new StructField("venue_id", DataTypes.IntegerType, true, Metadata.empty()), // 28   
    new StructField("seller_member_id", DataTypes.IntegerType, true, Metadata.empty()), //29
    new StructField("publisher_id", DataTypes.IntegerType, true, Metadata.empty()), //30    
    new StructField("site_id", DataTypes.IntegerType, true, Metadata.empty()), //31
    new StructField("site_domain", DataTypes.StringType, true, Metadata.empty()), //32    
    new StructField("tag_id", DataTypes.IntegerType, true, Metadata.empty()), //33
    new StructField("external_inv_id", DataTypes.IntegerType, true, Metadata.empty()), //34    
    new StructField("reserve_price", DataTypes.IntegerType, true, Metadata.empty()), //35
    new StructField("seller_revenue_cpm", DataTypes.DoubleType, true, Metadata.empty()), //36    
    new StructField("media_buy_rev_share_pct", DataTypes.DoubleType, true, Metadata.empty()), //37
    new StructField("pub_rule_id", DataTypes.IntegerType, true, Metadata.empty()), //38    
    new StructField("seller_currency", DataTypes.StringType, true, Metadata.empty()), //39
    new StructField("publisher_currency", DataTypes.StringType, true, Metadata.empty()), //40    
    
    
    /*
41	
publisher_exchange_rate
numeric(18,6)
The exchange rate (publisher currency: USD) on the day of the transaction.
Seller
42	
serving_fees_cpm
numeric(18,6)
The serving fees in addition to media costs to when serving an ad.
Buyer
43	
serving_fees_revshare
numeric(18,6)
The revenue share in addition to media costs when serving an ad.
Buyer
44	
buyer_member_id
int
The member ID of the buyer. Possible values:
1 = Blank. The buyer_member_id and creative_id is 0.
2 = PSA. The AppNexus PSA member_id displays.
3 or 4 = Default Error or Default. The seller_member_id displays.
Buyer
45	
advertiser_id
int
The ID of the advertiser.
Buyer
46	
brand_id
int
The ID for brand of the creative.
Both
47	
advertiser_frequency
int
The advertiser-level frequency (times they have seen an ad by this advertiser).
Buyer
48	
advertiser_recency
int
The advertiser-level recency in minutes (how long it has been since the user saw an ad from this advertiser).
Buyer
49	
insertion_order_id
int
The ID for insertion order if used.
Buyer
50	
campaign_group_id
int
The identifier of line item.
Buyer
51	
campaign_id
int
The ID of the campaign.
Buyer
52	
creative_id
int
The ID of the creative served.
Both
53	
creative_freq
int
The creative-level frequency (times the user has seen this creative by this advertiser).
Buyer
54	
creative_rec
int
The creative-level recency in minutes (how long it has been since the user has seen this creative ad).
Buyer
55	
cadence_modifier
numeric(18,6)
The cadence modifier for the impression, which is the number by which the bid was multiplied.
Buyer
56	
can_convert
tinyint
If impression has conversion pixels associated or not.
Buyer
57	
user_group_id
int
Used if advertiser separates users into groups for purposes of A/B testing
Buyer
58	
is_control
tinyint
Whether the creative served is a control creative.
0 = test impression
1 = control impression
2 = no cookie user. (AppNexus does not include no cookie users to avoid skewing the size of test and control groups).
Buyer
59	
controller_pct
numeric(18,6)
The percentage of impressions that are control. This is set by the network.
Buyer
60	
controller_creative_pct
int
The ID of the control creative, if a control creative is served.
Buyer
*/    
new StructField("publisher_exchange_rate", DataTypes.DoubleType, true, Metadata.empty()), //  41  
new StructField("serving_fees_cpm", DataTypes.DoubleType, true, Metadata.empty()), //    
new StructField("serving_fees_revshare", DataTypes.DoubleType, true, Metadata.empty()), //    
new StructField("buyer_member_id", DataTypes.IntegerType, true, Metadata.empty()), //  44  
new StructField("advertiser_id", DataTypes.IntegerType, true, Metadata.empty()), //45    
new StructField("brand_id", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("advertiser_frequency", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("advertiser_recency", DataTypes.IntegerType, true, Metadata.empty()), // 48   
new StructField("insertion_order_id", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("campaign_group_id", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("campaign_id", DataTypes.IntegerType, true, Metadata.empty()), //51    
new StructField("creative_id", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("creative_freq", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("creative_rec", DataTypes.IntegerType, true, Metadata.empty()), //54    
new StructField("cadence_modifier", DataTypes.DoubleType, true, Metadata.empty()), //    
new StructField("can_convert", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("user_group_id", DataTypes.IntegerType, true, Metadata.empty()), //  57  
new StructField("is_control", DataTypes.IntegerType, true, Metadata.empty()), //    
new StructField("controller_pct", DataTypes.DoubleType, true, Metadata.empty()), // 59   
new StructField("controller_creative_pct", DataTypes.DoubleType, true, Metadata.empty()), // 60   
    
/*    
61	
is_click
int
Indicates if the creative was clicked. Possible values:
0 = False
1 = True
NULL = No information available
Buyer
62	
pixel_id
int
The ID of conversion pixel.
Buyer
63	
is_remarketing
tinyint
Used to indicate that a new segment pixel is remarketing or not.
Buyer
64	
post_click_conv
int
Used to identify a row that is a post click conversion. This has a maximum value of 1.
Buyer
65	
post_view_conv
int
Used to identify a row that is a post view conversion. This has a maximum value of 1.
Buyer
66	
post_click_revenue
numeric(18,6)
The advertiser post click revenue.
Buyer
67	
post_view_revenue
numeric(18,6)
The advertiser post view revenue.
Buyer
68	order_id	
string(36)
An optional value passed in by buyer on conversion pixel using the order_id parameter. For more details, see Conversion Pixels Advanced (Customer login required).
Buyer
69	
external_data
string(30)
An optional extra data passed in by buyer on conversion pixel using the other parameter. For more details, see Conversion Pixels Advanced (Customer login required).
Buyer
70	
pricing_type
string(3)
The line item pricing type, or how the buyer network gets paid by the advertiser (e.g. cpm, cpc).
Buyer
71	
booked_revenue_dollars
numeric(18,6)
The dollar amount earned by network on impression.
Buyer
72	
booked_revenue_adv_curr
numeric(18,6)
The same amount as booked revenue dollars, but displayed in a different currency. For example, if your advertiser pays in Euros you can see this here.
Buyer
73	
commission_cpm
numeric(18,6)
If commission is used in the UI or API.
Buyer
74	
commission_revshare
numeric(18,6)
If commission is used in the UI or API.
Buyer
75	
auction_service_deduction
numeric(18,6)
The dollar amount deducted from the bid to pay for auction hosting.
Buyer
76	
auction_service_fees
numeric(18,6)
The dollar amount charged charged for purchasing inventory from a publisher that AppNexus does not have a revshare agreement with.
Buyer
77	
creative_overage_fees
numeric(18,6)
The dollar amount charged if the creative served is over our size (bandwith) limit, dollar amount.
Buyer
78	clear_fees	
numeric(18,6)
The dollar amount charged for facilitating the auction for the buyer.
Buyer
79	
buyer_currency
string(3)
The currency used by the buyer.
Buyer
80	
advertiser_currency
string
The currency used by advertiser.
Buyer
*/
new StructField("is_click", DataTypes.IntegerType, true, Metadata.empty()), // 61
new StructField("pixel_id", DataTypes.IntegerType, true, Metadata.empty()), // 62
new StructField("is_remarketing", DataTypes.IntegerType, true, Metadata.empty()), // 63
new StructField("post_click_conv", DataTypes.IntegerType, true, Metadata.empty()), // 64
new StructField("post_view_conv", DataTypes.IntegerType, true, Metadata.empty()), // 65
new StructField("post_click_revenue", DataTypes.DoubleType, true, Metadata.empty()), // 66
new StructField("post_view_revenue", DataTypes.DoubleType, true, Metadata.empty()), // 67
new StructField("order_id", DataTypes.StringType, true, Metadata.empty()), // 68
new StructField("external_data", DataTypes.StringType, true, Metadata.empty()), // 69
new StructField("pricing_type", DataTypes.StringType, true, Metadata.empty()), // 70
new StructField("booked_revenue_dollars", DataTypes.DoubleType, true, Metadata.empty()), // 71
new StructField("booked_revenue_adv_curr", DataTypes.DoubleType, true, Metadata.empty()), // 72
new StructField("commission_cpm", DataTypes.DoubleType, true, Metadata.empty()), // 73
new StructField("commission_revshare", DataTypes.DoubleType, true, Metadata.empty()), // 74
new StructField("auction_service_deduction", DataTypes.DoubleType, true, Metadata.empty()), // 75
new StructField("auction_service_fees", DataTypes.DoubleType, true, Metadata.empty()), // 76
new StructField("creative_overage_fees", DataTypes.DoubleType, true, Metadata.empty()), // 77
new StructField("clear_fees", DataTypes.DoubleType, true, Metadata.empty()), // 78
new StructField("buyer_currency", DataTypes.StringType, true, Metadata.empty()), // 79
new StructField("advertiser_currency", DataTypes.StringType, true, Metadata.empty()), // 80

/*
81	
advertiser_exchange_rate
numeric(18,6)
The exchange rate on day of event. Rate is updated daily at 4 PM GMT.
Buyer
82	
latitude
string
The latitude of the user's location, when GPS data is available from a mobile device. Expressed in the format "snn.ddd,snn.ddd", for example +12.345 or -45.123, where south is represented as negative. In order to comply with privacy standards, there can be a maximum of 3 decimal places of precision.
Both
83	longitude	
string
The longitude of the user's location, when GPS data is available from a mobile device. Expressed in the format "snn.ddd,snn.ddd", for example +12.345 or -45.123, where west is represented as negative. In order to comply with privacy standards, there can be a maximum of 3 decimal places of precision.
Both
84	device_unique_id	
string
The unique identifier representing the mobile device. The numeric prefix indicates the type of unique device identifier: 
0 = IDFA (Apple ID for Advertising)
1 = SHA1 
2 = MD5 
3 = ODIN 
4 = OPENUDID
5 = AAID (Android Advertising ID)
Both
85	device_id	int	
The model ID of the mobile device. To map model IDs to names, use the Device Model Service.
Both
86	carrier_id	int	
The ID of the carrier associated with the mobile device. Top map carrier IDs to names, use the Carrier Service
Both
87	deal_id	int	The Deal ID associated with this impression.	Both
88	
view_result
enum
The AppNexus viewability measurement result of the impression. Possible values: 
1 = VIEW_MEASURED_VIEWABLE 
2 = VIEW_MEASURED_NON_VIEWABLE 
3 = VIEW_NON_MEASURED
For more details about viewability, see Introduction to Viewability (Customer login required).
Both
89	application_id	string	The ID of the mobile application (if applicable).	Seller: Always
Buyer: If allowed by seller's visibility profile
90	supply_type	enum	
The type of supply. Possible values: 
0 = WEB
1 = MOBILE_WEB 
2 = MOBILE_APP
3 = FACEBOOK_SIDEBAR
5 = TOOLBAR
Both
91	sdk_version	string	The version of the SDK (e.g., "3.3.0", "sdkandroid_4-0-9", "soma_ios_602").	Both
92	ozone_id	int	The ID of the optimization zone.	Seller
93	billing_period_id	int	
The ID of the billing period under which the impression was served.
Buyer
94	view_non_measurable_reason	int	
The reason an impression could not be measured for viewability. Possible values:
0 = N/A. The impression was measured for viewability
1 = SCRIPT_NOT_SERVED. The viewability script was not served with the creative. For example, on mobile-app inventory.
2 = NO_SCRIPT_CALLBACK. The viewability script was served with the creative, but no callback/event was received. For example, the user left the page before the creative was served.
3 = TECHNICAL_LIMITATION. The viewability script was served and loaded, but was unable to measure for a technical reason. For example, a cross-domain iframe with Flash disabled.
For more information regarding viewability, see Introduction to Viewability (Customer login required).
Both
95	external_uid	string(100)	Specifies the external auction ID for this impression, passed in on the ad call by the seller.	Seller
96	request_uuid	string(36)	
Specifies a unique identifier for the request. For single tag requests (such as /ttj), this is the same as auction_id_64. For multitag requests (such as with AST or some OpenRTB integrations), represents all of the auction_id_64 values.
Both
97	geo_dma	int	Specifies the ID of the demographic area for this impression. Use the Demographic Area Service to look up the area associated with the ID.	Both
98	geo_city	int	The ID of the city for this impression. Use the City Service to look up the city associated with the ID.	Both
99	mobile_app_instance_id	int	Specifies the ID of the mobile app instance for this impression. Use the Mobile App Instance Service to look up the mobile app instance associated with the ID.	Both
100	traffic_source_code	string(100)	
Specifies the external source of the third party traffic for this impression.
Seller
*/
new StructField("advertiser_exchange_rate", DataTypes.DoubleType, true, Metadata.empty()), //81
new StructField("latitude", DataTypes.StringType, true, Metadata.empty()), //
new StructField("longitude", DataTypes.StringType, true, Metadata.empty()), //
new StructField("device_unique_id", DataTypes.StringType, true, Metadata.empty()), //84
new StructField("device_id", DataTypes.IntegerType, true, Metadata.empty()), //
new StructField("carrier_id", DataTypes.IntegerType, true, Metadata.empty()), //
new StructField("deal_id", DataTypes.IntegerType, true, Metadata.empty()), //87
new StructField("view_result", DataTypes.StringType, true, Metadata.empty()), //
new StructField("application_id", DataTypes.StringType, true, Metadata.empty()), //
new StructField("supply_type", DataTypes.IntegerType, true, Metadata.empty()), //90
new StructField("sdk_version", DataTypes.StringType, true, Metadata.empty()), //
new StructField("ozone_id", DataTypes.IntegerType, true, Metadata.empty()), //92
new StructField("billing_period_id", DataTypes.IntegerType, true, Metadata.empty()), //
new StructField("view_non_measurable_reason", DataTypes.IntegerType, true, Metadata.empty()), //
new StructField("external_uid", DataTypes.StringType, true, Metadata.empty()), //95
new StructField("request_uuid", DataTypes.StringType, true, Metadata.empty()), //
new StructField("geo_dma", DataTypes.IntegerType, true, Metadata.empty()), //
new StructField("geo_city", DataTypes.IntegerType, true, Metadata.empty()), //98
new StructField("mobile_app_instance_id", DataTypes.IntegerType, true, Metadata.empty()), //
new StructField("traffic_source_code", DataTypes.StringType, true, Metadata.empty()), //100

/*
101	external_request_id	string(100)	Specifies the seller's own version of request_uuid or auction_id_64 that ties the impression back to their own data feeds.	Seller
102	deal_type	int	
Specifies the type of deal:
1 = open auction
2 = private auction
Both
103	ym_floor_id	int	The ID of the yield management floor that was applied to the buyer. Use the Yield Management Floor Service to look up the floor associated with the ID.	Seller
104	ym_bias_id	int	The ID of the yield management bias that was applied to the buyer. Use the Yield Management Bias Service to look up the bias associated with the ID.	Seller
105	
is_filtered_request
int	
Specifies whether or not the event was filtered for Inventory Quality reasons
0 = event was not filtered
1 = event was filtered
Seller
106	
age
int	The age of the user, if known	Both
107	
gender
string(1)	The gender of the user, if known. Possible values are u, m, and f.	Both
108	
is_exclusive
int	Specifies which types of bids were eligible to win the auction
0 = Both managed and non-managed bids were eligible to win
1 = Only managed bids were eligible to win	Seller
109	bid_priority	int	Specifies the campaign or line item's priority from the bidder only when imp_type = 5 (managed impressions). For all other imp_type values, defaults to 0.	Seller
110	custom_model_id	int	The id of the custom model used in the auction. When no custom model is used, this defaults to 0.	Buyer
111	custom_model_last_modified	int	The date and time (in Unix Epoch time) since the custom model that was used in the auction was last modified. If no model was used, this defaults to 0.	Buyer
112	leaf_name	string	The leaf_name specified in the leaf that determined the winning bid. If no name is specified, or if a model was not used, this defaults to ---.	Buyer
113	data_costs_cpm	numeric(18,6)	Total data costs the buyer paid for the given impression	Buyer
*/

    new StructField("external_request_id", DataTypes.StringType, true, Metadata.empty()), //101
    new StructField("deal_type", DataTypes.IntegerType, true, Metadata.empty()), //102
    new StructField("ym_floor_id", DataTypes.IntegerType, true, Metadata.empty()), //103
    new StructField("ym_bias_id", DataTypes.IntegerType, true, Metadata.empty()), //104
    new StructField("is_filtered_request", DataTypes.IntegerType, true, Metadata.empty()), //105
    new StructField("age", DataTypes.IntegerType, true, Metadata.empty()), //106
    new StructField("gender", DataTypes.StringType, true, Metadata.empty()), //107
    new StructField("is_exclusive", DataTypes.IntegerType, true, Metadata.empty()), //108
    new StructField("bid_priority", DataTypes.IntegerType, true, Metadata.empty()), //109
    new StructField("custom_model_id", DataTypes.IntegerType, true, Metadata.empty()), //110
    new StructField("custom_model_last_modified", DataTypes.IntegerType, true, Metadata.empty()), //111
    new StructField("leaf_name", DataTypes.StringType, true, Metadata.empty()), //112
    new StructField("data_costs_cpm", DataTypes.DoubleType, true, Metadata.empty()) //113
    
    
    
    
    
    
    
});
    
 
    
public static String[] colnames = new String[]{

    "auction_id_64",
    "datetime", 
    "user_tz_offset", 
    "width", 
    "height", 
    "media_type", 
    "fold_position", 
    "event_type", 
    "imp_type", 
    "payment_type", 
    "media_cost_dollars_cpm", 
    "revenue_type", 
    "buyer_spend", 
    "buyer-bid", 
    "ecp", 
    "eap", 
    "is_imp", 
    "is_learn", 
    "predict_type_rev", 
    "user_id_64", 
   "ip_address", 
    "ip_address_trunc",    
    "geo_country", 
    "geo_region", 
    "operating_system", 
    "browser",  
    "language", 
    "venue_id",  
    "seller_member_id", 
    "publisher_id",     
    "site_id", 
    "site_domain",     
    "tag_id", 
    "external_inv_id",  
    "reserve_price", 
    "seller_revenue_cpm",  
    "media_buy_rev_share_pct", 
    "pub_rule_id", 
    "seller_currency", 
    "publisher_currency",   
"publisher_exchange_rate",  
"serving_fees_cpm",  
"serving_fees_revshare",    
"buyer_member_id", 
"advertiser_id",  
"brand_id",  
"advertiser_frequency",     
"advertiser_recency",   
"insertion_order_id",  
"campaign_group_id", 
"campaign_id",    
"creative_id",   
"creative_freq",   
"creative_rec",    
"cadence_modifier",  
"can_convert",   
"user_group_id",  
"is_control",  
"controller_pct",  
"controller_creative_pct",  
"is_click", 
"pixel_id", 
"is_remarketing", 
"post_click_conv", 
"post_view_conv", 
"post_click_revenue", 
"post_view_revenue", 
"order_id", 
"external_data", 
"pricing_type", 
"booked_revenue_dollars", 
"booked_revenue_adv_curr", 
"commission_cpm", 
"commission_revshare", 
"auction_service_deduction", 
"auction_service_fees", 
"creative_overage_fees", 
"clear_fees", 
"buyer_currency", 
"advertiser_currency", 
"advertiser_exchange_rate", 
"latitude", 
"longitude", 
"device_unique_id", 
"device_id", 
"carrier_id", 
"deal_id", 
"view_result", 
"application_id", 
"supply_type", 
"sdk_version", 
"ozone_id", 
"billing_period_id", 
"view_non_measurable_reason", 
"external_uid", 
"request_uuid", 
"geo_dma", 
"geo_city", 
"mobile_app_instance_id", 
"traffic_source_code", 
"external_request_id", 
    "deal_type", 
    "ym_floor_id", 
    "ym_bias_id", 
    "is_filtered_request", 
    "age", 
    "gender", 
    "is_exclusive", 
    "bid_priority", 
    "custom_model_id", 
    "custom_model_last_modified", 
    "leaf_name", 
    "data_costs_cpm"};


}    
    
    
    
    
    

