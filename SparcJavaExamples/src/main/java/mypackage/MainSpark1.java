/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;
/**
 *
 * @author robertspolis
 */
public class MainSpark1 {
  

  public static void main(String[] args) throws Exception{

      
      String S3_FILE = "s3://mae-data-feed-dev/input/DataProvider=APPNEXUS/DataType=segment_feed/Date=2017-01-30/Hour=2017-01-30-14/Version=0/Timestamp=20170130153814/Split=0/segment_feed_2017_01_30_14.tsv";
       
    // create Spark context with Spark configuration
    //JavaSparkContext sc = new JavaSparkContext(new SparkConf().setAppName("Sparking just starts here!"));
    /*
    
    ./bin/spark-submit \
  --class <main-class> \
  --master <master-url> \
  --deploy-mode <deploy-mode> \
  --conf <key>=<value> \
  ... # other options
  <application-jar> \
  [application-arguments]
    
    
    
    this is how we run our current clients project

[11:14]  
spark-submit \
          --master "yarn" \
          --driver-memory 8g \
          --deploy-mode client \
          --files=/path/to/metrics.properties
          --conf spark.metrics.conf=metrics.properties
          --conf "spark.driver.extraJavaOptions=-Dspring.profiles.active=emr -Dlog4j.configuration=file:`pwd`/log4j.properties  -Ddm.logging.level=INFO" \
          --class com.theexchangelab.datapipeline.Application \
          ./tel-data-pipelines-1.0.jar $*

[11:15]  
you can remove the following

[11:15]  
--files=/path/to/metrics.properties
         --conf spark.metrics.conf=metrics.properties
         --conf "spark.driver.extraJavaOptions=-Dspring.profiles.active=emr -Dlog4j.configuration=file:`pwd`/log4j.properties  -Ddm.logging.level=INFO" \



as this setups up metrics logging and parameters for their app

[11:16]  
spark-submit \
          --master "yarn" \
          --driver-memory 8g \
          --deploy-mode client \        
          --class com.theexchangelab.datapipeline.Application \
          ./tel-data-pipelines-1.0.jar $*
    
    
    
    
    */
    
    SparkSession spark = SparkSession.builder().getOrCreate();
    JavaSparkContext sc = new JavaSparkContext(spark.sparkContext());
    
    
    
    Test1 t1 = new Test1();
    Test3 t3 = new Test3();
    
    // next tries to read S3
    try{
        
        t3.s3read(spark,S3_FILE);
    
    
    }catch(Exception e){e.printStackTrace();}
    
    
    
    try{t1.test1(sc);}catch(Exception e){e.printStackTrace();}
    try{t1.test5(spark);}catch(Exception e){e.printStackTrace();}
    try{t1.javabeanDS(spark);}catch(Exception e){e.printStackTrace();}
    
    
    
  }

}

