package mypackage;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by robertspolis on 17/02/17.
 */
public class AppNexusSiphonLister {

    private static final Logger LOG = LogManager.getLogger(AppNexusSiphonLister.class);

    public static void main(String[] args)throws Exception{

          new AppNexusSiphonLister();



    }


    public AppNexusSiphonLister()throws Exception {

        siphonListFormattedFile();
        siphonListFindDuplicates();


    }

    public void siphonListFindDuplicates()throws Exception{


        StringBuffer out = new StringBuffer();


        Path paths = Paths.get("siphon2.json");

        System.out.println("reading: "+paths.toAbsolutePath());

        String content = new String(Files.readAllBytes(paths));

        List<String[]> kvFiles = populateKVpairs(content);  // this is list of elements String[fileId,timestamp]

        // find duplicates of fileId
        Set<String> fileIdDups = filterDups(kvFiles);

        LOG.info(fileIdDups);

        /*
         TODO next.
         for each fileIdDups find all kvParts and print them
         given kvParts map<String,String[]>  <fileId , array of fileTimestamp(s)
         print fileId,fileTimestamp
        */

        List<String[]> kvDups = new ArrayList<>();
        System.out.println("printing dups ...");
        fileIdDups.stream().forEach(dupId -> {
                    System.out.println(dupId);
        });




        List<String[]> kvOut = new ArrayList<>();

                fileIdDups.stream().forEach(
                dupId -> {
                    kvFiles.stream().filter(kvp -> kvp[0].equals(dupId)).forEach(kvOut::add);
        }


        );

        System.out.println("printing dups w ts ...");
        kvOut.stream().forEach(kvp->System.out.println(kvp[0]+"="+kvp[1]));

    }

    /**
     * Given list of strings return duplicates
     * @param source
     * @return
     */
    Set<String> filterDups(List<String[]>  source){

        Set<String> dupRemoved = new HashSet();
        Set<String> dups = source.stream().map(s->s[0]).filter(n -> !dupRemoved.add(n)).collect(Collectors.toSet());
        return dups;


    }



    /**
     * list syphons in following way:
     *
     *
     * NAME+HOUR+SPLIT,TIMESTAMP
     *
     *
     *
     */
    public void siphonListFormattedFile()throws Exception{


        StringBuffer out = new StringBuffer();


        Path paths = Paths.get("siphon2.json");

        System.out.println("reading: "+paths.toAbsolutePath());

        String content = new String(Files.readAllBytes(paths));


        List<String[]> kvFiles = populateKVpairs(content);




        kvFiles.stream().forEach(

                pair->  {out
                        .append(pair[0])
                        .append(",")
                        .append(pair[1])
                        .append("\n");
                }

        );


        Files.write(Paths.get("siphon2_list.txt"), out.toString().getBytes());

    }

    /**
     *
     */
    List<String[]> populateKVpairs(String content){

        JsonParser parser = new JsonParser();
        JsonElement json = parser.parse(content);



        JsonArray jsArr1 = json.getAsJsonObject().get("response").getAsJsonObject().get("siphons").getAsJsonArray();

        List<String> idParts = new ArrayList<>();

        List<String[]> kvParts = new ArrayList<>();



        // filling siphoneFormatted

        jsArr1.forEach(      s ->  {

                    JsonObject jo =  s.getAsJsonObject();

                    JsonArray jsarr2 = jo.get("splits").getAsJsonArray();

                    if(jsarr2.size()>0){

                        jsarr2.forEach(

                                t -> {

                                    String idPart = jo.get("name").getAsString()+"|"+jo.get("hour").getAsString()+"|"+t.getAsJsonObject().get("part").getAsString();
                                    String valuePart=jo.get("timestamp").getAsString();
                                    idParts.add(idPart);

                                    kvParts.add(new String[]{idPart,valuePart});






                                }
                        );
                    }
                }
        );



        return kvParts;

    }



}
