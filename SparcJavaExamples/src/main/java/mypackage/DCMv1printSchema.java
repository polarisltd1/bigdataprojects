/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author robertspolis
 */
public class DCMv1printSchema implements Serializable {

    final String AWS_ACCESS_KEY = "AKIAJRSHPA2ISUHXM2DA";
    final String AWS_SECRET_KEY = "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";

    final String TSV_SEGMENT_FEED = "src/main/resources/segment_feed_2017_01_29_23.tsv";
    final String TSV_STANDARD_FEED = "/Users/robertspolis/Downloads/spark/NetworkClick_2015-07-31-00.csv";

    public void readAciiAndPrintSchema(String filename, SparkSession spark) throws Exception {

        System.out.println(">>>>>>>>>>>>>>>>>>> "+filename+"  >>>>>>>>>>>>>>>>>>>>");


        String[] sepCharDCMv1 = {"þ","\376"};

        Dataset<Row> df = spark.read()
                .format("com.databricks.spark.csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .option("delimiter", sepCharDCMv1[0])
                .option("charset","ISO_8859_1")
                .load(filename);

        // about renaming columns
        // http://stackoverflow.com/questions/35592917/renaming-column-names-of-a-data-frame-in-spark-scala
        // df = df.toDF(StandardFeedSchema.colnames); // add renamed columns

        df.printSchema();

        df.limit(10).show();

        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");


    }

    public void manualParse(SparkSession spark) throws Exception {

        System.out.println(">>>>>>>>>>>>>>>>>>> NEXUS_STANDARD_FEED / MANUAL PROCESS >>>>>>>>>>>>>>>>>>>>");

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        JavaRDD<String> data = spark.sparkContext()
                .textFile(TSV_STANDARD_FEED, 1)
                .toJavaRDD();

        JavaRDD<Row> rowRDD = data.map(new Function<String, Row>() {
            @Override
            public Row call(String record) throws Exception {

                byte[] bytes = record.getBytes("ISO_8859_1");

                // 239	357	EF	11101111	ï	&#239;	&iuml;	Latin small letter i with diaeresis

                StringBuilder sb = new StringBuilder();

                for(int i=0;i<record.length();i++){

                    int b = bytes[i]& 0xFF;

                    if(b==63)sb.append(",");else sb.append(record.substring(i,i+1));

                    System.out.print(":"+b);
                }

                System.out.println("encoded string is: "+sb.toString());
                String[] splitRow = sb.toString().split(",");


                /* Rowfactory accepts array as well!
    List<MyData> mlist = new ArrayList<MyData>();
    mlist.add(d1);
    mlist.add(d2);
    Row row = RowFactory.create(mlist.toArray()); 
                 */

                // FUNNY THING, I NEED TO construct ROW by looking into schema so RowFactory.create(Object[])
                //System.out.println("*** row str:size,0,1,2,3, string:'" + record + "',split_size:" + a.length + ",split:[" + a[0] + "," + a[1] + "/" + Long.getLong(a[1]) + ":" + a[2] + "/" + Long.getLong(a[2]) + ":" + a[3] + ":" + Integer.getInteger(a[4]) + ":" + Integer.getInteger(a[5]) + ":" + a[6] + " string: " + a.toString());

//                Object[] row = new Object[StandardFeedSchema.schema.length()];
                
//                for(int i=0;i<StandardFeedSchema.schema.length();i++){
//                    StructField sf = StandardFeedSchema.schema.fields()[i];
//                    row[i] = encodeRow(sf,a[i]);
//                }
                
                
                
                /* next is flat assignement
                Row test = RowFactory.create(
                        Timestamp.valueOf(formatter.format(formatter.parse(a[0]))),
                        Long.valueOf(a[1]),
                        Integer.valueOf(a[2]),
                        Integer.valueOf(a[3]),
                        Integer.valueOf(a[4]),
                        Integer.valueOf(a[5]),
                        a[6]
                );
                 but this is dynamic.
*/


                System.out.println("returning row of this number of columns: " + splitRow.length);


                return RowFactory.create(splitRow);
            }
        });


        //Dataset<Row> df = spark.createDataset(rowRDD);


        //Dataset<Row> df = spark.createDataFrame(rowRDD, StandardFeedSchema.schema);

        //String[] colNames = new String[]{};

// Creates a temporary view using the DataFrame
        //df.createOrReplaceTempView("standard_feed");

        //df.printSchema();

        //df.limit(10).show();

        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

    }

    public void testDataBrickTSVWithSchema(SparkSession spark) throws Exception {

        StructType customSchema = new StructType(new StructField[]{
            new StructField("year", DataTypes.IntegerType, true, Metadata.empty()),
            new StructField("make", DataTypes.StringType, true, Metadata.empty()),
            new StructField("model", DataTypes.StringType, true, Metadata.empty()),
            new StructField("comment", DataTypes.StringType, true, Metadata.empty()),
            new StructField("blank", DataTypes.StringType, true, Metadata.empty())
        });
// example 2
        Dataset<Row> df2 = spark.read()
                .format("com.databricks.spark.csv")
                .schema(customSchema)
                .option("header", "true")
                .load("src/main/resources/cars.csv");

        df2.select("year", "model").write()
                .format("com.databricks.spark.csv")
                .option("header", "true")
                .option("delimiter", "\t")
                .save(getTmpName("newcars2") + ".tsv");

// compressed output
        Dataset<Row> df3 = spark.read()
                .format("com.databricks.spark.csv")
                .option("inferSchema", "true")
                .option("header", "true")
                .load("src/main/resources/cars.csv");

        df3.select("year", "model").write()
                .format("com.databricks.spark.csv")
                .option("header", "true")
                .option("delimiter", "\t")
                .option("codec", "org.apache.hadoop.io.compress.GzipCodec")
                .save(getTmpName("newcars3") + ".tsv");

    }

    public void s3read(SparkSession spark,String file) {
     System.out.println("******** S3 Read ***********");
//TSV files just add option TSV
        // bellow is included just to know its possible
        if(false)spark.sparkContext().hadoopConfiguration().set("fs.s3n.awsAccessKeyId", AWS_ACCESS_KEY);
        if(false)spark.sparkContext().hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", AWS_SECRET_KEY);

        Dataset<Row> df = spark.read()
                .format("com.databricks.spark.csv")
                .option("inferSchema", "true")
                .option("header", "false")
                .option("delimiter", "\t")
                .load(file);
     System.out.println("******** Done, good job! ***********");
     
    }

    String getTmpName(String prefix) throws Exception {
        return File.createTempFile(prefix, null, new File("/tmp")).getName();
    }

    Object encodeRow(StructField sf, String s) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            if (sf.dataType().equals(DataTypes.DoubleType)) {
                return Double.valueOf(s);
            } else if (sf.dataType().equals(DataTypes.TimestampType)) {
                return Timestamp.valueOf(formatter.format(formatter.parse(s)));
            } else if (sf.dataType().equals(DataTypes.IntegerType)) {
                return Integer.valueOf(s);
            } else if (sf.dataType().equals(DataTypes.DateType)) {
                return formatter.parse(s);
            } else if (sf.dataType().equals(DataTypes.FloatType)) {
                return Float.valueOf(s);
             } else if (sf.dataType().equals(DataTypes.StringType)) {
                return s.trim();
            } else if (sf.dataType().equals(DataTypes.ShortType)) {
                return Short.valueOf(s);  
            } else if (sf.dataType().equals(DataTypes.LongType)) {
                return Long.valueOf(s);    
            } else{
                System.out.println("Undefined type for column: "+sf.name());
                return null;
            }
        } catch (Exception ex) {
            System.out.println("Unable to parse column: "+sf.name()+" value:'"+s+"'");
            return null;
        }

        //return new Object(); // never fall here

    }

}
