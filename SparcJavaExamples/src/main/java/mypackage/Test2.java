/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mypackage;

import java.io.File;
import java.io.Serializable;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

/**
 * THIS IS MASTER EXAMPLE!!!!!
 * https://github.com/apache/spark/blob/master/examples/src/main/java/org/apache/spark/examples/sql/JavaSQLDataSourceExample.java
 */
public class Test2 implements Serializable{
 

 public void readAndWriteParquet(SparkSession spark)  throws Exception{
     
     
     
    // $example on:generic_load_save_functions$
    Dataset<Row> usersDF = spark.read().load("examples/src/main/resources/users.parquet");
    usersDF.select("name", "favorite_color").write().save("namesAndFavColors.parquet");
    // $example off:generic_load_save_functions$
    // $example on:manual_load_options$
    Dataset<Row> peopleDF =
      spark.read().format("json").load("examples/src/main/resources/people.json");
    
    
    
    
    
    peopleDF.select("name", "age").write().format("parquet").save("namesAndAges.parquet");
    // $example off:manual_load_options$
    // $example on:direct_sql$
    Dataset<Row> sqlDF =
      spark.sql("SELECT * FROM parquet.`examples/src/main/resources/users.parquet`");
    // $example off:direct_sql$
  }
    

public void readCsvAndWriteSchemaParquet(SparkSession spark) throws Exception{
      System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    StructType schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("id", DataTypes.IntegerType, false),
					DataTypes.createStructField("name", DataTypes.StringType, false),
					DataTypes.createStructField("url", DataTypes.StringType, true),
					DataTypes.createStructField("pictures", DataTypes.StringType, true),
					DataTypes.createStructField("time", DataTypes.TimestampType, true) });
    
    Dataset<Row> csvDF = spark.read().schema(schema).csv("src/main/resources/file.csv");
    

    // DataFrames can be saved as Parquet files, maintaining the schema information
    String csvParquet = null;
    try{
     File f = File.createTempFile("csv_",null,new File("/tmp"));
     csvParquet = f.getName()+".parquet";
     System.out.println("parquet filename: "+csvParquet);
    }catch(Exception e){}
    
    csvDF.write().parquet(csvParquet);

    Dataset<Row> csvParquetDF = spark.read().parquet(csvParquet);

    // Parquet files can also be used to create a temporary view and then used in SQL statements
    csvParquetDF.createOrReplaceTempView("csvParquet");
    Dataset<Row> csvParquetSqlDF = spark.sql("SELECT * FROM csvParquet");
    
    csvParquetSqlDF.show();
    
    System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    
    ///////////////////
  
}

 
 
 
 
public void readJsonAndWriteParquet(SparkSession spark) throws Exception{
    
    
    Dataset<Row> peopleDF = spark.read().json("src/main/resources/people.json");

    // DataFrames can be saved as Parquet files, maintaining the schema information
    String peopleParquet = null;
    try{
     File f = File.createTempFile("people",null,new File("/tmp"));
     peopleParquet = f.getName()+".parquet";
     System.out.println("parquet filename: "+peopleParquet);
    }catch(Exception e){}
    
    
    
    peopleDF.write().parquet(peopleParquet);

    // Read in the Parquet file created above.
    // Parquet files are self-describing so the schema is preserved
    // The result of loading a parquet file is also a DataFrame
    Dataset<Row> parquetFileDF = spark.read().parquet(peopleParquet);

    // Parquet files can also be used to create a temporary view and then used in SQL statements
    parquetFileDF.createOrReplaceTempView("peopleParquet");
    Dataset<Row> namesDF = spark.sql("SELECT name FROM peopleParquet WHERE age BETWEEN 13 AND 19");
    
    Dataset<String> namesDS = namesDF.map(new MapFunction<Row, String>() {
      public String call(Row row) {
        return "Name: " + row.getString(0);
      }
    }, Encoders.STRING());

    namesDS.show();
   
    // +------------+
    // |       value|
    // +------------+
    // |Name: Justin|
    // +------------+
    // $example off:basic_parquet_example$
  }


/*   WHAT THIS IS DOING?


public class StratifiedDatasets {
    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder()
                .appName("Stratified Datasets")
                .getOrCreate();

        Dataset<Row> data = spark.read().format("libsvm").load("sample_libsvm_data.txt");


        // JavaPairRDD is just RDD of tuples. 

        JavaPairRDD<Double, Row> rdd = data.toJavaRDD().keyBy(x -> x.getDouble(0));
        Map<Double, Double> fractions = rdd.map(Tuple2::_1)
                .distinct()
                .mapToPair((PairFunction<Double, Double, Double>) (Double x) -> new Tuple2(x, 0.8))
                .collectAsMap();

        JavaRDD<Row> sampledRDD = rdd.sampleByKeyExact(false, fractions, 2L).values();
        Dataset<Row> sampledData = spark.createDataFrame(sampledRDD, data.schema());

        sampledData.show();
        sampledData.printSchema();
    }
}



*/


 
    
}
