package local.spark;


import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SparkSessionProvider {

    private static final Logger log = LoggerFactory.getLogger(SparkSessionProvider.class);

    @Autowired
    private SparkSessionFactory sparkSessionFactory;

    @Value("${application.base}")
    private String applicationBasename;

    private SparkSession session;
    private JavaSparkContext sparkContext;

    public SparkSession getSession() {
        if (session == null) {
            String msg = "Spark session has not been initialised!";
            log.error(msg);
            throw new RuntimeException(msg);
        }
        return session;
    }

    public JavaSparkContext getSparkContext() {
        if (sparkContext == null) {
            String msg = "Spark context has not been initialised!";
            log.error(msg);
            throw new RuntimeException(msg);
        }
        return sparkContext;
    }

    public void stopSession() {
        session.stop();
        session = null;
    }

    public void initialiseSession(BatchLoad batchLoad) {
        String appName = String.format("%s %s", applicationBasename, batchLoad.getAppName());
        log.info("Initialising spark session with appName {}", appName);
        session = sparkSessionFactory.buildSparkSession(appName);
        sparkContext = new JavaSparkContext(session.sparkContext());
    }
}
