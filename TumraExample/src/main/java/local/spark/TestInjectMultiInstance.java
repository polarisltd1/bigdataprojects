package local.spark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Each time someone wires this it will get unique instance of this class
 */
@Component
@Scope("prototype")
public class TestInjectMultiInstance {
    @Autowired
    public ExampleMessageInjectSingleton em;

    @PostConstruct
    public void initialize(){
        System.out.println("PostConstruct "+this.toString());
    }

    @PreDestroy
    public void cleanup(){
        System.out.println("Cleanup "+this.toString());
    }






}
