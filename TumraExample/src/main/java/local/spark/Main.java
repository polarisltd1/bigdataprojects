package local.spark;

    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.context.ApplicationContext;
    import org.springframework.context.annotation.AnnotationConfigApplicationContext;
    import org.springframework.context.annotation.ComponentScan;

    @ComponentScan(basePackages = "local.spark")
    public class Main {

        @Autowired
        private ExampleMessageInjectSingleton message1;

        @Autowired
        private ExampleMessageInjectSingleton message2;



        public static void main(String[] args) {

            ApplicationContext context
                    = new AnnotationConfigApplicationContext(Main.class);

            Main p = context.getBean(Main.class);

            System.out.println("Main instantiated: "+p.toString());

            p.start(args);


            // Explicitly closing application
            // context to force bean cleanup
            ((AnnotationConfigApplicationContext)context).close();
            // fun is that only singleton got postconstruct executed!


        }





        private void start(String[] args) {
            System.out.println("Message: " + message1.getMessage());
            System.out.println("Message: " + message2.getMessage());

            // what if instance needs to be created manually instead odf autowiring?
            // could instance be manually created and having Autowire worked?
            int i=0;
            while(i++<3) {
                TestInjectMultiInstance ti = (TestInjectMultiInstance) SpringUtils.getInstance(TestInjectMultiInstance.class);
                System.out.println("Manually instantiated inst containing autowired "+ti.toString()+"  => "  + ti.em.getMessage());
            }

        }

    }






