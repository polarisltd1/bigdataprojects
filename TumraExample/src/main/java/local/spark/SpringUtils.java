package local.spark;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SpringUtils {
    //private static final Logger LOG = LogManager.getLogger(DownloadReferenceTask.class);
    public static ApplicationContext ctx;

    /**
     * Make Spring inject the application context
     * and save it on a static variable,
     * so that it can be accessed from any point in the application.
     */
    @Autowired
    private void setApplicationContext(ApplicationContext applicationContext) {
        ctx = applicationContext;
    }


    public static Object getInstance(Class theClass){

        if(SpringUtils.ctx==null) {
            System.out.println("CTX is NULL!");
            return null;
        }

        Object o = SpringUtils.ctx.getBean(theClass);
        if(o==null) {
            System.out.println("SpringUtils cant retriecve instance of "+theClass.getName());
        }
        return o;


    }




}