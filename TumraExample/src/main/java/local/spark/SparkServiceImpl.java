package local.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SparkServiceImpl implements SparkService {

    private static final Logger log = LoggerFactory.getLogger(SparkServiceImpl.class);

    @Autowired
    private SparkSessionProvider sparkSessionProvider;

    @Override
    public Dataset<Row> writeSqlResultsParquet(String sql, String parquetPath, WriteMode writeMode, int numberPartitions, String... partitionColumns) {

        SparkSession spark = sparkSessionProvider.getSession();
        Dataset<Row> df = spark.sql(sql);

        if ( numberPartitions == 0 ) {
            df
                    .write()
                    .mode(writeMode.getMode())
                    .partitionBy(partitionColumns)
                    .parquet(parquetPath);
            return df;
        } else {
            df
                    .coalesce(numberPartitions)
                    .write()
                    .mode(writeMode.getMode())
                    .partitionBy(partitionColumns)
                    .parquet(parquetPath);
            return df;
        }
    }

    @Override
    public Dataset<Row> writeSqlResultsCsv(String sql, String csvPath, WriteMode writeMode, String... partitionColumns) {
        SparkSession spark = sparkSessionProvider.getSession();
        Dataset<Row> df = spark.sql(sql);
        df.write()
                .mode(writeMode.getMode())
                .format("com.databricks.spark.csv")
                .partitionBy(partitionColumns)
                .option("header", "true")
                .save(csvPath);
        return df;
    }

    @Override
    public void createViewFromSql(String sql, String viewName) {
        SparkSession spark = sparkSessionProvider.getSession();
        Dataset<Row> df = spark.sql(sql);
        df.createOrReplaceTempView(viewName);
    }

    @Override
    public void createViewFromParquet(String parquetPath, String viewName) {
        SparkSession spark = sparkSessionProvider.getSession();
        Dataset<Row> df = spark.read().parquet(parquetPath);
        df.createOrReplaceTempView(viewName);
    }

    @Override
    public Dataset<Row> openParquet(String parquetPath) {
        SparkSession spark = sparkSessionProvider.getSession();
        return spark.read().parquet(parquetPath);
    }

    @Override
    public Dataset<Row> runSql(String sql) {
        SparkSession spark = sparkSessionProvider.getSession();
        return spark.sql(sql);
    }
    @Override
    public Dataset<Row> runSql(String sql, int numberPartitions) {
        SparkSession spark = sparkSessionProvider.getSession();
        return spark
                .sql(sql)
                .coalesce(numberPartitions);
    }

    @Override
    public void runSqlAndShow(String sql) {
        SparkSession spark = sparkSessionProvider.getSession();
        spark.sql(sql).show();
    }

    @Override
    public Dataset<Row> readDelimitedFiles(boolean header, FieldDelimiter fieldDelimiter, String basePath, String loadPath
            , EscapeCharacter escape, boolean inferSchema, QuoteCharacter quote) {
        SparkSession spark = sparkSessionProvider.getSession();
        if (basePath == null ) {
            Dataset<Row> rows = spark.read()
                    .format("com.databricks.spark.csv")
                    .option("header", header ? "true" : "false")
                    .option("delimiter", fieldDelimiter.getDelimiter())
                    .option("inferSchema", inferSchema ? "true" : "false")
                    //.option("basePath", basePath)
                    .option("escape", escape.getCharacter())
                    .option("quote", quote.getCharacter())
                    .load(loadPath);
            return rows;
        } else {
            Dataset<Row> rows = spark.read()
                    .format("com.databricks.spark.csv")
                    .option("header", header ? "true" : "false")
                    .option("delimiter", fieldDelimiter.getDelimiter())
                    .option("inferSchema", inferSchema ? "true" : "false")
                    .option("basePath", basePath)
                    .option("escape", escape.getCharacter())
                    .option("quote", quote.getCharacter())
                    .load(loadPath);
            return rows;
        }

    }

    @Override
    public Dataset<Row> readJsonFiles(String jsonPath) {
        SparkSession spark = sparkSessionProvider.getSession();
        Dataset<Row> rows = spark.read().json(jsonPath);
        return rows;
    }

    @Override
    public Dataset<Row> createDataFrame(JavaRDD<Row> rdd, StructType schema) {
        SparkSession spark = sparkSessionProvider.getSession();
        Dataset<Row> df = spark.createDataFrame(rdd, schema);
        return df;
    }

    @Override
    public SparkSession getSession() {
        return sparkSessionProvider.getSession();
    }

    @Override
    public JavaSparkContext getSparkContext() {
        return sparkSessionProvider.getSparkContext();
    }

    @Override
    public void stopSession() {
        sparkSessionProvider.stopSession();
    }

    @Override
    public void cacheView(String tableName){
        SparkSession spark = sparkSessionProvider.getSession();
        spark.sqlContext().cacheTable(tableName);
    }
}
