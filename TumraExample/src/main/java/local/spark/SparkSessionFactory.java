package local.spark;


import org.apache.spark.sql.SparkSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SparkSessionFactory {

    private final String master;
    private final String deployMode;

    public SparkSessionFactory(@Value("${application.spark.master}") String master,
                               @Value("${application.spark.deploy-mode}") String deployMode) {
        this.master = master;
        this.deployMode = deployMode;
    }

    public SparkSession buildSparkSession(String app) {
        SparkSession spark = SparkSession
                .builder()
                .appName(app)
                .master(master)
                .config("deploy-mode", deployMode)
                .config("spark.ui.enabled", "false")
                .config("spark.sql.parquet.mergeSchema", "false")
                //.config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
                .getOrCreate();
        return spark;
    }
}
