package local.spark;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.StructType;



public interface SparkService {
    Dataset<Row> writeSqlResultsParquet(String sql, String parquetPath, local.spark.WriteMode writeMode, int numberPartitions, String... partitionColumns);
    Dataset<Row> writeSqlResultsCsv(String sql, String csvPath, WriteMode writeMode, String... partitionColumns);

    void createViewFromSql(String sql, String viewName);
    void createViewFromParquet(String parquetPath, String viewName);
    Dataset<Row> openParquet(String parquetPath);

    Dataset<Row> runSql(String sql);
    Dataset<Row> runSql(String sql, int numberPartitions);
    void runSqlAndShow(String sql);

    Dataset<Row> readDelimitedFiles(boolean header, FieldDelimiter fieldDelimiter, String basePath, String loadPath
            , EscapeCharacter escape, boolean inferSchema, QuoteCharacter quote);

    Dataset<Row> readJsonFiles(String jsonPath);

    Dataset<Row> createDataFrame(JavaRDD<Row> rdd, StructType schema);

    SparkSession getSession();
    JavaSparkContext getSparkContext();
    void stopSession();

    void cacheView(String tableName);
}
