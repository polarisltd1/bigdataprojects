package local.spark;

enum WriteMode {
    APPEND,
    OVERWRITE;

    public String getMode() {
        return this.name().toLowerCase();
    }
}
