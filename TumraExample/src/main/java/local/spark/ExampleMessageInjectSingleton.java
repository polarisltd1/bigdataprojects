package local.spark;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Demonstrates singleton dependency
 */


@Component
public class ExampleMessageInjectSingleton {

    String getMessage(){
       return "Hello from bean inst: "+this.toString();
    }


    @PostConstruct
    public void initialize(){
        System.out.println("PostConstruct "+this.toString());
    }

    @PreDestroy
    public void cleanup(){
        System.out.println("Cleaninup "+this.toString());
    }


}
