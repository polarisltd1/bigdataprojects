import java.io.FileInputStream

import com.mongodb.casbah._
import com.mongodb.casbah.commons.{MongoDBList, MongoDBObject}
import com.mongodb.casbah.gridfs.GridFS
import com.mongodb.casbah.gridfs.Imports._

//http://api.mongodb.com/scala/casbah/2.1.1/tutorial.html


/**
  * http://api.mongodb.com/scala/casbah/2.1.1/tutorial.html
  *
  *
$ cat /etc/mongodb.conf
fork = false
bind_ip = 0.0.0.0
port = 1234
quiet = false
dbpath = /Users/robertspolis/apps/mongodb/db1
logpath = /Users/robertspolis/apps/mongodb/db1_log/mongo.log
logappend = true
journal = true
  *
  * execute via following script
  *
export PATH=/Users/robertspolis/apps/mongodb-osx-x86_64-3.4.2/bin:$PATH
mongod --config /etc/mongodb.conf
  *
  */
object app extends App {




  val mongoConn = MongoConnection("localhost", 1234)

  println(mongoConn)

  val newObj = MongoDBObject("foo" -> "bar",
    "x" -> "y",
    "pie" -> 3.14,
    "spam" -> "eggs")

  print("saved!")


  val builder = MongoDBList.newBuilder
  builder += "foo"
  builder += "x"
  builder += "pie"
  builder += "spam"
  val newLst = builder.result

  println()

  println("reading: "+newLst)

  // GridFS is a specification for storing and retrieving files that exceed the BSON-document size limit of 16MB.

  val filename = "/Users/robertspolis/Pictures/Image001.png"  // sample file.


  val mongoDB = mongoConn("gridfs_test")  // which database
  val gridfs = GridFS(mongoDB)

  // store file1

  val logo = new FileInputStream(filename)

  gridfs(logo) { fh =>
    fh.filename = "powered_by_mongo.png"
    fh.contentType = "image/png"
  }


  // store file2

  val xls = new java.io.File(filename)
  val savedFile=gridfs.createFile(xls)
  savedFile.filename="ok.xls"
  savedFile.save
  println("savedfile id: %s".format(savedFile._id.get))
  val file=gridfs.findOne(savedFile._id.get)
  val byteArrayOutputStream = new java.io.ByteArrayOutputStream()
  file.map(_.writeTo(byteArrayOutputStream))
  byteArrayOutputStream.toByteArray







}