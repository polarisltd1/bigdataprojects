name := "MongoDBDemo1"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
//  "org.mongodb" %% "casbah" % "2.6.0",
  "org.slf4j" % "slf4j-simple" % "1.6.4"
)

// https://mvnrepository.com/artifact/org.mongodb/casbah_2.11
libraryDependencies += "org.mongodb" % "casbah_2.11" % "3.1.1"
// https://mvnrepository.com/artifact/org.mongodb/mongodb-driver
//libraryDependencies += "org.mongodb" % "mongodb-driver" % "3.1.1"




scalacOptions += "-deprecation"

resolvers ++= Seq("Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
  "sonatype release" at "https://oss.sonatype.org/content/repositories/releases",
  "OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/")

