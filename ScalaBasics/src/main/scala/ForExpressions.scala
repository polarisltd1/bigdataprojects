/**
  * Created by robertspolis on 14/05/2017.
  */
object ForExpressions extends App{

  /**
    *
    * flatmap on String array receives each element string split into character
  */

  {
    val fruits = Seq("apple", "banana", "orange")
    val res = fruits.flatMap(_.toUpperCase)
    println(res)
  }

  /**
    * There comes two For Expressions which are equivalent.
    */


  {
    val n = 5

    val res = (1 until n) flatMap (i =>
      (1 until i) filter (j =>
        isPrime(i + j)) map
        (j => (i, j)))
    println(res)

  }

  {
    val n = 5

    val res = for {
      i <- 1 until n
      j <- 1 until i
      if (isPrime(i + j))
    } yield (i, j)

    println(res)
  }

  def isPrime(x:Int):Boolean = (x/2)!=0







}
