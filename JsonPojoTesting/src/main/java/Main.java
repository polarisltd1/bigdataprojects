import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.StandardToStringStyle;

import java.io.BufferedWriter;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Main {


    public static void main(String[] args) {
        new Main();
    }

    public Main() {


        // serializing into json

        Gson gson = new Gson();

        List<UploadChronoLog> sl = buildUploadChronoLog();

        String jsonStr = gson.toJson(sl);


        System.out.println(jsonStr);

        // deserializing from json


        Type specificationsListType = new TypeToken<ArrayList<Specifications>>() {
        }.getType();

        List<Specifications> objects =
                gson.fromJson(jsonStr, specificationsListType);

        System.out.println(objects);


    }

    /**
     * embed object creation
     *
     * @return
     */
    List<UploadChronoLog> buildUploadChronoLog() {
        List<UploadChronoLog> sl = new ArrayList<UploadChronoLog>();

        UploadChronoLog log = new UploadChronoLog("DCM","DCM_Activity","2016-01-02-10",202020202,"0",LocalDateTime.now(),"filename1");


        sl.add(log);

        return sl;

    }


    void writeStringToFile(String filePathStr, String content) throws Exception {
        Path path = Paths.get(filePathStr);
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write(content);
        }


    }

    /**
     * POJO class to serialize into json
     */


    public static class UploadChronoLog {

        private String dataProvider;
        private String dataType;
        private String hour;
        private long version;
        private long split;
        private LocalDateTime uploaded;
        private String filename;

public UploadChronoLog(String dataProvider
         ,String dataType
         ,String hour
         ,long version
         ,String split
         ,LocalDateTime uploaded
         ,String filename){

    this.dataProvider = dataProvider;
    this.dataType=dataType;
    this.hour=hour;
    this.version=version;
    this.split=split;
    this.uploaded=uploaded;
    this.filename=filename;



}

        @Override
        public String toString() {
            StandardToStringStyle style = new StandardToStringStyle();
            style.setContentEnd("}");
            style.setContentStart("{");
            style.setFieldSeparator(", ");
            style.setUseClassName(false);
            style.setUseIdentityHashCode(false);

            return new ReflectionToStringBuilder(this, style).toString();
        }


        public String getDataProvider() {
            return dataProvider;
        }

        public void setDataProvider(String dataProvider) {
            this.dataProvider = dataProvider;
        }

        public String getDataType() {
            return dataType;
        }

        public void setDataType(String dataType) {
            this.dataType = dataType;
        }

        public String getHour() {
            return hour;
        }

        public void setHour(String hour) {
            this.hour = hour;
        }

        public long getVersion() {
            return version;
        }

        public void setVersion(long version) {
            this.version = version;
        }

        public String getSplit() {
            return split;
        }

        public void setSplit(String split) {
            this.split = split;
        }

        public LocalDateTime getUploaded() {
            return uploaded;
        }

        public void setUploaded(LocalDateTime uploaded) {
            this.uploaded = uploaded;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }


    /**
     *
     */
    public static class ExceptionLog {

       private LocalDateTime heartbeat;

        List<LogEntry> entries;



        @Override
        public String toString() {
            StandardToStringStyle style = new StandardToStringStyle();
            style.setContentEnd("}");
            style.setContentStart("{");
            style.setFieldSeparator(", ");
            style.setUseClassName(false);
            style.setUseIdentityHashCode(false);

            return new ReflectionToStringBuilder(this, style).toString();
        }


    }

    public static class LogEntry {

        private LocalDateTime lastOccurance;
        private Integer occurancies;
        private String message;
        private String content;


    }



}
